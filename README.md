# Yindula Cms

## English

### Description

YindulaCms is a Laravel module developed by Daskana, serving as a CMS (Content Management System). A CMS is a software application or platform that enables users to create, manage, and modify digital content on a website.

The term 'Yindula' holds significance as it means "To reflect" in Kongo or Kikongo, which is a Bantu language spoken by the Kongo people. The Kongo people have historically resided along the Atlantic coast of Central Africa. During the 15th century, this region was a well-organized and centralized Kingdom of Kongo. However, it is now divided among several countries.

In line with this historical context, the creator envisioned YindulaCms as a valuable component that alleviates the need for excessive deliberation in CMS creation. This is because the author has already extensively reflected upon, examined, and analyzed various ideas and experiences.

By providing the necessary code, YindulaCms offers assistance and greatly facilitates the process.

### Prerequisites

- [Git](https://git-scm.com/downloads) installed.
- [Laravel Framework](https://laravel.com) installed.
- [Laravel Modules](https://github.com/nwidart/laravel-modules) installed.
- [Laravel Module Installer](https://github.com/joshbrw/laravel-module-installer) installed.

#### Important Notice

- Before proceeding, ensure that you have installed the "[nwidart/laravel-modules](https://github.com/nwidart/laravel-modules)" package in your Laravel project.
Follow the provided instructions to install this package, as it allows for module installation within Laravel projects.

- Once the "nwidart/laravel-modules" package is installed, an additional step is required to install modules into the "Modules" directory of your project.

- To facilitate the automatic movement of Yindula CMS module files, you need to install the "[joshbrw/laravel-module-installer](https://github.com/joshbrw/laravel-module-installer)" composer plugin.

To install the plugin, open a terminal in the root directory of your Laravel project and execute the following command:

   ```bash
    composer require joshbrw/laravel-module-installer
   ```

- After successfully installing the "joshbrw/laravel-module-installer" plugin, you can proceed with the installation of the Yindula CMS module.
During the installation process, the Yindula CMS module will be automatically placed in the "Modules" directory of your project.

- Refer to the "Installation and Build" section for the appropriate command or instructions to install a specific module.
The "joshbrw/laravel-module-installer" plugin will handle the automatic movement of Yindula CMS module files to the "Modules" directory.

### Installation and Build

To install Yindula Cms for your application, follow these steps:

1. Open a terminal in the root directory of your application.

2. Use the following command to install Yindula Cms:

   ```bash
   composer require loecoscorp/yindula-cms-module
   ```

3. Navigate to the `Modules/YindulaCms` directory using the following command:

   ```bash
   cd Modules/YindulaCms
   ```

4. Install the necessary Node.js dependencies by running the following command:

   ```bash
   npm install
   ```

5. Build the frontend assets by executing the following command:

   ```bash
   npm run build
   ```

6. Finally, compile and build the assets for development by running the following command:

   ```bash
   IF "%NODE_ENV%"=="production" (npm run prod) ELSE (npm run dev)
   ```

## Français

### Description

### Prérequis

- [Git](https://git-scm.com/downloads) installé.
- [Laravel Framework](https://laravel.com) installé.
- [Laravel Modules](https://github.com/nwidart/laravel-modules) installé.
- [Laravel Module Installer](https://github.com/joshbrw/laravel-module-installer) installé.

#### Avis important

- Avant de continuer, assurez-vous d'avoir installé le package "[nwidart/laravel-modules](https://github.com/nwidart/laravel-modules)" dans votre projet Laravel.
Suivez les instructions fournies pour installer ce package, car il permet l'installation de modules dans les projets Laravel.

- Une fois le package "nwidart/laravel-modules" installé, une étape supplémentaire est nécessaire pour installer les modules dans le répertoire "Modules" de votre projet.

- Pour faciliter le déplacement automatique des fichiers de module Yindula CMS, vous devez installer le plugin composer "[joshbrw/laravel-module-installer](https://github.com/joshbrw/laravel-module-installer)".

Pour installer le plugin, ouvrez un terminal dans le répertoire racine de votre projet Laravel et exécutez la commande suivante :

   ```bash
    composer require joshbrw/laravel-module-installer
   ```

- Après avoir installé avec succès le plugin "joshbrw/laravel-module-installer", vous pouvez procéder à l'installation du module Yindula CMS.
Pendant le processus d'installation, le module Yindula CMS sera automatiquement placé dans le répertoire "Modules" de votre projet.

- Reportez-vous à la section "Installation et construction" pour obtenir la commande ou les instructions appropriées pour installer un module spécifique.
- Le plugin "joshbrw/laravel-module-installer" se chargera du déplacement automatique des fichiers de module Yindula CMS vers le répertoire "Modules".

### Installation et construction

Pour installer le module Yindula Cms dans votre application, suivez ces étapes :

1. Ouvrez un terminal dans le répertoire principal de votre application.

2. Utilisez la commande suivante pour installer Yindula Cms :

   ```bash
   composer require loecoscorp/yindula-cms-module
   ```

3. Naviguez vers le répertoire `Modules/YindulaCms` en utilisant la commande suivante :

   ```bash
   cd Modules/YindulaCms
   ```

4. Installez les dépendances nécessaires de Node.js en exécutant la commande suivante :

   ```bash
   npm install
   ```

5. Construisez les ressources frontales en exécutant la commande suivante :

   ```bash
   npm run build
   ```

6. Enfin, compilez et construisez les ressources pour le développement en exécutant la commande suivante :

   ```bash
   IF "%NODE_ENV%"=="production" (npm run prod) ELSE (npm run dev)
   ```
