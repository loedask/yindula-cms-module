<?php

namespace YindulaCms\View\Components;

use Closure;
use Spatie\Menu\Laravel\Link;
use Illuminate\View\Component;
use Illuminate\Contracts\View\View;
use Spatie\Menu\Laravel\Menu as SpatieMenu;

class Ckeditor extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('yindulacms::components.ckeditor');
    }
}
