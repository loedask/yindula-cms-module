<?php

namespace Modules\YindulaCms\app\Console\Traits;

use Modules\YindulaCms\app\Console\Commands\RunCmsSetupCommands;


trait RegistersCommandsTrait
{
    /**
     * Register commands in the format of Command::class
     */
    protected function registerCommands(): void
    {
        $this->commands([
            RunCmsSetupCommands::class,
        ]);
    }
}
