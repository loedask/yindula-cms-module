<?php

namespace Modules\YindulaCms\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class RunCmsSetupCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yindula:run-cms-setup {--fresh : Reset the database and migrate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run setup commands: migrate, seed, passport:install, passport:keys';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($this->option('fresh')) {

            // Collect data before refreshing the database
            if ($this->confirm('Do you want to collect data before refreshing the database?')) {
                $this->collectData();
            }

            $this->call('migrate:fresh');

        } else {
            $this->call('migrate');
        }

        $this->call('db:seed');

        $this->info('Setup commands completed successfully.');
    }

    private function collectData()
    {
        $this->info('Collecting data...');

        Artisan::call('iseed users,cms_settings,pages,menus,menu_items,banners,feature_sections,features,cms_events --force');

        $this->info('Data collection completed successfully.');
    }
}
