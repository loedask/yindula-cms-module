<?php

namespace Modules\YindulaCms\app\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureSetting extends Model
{
    public $table = 'feature_settings';

    public $fillable = [
        'feature_section_id',
        'has_category',
        'has_content',
        'has_url',
        'has_icon',
        'has_sort_order',
        'has_redirect_to_page',
        'has_image',
        'has_image_two',
        'has_video',
        'has_video_two'
    ];

    protected $casts = [
        'has_category' => 'boolean',
        'has_content' => 'boolean',
        'has_url' => 'boolean',
        'has_icon' => 'boolean',
        'has_sort_order' => 'boolean',
        'has_redirect_to_page' => 'boolean',
        'has_image' => 'boolean',
        'has_image_two' => 'boolean',
        'has_video' => 'boolean',
        'has_video_two' => 'boolean'
    ];

    public static array $rules = [
        'feature_section_id' => 'required',
        'has_category' => 'nullable|boolean',
        'has_content' => 'nullable|boolean',
        'has_url' => 'nullable|boolean',
        'has_icon' => 'nullable|boolean',
        'has_sort_order' => 'nullable|boolean',
        'has_redirect_to_page' => 'nullable|boolean',
        'has_image' => 'nullable|boolean',
        'has_image_two' => 'nullable|boolean',
        'has_video' => 'nullable|boolean',
        'has_video_two' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    public function featureSection(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\Modules\YindulaCms\app\Models\FeatureSection::class, 'feature_section_id');
    }
}
