<?php

namespace Modules\YindulaCms\app\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;
use Modules\YindulaCms\app\Traits\UsesDefaultConnection;
use Modules\YindulaCms\app\Utilities\BannerHelper;
use Modules\YindulaCms\app\Utilities\ModelMediaHelper;
use Modules\YindulaCore\Traits\CanGetTableNameStatically;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * Class Banner
 * @package App\Models
 * @version September 10, 2022, 5:51 pm UTC
 *
 * @property string $title
 * @property string $excerpt
 * @property string $description
 * @property string $image
 * @property string $video
 * @property boolean $is_default
 * @property string $image_two
 */
class Banner extends Model
{

    use HasFactory, CanGetTableNameStatically, UsesCmsDatabaseConnection;

    public $table = 'banners';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'excerpt',
        'description',
        'image',
        'video',
        'is_default',
        'image_two'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'excerpt' => 'string',
        'description' => 'string',
        'image' => 'string',
        'video' => 'string',
        'is_default' => 'boolean',
        'image_two' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:100',
        'excerpt' => 'nullable|string',
        'description' => 'nullable|string',
        'image' => 'nullable|string|max:250',
        'video' => 'nullable|string|max:125',
        'is_default' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'image_two' => 'nullable|string'
    ];




    public function getReadableVideoAttribute()
    {
        return env('APP_URL') .  (new Application($this->getVideoPath()))->replaceBackSlashesWithForwardSlashes();
    }


    private function getVideoPath()
    {
        if (App::environment('production')) {
            return '/storage/app/public/' . json_decode($this->attributes['video'])[0]->download_link;
        }
        return Storage::url(json_decode($this->attributes['video'])[0]->download_link);
    }

    /**
     *
     * @return string
     */
    public function getImageUrlAttribute(): string
    {
        return ModelMediaHelper::getImageAttribute(self::tableName(), $this->id);
    }

    /**
     *
     * @return string
     */
    public function getImageTwoUrlAttribute(): string
    {
        return ModelMediaHelper::getImageTwoAttribute(self::tableName(), $this->id);
    }
}
