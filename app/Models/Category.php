<?php

namespace Modules\YindulaCms\app\Models;

use Cviebrock\EloquentSluggable\Sluggable;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class Category
 * @package Modules\YindulaCms\app\Models
 * @version February 24, 2022, 8:23 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $featureSections
 * @property \Illuminate\Database\Eloquent\Collection $features
 * @property integer $parent_id
 * @property integer $order
 * @property string $name
 * @property string $slug
 */
class Category extends Model
{

    use HasFactory, Sluggable, UsesCmsDatabaseConnection;

    public $table = 'categories';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'parent_id',
        'order',
        'name',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'parent_id' => 'integer',
        'order' => 'integer',
        'name' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'parent_id' => 'nullable|integer',
        'order' => 'required|integer',
        'name' => 'required|string|max:255',
        'slug' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function featureSections()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\FeatureSection::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function features()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\Feature::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function parentId()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')
            ->with('children')->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function posts()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\Post::class)
            ->published()
            ->orderBy('created_at', 'DESC');
    }

    /*
    |---------------------------------------------------------------------------------------------
    | Custom Attributes
    |---------------------------------------------------------------------------------------------
    */
    /**
     * This attribute converts the "created_at" field into a more readable attribute.
     * It does not have a corresponding column in the database. It can be accessed
     * using the "snake case", e.g. $game_participant->readable_created_at.
     * @return void
     */
    public function getNameWithParentAttribute()
    {
        return $this->attributes['name'] . ' (' . $this->parentId->name . ')';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate'  => true,
                'separator' => '-',
            ]
        ];
    }
}
