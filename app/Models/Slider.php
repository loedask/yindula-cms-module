<?php

namespace Modules\YindulaCms\app\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class Slider
 * @package App\Models
 * @version September 11, 2022, 6:56 am UTC
 *
 * @property string $title
 * @property string $subtitle
 * @property string $excerpt
 * @property string $description
 * @property string $image
 * @property string $image_two
 * @property string $video
 * @property string $url
 * @property boolean $is_default
 */
class Slider extends Model
{

    use HasFactory, UsesCmsDatabaseConnection;

    public $table = 'sliders';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'subtitle',
        'excerpt',
        'description',
        'image',
        'image_two',
        'video',
        'url',
        'is_default'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'subtitle' => 'string',
        'excerpt' => 'string',
        'description' => 'string',
        'image' => 'string',
        'image_two' => 'string',
        'video' => 'string',
        'url' => 'string',
        'is_default' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:100',
        'subtitle' => 'nullable|string|max:100',
        'excerpt' => 'nullable|string',
        'description' => 'nullable|string',
        'image' => 'nullable|string|max:125',
        'image_two' => 'nullable|string',
        'video' => 'nullable|string|max:125',
        'url' => 'nullable|string|max:125',
        'is_default' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    protected $translatable = [
        'title',
        'description'
    ];

    public function getDefaultSliderAttribute()
    {
        if ($this->is_default == 1) {
            return "active";
        }
        return "";
    }

    public function getSliders()
    {
        // return self::withTranslations(App::getLocale())->get();
        return self::all();
    }
}
