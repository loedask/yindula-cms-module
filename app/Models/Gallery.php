<?php

namespace Modules\YindulaCms\app\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\YindulaCore\Traits\CanGetTableNameStatically;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class Gallery
 * @package App\Models
 * @version September 13, 2022, 6:06 pm UTC
 *
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $image_two
 * @property string $video
 */
class Gallery extends Model
{

    use HasFactory, CanGetTableNameStatically, UsesCmsDatabaseConnection;

    public $table = 'galleries';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'title',
        'description',
        'image',
        'image_two',
        'video'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'image' => 'string',
        'image_two' => 'string',
        'video' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:100',
        'description' => 'nullable|string',
        'image' => 'nullable|string|max:125',
        'image_two' => 'nullable|string',
        'video' => 'nullable|string|max:125',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


}
