<?php

namespace Modules\YindulaCms\app\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\YindulaCms\app\Models\FeatureSection;

class FeatureSectionSetting extends Model
{
    public $table = 'feature_section_settings';

    public $fillable = [
        'feature_section_id',
        'has_category',
        'has_name',
        'has_title',
        'has_description',
        'has_image',
        'has_image_two'
    ];

    protected $casts = [
        'has_category' => 'boolean',
        'has_name' => 'boolean',
        'has_title' => 'boolean',
        'has_description' => 'boolean',
        'has_image' => 'boolean',
        'has_image_two' => 'boolean'
    ];

    public static array $rules = [
        'feature_section_id' => 'required',
        'has_category' => 'nullable|boolean',
        'has_name' => 'nullable|boolean',
        'has_title' => 'nullable|boolean',
        'has_description' => 'nullable|boolean',
        'has_image' => 'nullable|boolean',
        'has_image_two' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    public function featureSection(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(FeatureSection::class, 'feature_section_id');
    }
}
