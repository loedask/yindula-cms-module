<?php

namespace Modules\YindulaCms\app\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\YindulaCore\Utilities\DateTimeFormat;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

class CmsEvent extends Model
{
    use HasFactory, UsesCmsDatabaseConnection;

    public $table = 'cms_events';

    public $fillable = [
        'title',
        'description',
        'start_date',
        'end_date',
        'image',
        'start_time',
        'end_time',
        'is_full_day'
    ];

    protected $dates = [
        'start_date',
    ];


    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'start_date' => 'date',
        'end_date' => 'date',
        'image' => 'string',
        'start_time' => 'datetime:H:i:s',
        'end_time' => 'datetime:H:i:s',
        'is_full_day' => 'boolean'
    ];

    public static array $rules = [
        'title' => 'required|string|max:255',
        'description' => 'nullable|string|max:65535',
        'start_date' => 'required',
        'end_date' => 'nullable',
        'image' => 'nullable|string|max:255',
        'start_time' => 'nullable',
        'end_time' => 'nullable',
        'is_full_day' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    public function getEventDateAtAttribute()
    {
        return (new DateTimeFormat($this->start_date))->getReadable();
    }

    // public function getStartDateAttribute()
    // {
    //     return $this->start_date ? $this->start_date->format('Y-m-d') : null;
    // }

    // public function getEndDateAttribute()
    // {
    //     return $this->end_date->format('Y-m-d');
    // }

    // public function getStartTimeAttribute()
    // {
    //     return $this->start_time->format('H:i:s');
    // }

    // public function getEndTimeAttribute()
    // {
    //     return $this->end_time->format('H:i:s');
    // }

}
