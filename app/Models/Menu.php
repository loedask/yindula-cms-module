<?php

namespace Modules\YindulaCms\app\Models;


use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Cache;
use Modules\YindulaCms\app\Enums\MenuBuilder;
use Modules\YindulaCms\app\Events\DisplayMenu;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class Menu
 * @package Modules\YindulaCms\app\Models
 * @version January 4, 2022, 9:32 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $menuItems
 * @property string $name
 */
class Menu extends Model
{

    use HasFactory, UsesCmsDatabaseConnection;

    public $table = 'menus';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function menuItems()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\MenuItem::class, 'menu_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function parentItems()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\MenuItem::class)
            ->whereNull('parent_id')->orderBy('order');
    }

    public function removeMenuFromCache()
    {
        Cache::forget(MenuBuilder::CACHE_NAME . $this->name);
    }



    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            $model->removeMenuFromCache();
        });

        static::deleted(function ($model) {
            $model->removeMenuFromCache();
        });
    }

    /**
     * Display menu.
     *
     * @param string      $menuName
     * @param string|null $type
     * @param array       $options
     *
     * @return string
     */
    public static function display($menuName, $type = null, array $options = [])
    {
        // GET THE MENU - sort collection in blade
        $menu = Cache::remember(MenuBuilder::CACHE_NAME . $menuName, \Carbon\Carbon::now()->addDays(30), function () use ($menuName) {
            return static::where('name', '=', $menuName)
                ->with(['parentItems.children' => function ($q) {
                    $q->orderBy('order');
                }])
                ->first();
        });

        // Check for Menu Existence
        if (!isset($menu)) {
            return false;
        }

        event(new DisplayMenu($menu));

        // Convert options array into object
        $options = (object) $options;

        $items = $menu->parentItems->sortBy('order');


        if (is_null($type)) {
            $type = 'yindulacms::menu.default';
        } elseif ($type == 'bootstrap' && !view()->exists($type)) {
            $type = 'yindulacms::menu.bootstrap';
        } elseif ($type == 'custom' && !view()->exists($type)) {
            $type = 'yindulacms::menu.custom';
        }


        if (!isset($options->locale)) {
            $options->locale = app()->getLocale();
        }

        if ($type === '_json') {
            return $items;
        }

        return new \Illuminate\Support\HtmlString(
            \Illuminate\Support\Facades\View::make($type, ['items' => $items, 'options' => $options])->render()
        );
    }
}
