<?php

namespace Modules\YindulaCms\app\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class CmsSetting
 *
 * @package App\Models
 * @version September 13, 2022, 1:52 pm UTC
 * @property string $key
 * @property string $value
 * @property string $type
 * @property integer $sort_order
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CmsSetting whereValue($value)
 * @mixin Model
 */
class CmsSetting extends Model
{

    use HasFactory, UsesCmsDatabaseConnection;

    public $table = 'cms_settings';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'key',
        'value',
        'type',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'key' => 'string',
        'value' => 'string',
        'type' => 'string',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'key' => 'required|string|max:125',
        'value' => 'required|string',
        'type' => 'required|string|max:125',
        'sort_order' => 'nullable|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

}
