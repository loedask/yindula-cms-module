<?php
namespace Modules\YindulaCms\app\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Modules\YindulaCore\Traits\CanGetTableNameStatically;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class Post
 * @package Modules\Cms\Entities
 * @version January 8, 2022, 9:54 am UTC
 *
 * @property integer $author_id
 * @property integer $category_id
 * @property string $title
 * @property string $seo_title
 * @property string $excerpt
 * @property string $body
 * @property string $image
 * @property string $slug
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $status
 * @property boolean $featured
 */
class Post extends Model
{
    use HasFactory,Sluggable, CanGetTableNameStatically, UsesCmsDatabaseConnection;

    public $table = 'posts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public const PUBLISHED = 'PUBLISHED';


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate'  => true,
                'separator' => '-',
            ]
        ];
    }


    public $fillable = [
        'author_id',
        'category_id',
        'title',
        'seo_title',
        'excerpt',
        'body',
        'image',
        'slug',
        'meta_description',
        'meta_keywords',
        'status',
        'featured'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'author_id' => 'integer',
        'category_id' => 'integer',
        'title' => 'string',
        'seo_title' => 'string',
        'excerpt' => 'string',
        'body' => 'string',
        'image' => 'string',
        'slug' => 'string',
        'meta_description' => 'string',
        'meta_keywords' => 'string',
        'status' => 'string',
        'featured' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'author_id' => 'integer',
        'category_id' => 'nullable|integer',
        'title' => 'required|string|max:255',
        'seo_title' => 'nullable|string|max:255',
        'excerpt' => 'nullable|string',
        'body' => 'required|string',
        'image' => 'nullable|string|max:255',
        'slug' => 'string|max:255',
        'meta_description' => 'nullable|string',
        'meta_keywords' => 'nullable|string',
        'status' => 'string',
        'featured' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->getKey();
        }

        $this->status = static::PUBLISHED;

        return parent::save();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     **/
    public function authorId()
    {
        return $this->belongsTo(\App\Models\User::class, 'author_id', 'id');
    }

    /**
     * Scope a query to only published scopes.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::PUBLISHED);
    }


    public function getLinkAttribute()
    {
        return 'blog/' . $this->slug;
    }

    public function getTruncatedBodyAttribute()
    {
        $body = Str::words($this->body, 30);
        return strip_tags($body);
    }

    public function getPosts()
    {
        return self::where('status', '=', 'PUBLISHED')
            // ->withTranslations(App::getLocale())
            ->orderBy('created_at', 'DESC')
            ->paginate(10)
            ->get();
    }

}
