<?php
namespace Modules\YindulaCms\app\Models;


use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Route;
use Modules\YindulaCms\app\Http\Observers\After\MenuItemObserverAfter;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class MenuItem
 * @package Modules\YindulaCms\app\Models
 * @version January 4, 2022, 10:27 am UTC
 *
 * @property \Modules\YindulaCms\app\Models\Menu $menu
 * @property integer $menu_id
 * @property string $title
 * @property string $url
 * @property string $target
 * @property string $icon_class
 * @property string $color
 * @property integer $parent_id
 * @property integer $order
 * @property string $route
 * @property string $parameters
 */
class MenuItem extends Model
{

    use HasFactory, UsesCmsDatabaseConnection;

    public $table = 'menu_items';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'menu_id',
        'title',
        'url',
        'target',
        'icon_class',
        'color',
        'parent_id',
        'order',
        'route',
        'parameters'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'menu_id' => 'integer',
        'title' => 'string',
        'url' => 'string',
        'target' => 'string',
        'icon_class' => 'string',
        'color' => 'string',
        'parent_id' => 'integer',
        'order' => 'integer',
        'route' => 'string',
        'parameters' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'menu_id' => 'nullable|integer',
        'title' => 'required|string|max:255',
        'url' => 'required|string|max:255',
        'target' => 'required|string|max:255',
        'icon_class' => 'nullable|string|max:255',
        'color' => 'nullable|string|max:255',
        'parent_id' => 'nullable|integer',
        'order' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'route' => 'nullable|string|max:255',
        'parameters' => 'nullable|string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function menu()
    {
        return $this->belongsTo(\Modules\YindulaCms\app\Models\Menu::class, 'menu_id');
    }

    public function children()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\MenuItem::class, 'parent_id')
            ->with('children')->orderBy('order');
    }

    public static function boot()
    {
        parent::boot();

        self::observe(MenuItemObserverAfter::class);

        // static::created(function ($model) {
        //     $model->menu->removeMenuFromCache();
        // });

        // static::saved(function ($model) {
        //     $model->menu->removeMenuFromCache();
        // });

        // static::deleted(function ($model) {
        //     $model->menu->removeMenuFromCache();
        // });
    }

    public function link($absolute = false)
    {
        return $this->prepareLink($absolute, $this->route, $this->parameters, $this->url);
    }

    protected function prepareLink($absolute, $route, $parameters, $url)
    {
        if (is_null($parameters)) {
            $parameters = [];
        }

        if (is_string($parameters)) {
            $parameters = json_decode($parameters, true);
        } elseif (is_array($parameters)) {
            $parameters = $parameters;
        } elseif (is_object($parameters)) {
            $parameters = json_decode(json_encode($parameters), true);
        }

        if (!is_null($route)) {
            if (!Route::has($route)) {
                return '#';
            }

            return route($route, $parameters, $absolute);
        }

        if ($absolute) {
            return url($url);
        }

        return $url;
    }
}
