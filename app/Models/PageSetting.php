<?php

namespace Modules\YindulaCms\app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PageSetting extends Model
{
    use HasFactory;
    public $table = 'page_settings';

    public $fillable = [
        'page_id',
        'has_subtitle',
        'has_meta_description',
        'has_meta_keywords',
        'has_excerpt',
        'has_body',
        'has_image',
        'has_image_two'
    ];

    protected $casts = [
        'has_subtitle' => 'boolean',
        'has_meta_description' => 'boolean',
        'has_meta_keywords' => 'boolean',
        'has_excerpt' => 'boolean',
        'has_body' => 'boolean',
        'has_image' => 'boolean',
        'has_image_two' => 'boolean'
    ];

    public static array $rules = [
        'page_id' => 'required',
        'has_subtitle' => 'nullable|boolean',
        'has_meta_description' => 'nullable|boolean',
        'has_meta_keywords' => 'nullable|boolean',
        'has_excerpt' => 'nullable|boolean',
        'has_body' => 'nullable|boolean',
        'has_image' => 'nullable|boolean',
        'has_image_two' => 'nullable|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    public function page(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\Modules\YindulaCms\app\Models\Page::class, 'page_id');
    }
}
