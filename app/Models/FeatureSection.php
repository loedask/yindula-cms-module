<?php

namespace Modules\YindulaCms\app\Models;


use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\YindulaCore\Traits\CanGetTableNameStatically;
use Modules\YindulaCms\app\Http\Observers\After\FeatureSectionAfterSaveObserver;
use Modules\YindulaCms\app\Http\Observers\After\FeatureSectionAfterDeleteObserver;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;


/**
 * Class FeatureSection
 * @package Modules\YindulaCms\app\Models
 * @version January 3, 2022, 1:39 pm UTC
 *
 * @property \Modules\YindulaCms\app\Models\Category $category
 * @property \Modules\YindulaCms\app\Models\Page $page
 * @property \Illuminate\Database\Eloquent\Collection $features
 * @property string $key
 * @property string $name
 * @property string $title
 * @property integer $page_id
 * @property string $description
 * @property boolean $is_active
 * @property integer $category_id
 */
class FeatureSection extends Model
{
    use HasFactory, Sluggable, CanGetTableNameStatically, UsesCmsDatabaseConnection;

    public $table = 'feature_sections';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const FEATURE_SECTION_ONE = 1;
    const FEATURE_SECTION_TWO = 2;
    const FEATURE_SECTION_THREE = 3;
    const FEATURE_SECTION_FOUR = 4;


    public $fillable = [
        'key',
        'name',
        'title',
        'image',
        'page_id',
        'description',
        'is_active',
        'category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'key' => 'string',
        'name' => 'string',
        'title' => 'string',
        'image' => 'string',
        'page_id' => 'integer',
        'description' => 'string',
        'is_active' => 'boolean',
        'category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'key' => 'string|max:100',
        'name' => 'required|string|max:100',
        'title' => 'nullable|string|max:100',
        'image' => 'nullable|string|max:255',
        'page_id' => 'nullable|integer',
        'description' => 'nullable|string',
        'is_active' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'category_id' => 'nullable|integer'
    ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::observe(FeatureSectionAfterSaveObserver::class);
        self::observe(FeatureSectionAfterDeleteObserver::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\Modules\YindulaCms\app\Models\Category::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function page()
    {
        return $this->belongsTo(\Modules\YindulaCms\app\Models\Page::class, 'page_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function features()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\Feature::class, 'feature_section_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'key' => [
                'source' => 'name',
                'onUpdate'  => true,
                'separator' => '-',
            ]
        ];
    }


    public function getLabelAttribute()
    {
        return $this->attributes['name'] . ' ( ' . $this->page->title . ' )';
    }


    public static function findByKey($key)
    {
        return static::where('key', $key)->first();
    }

    /*
    |---------------------------------------------------------------------------------------------
    | Custom Attributes
    |---------------------------------------------------------------------------------------------
    */
    /**
     * This attribute converts the "created_at" field into a more readable attribute.
     * It does not have a corresponding column in the database. It can be accessed
     * using the "snake case", e.g. $game_participant->readable_created_at.
     * @return void
     */
    public function getIsActiveAttribute()
    {
        return ($this->attributes['is_active']) ? "Yes" : "No";
    }
}
