<?php

namespace Modules\YindulaCms\app\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Modules\YindulaCms\app\Enums\PageStatus;
use Modules\YindulaCms\app\Http\Observers\After\PageObserverAfter;
use Modules\YindulaCms\app\Http\Observers\Before\PageObserverBefore;
use Modules\YindulaCms\app\Utilities\PageHelper;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class Page
 *
 * @package Modules\Cms\Entities
 * @version January 3, 2022, 12:53 pm UTC
 * @property \Illuminate\Database\Eloquent\Collection $featureSections
 * @property \Illuminate\Database\Eloquent\Collection $features
 * @property integer $author_id
 * @property string $title
 * @property string $subtitle
 * @property string $excerpt
 * @property string $body
 * @property string $image
 * @property string $slug
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $status
 * @property boolean $is_home
 * @property boolean $is_page
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\YindulaCms\app\Models\FeatureSection[] $featureSection
 * @property-read int|null $feature_section_count
 * @property-read int|null $feature_sections_count
 * @property-read int|null $features_count
 * @property-read void $readable_is_home
 * @property-read void $readable_is_page
 * @property-read mixed $truncated_body
 * @method static \Illuminate\Database\Eloquent\Builder|Page active()
 * @method static \Illuminate\Database\Eloquent\Builder|Page allPages()
 * @method static \Illuminate\Database\Eloquent\Builder|Page findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|Page isAbout()
 * @method static \Illuminate\Database\Eloquent\Builder|Page isPage()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereIsHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereIsContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereIsPage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 * @mixin Model
 */
class Page extends Model
{

    use HasFactory, Sluggable, UsesCmsDatabaseConnection;

    public $table = 'pages';


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate'  => true,
                'separator' => '-',
            ]
        ];
    }


    public $fillable = [
        'author_id',
        'title',
        'subtitle',
        'excerpt',
        'body',
        'image',
        'slug',
        'meta_description',
        'meta_keywords',
        'status',
        'is_home',
        'is_contact',
        'is_page',
        'is_event'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'author_id' => 'integer',
        'title' => 'string',
        'subtitle' => 'string',
        'excerpt' => 'string',
        'body' => 'string',
        'image' => 'string',
        'slug' => 'string',
        'meta_description' => 'string',
        'meta_keywords' => 'string',
        'status' => 'string',
        'is_home' => 'boolean',
        'is_contact' => 'boolean',
        'is_page' => 'boolean',
        'is_event' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'author_id' => 'integer|integer',
        'title' => 'required|string|max:255|string|max:255',
        'subtitle' => 'nullable|string|max:255',
        'excerpt' => 'string|nullable|string',
        'body' => 'string|nullable|string',
        'image' => 'string|max:255|nullable|string|max:255',
        'slug' => 'string|max:255|string|max:255',
        'meta_description' => 'nullable|string|nullable|string',
        'meta_keywords' => 'nullable|string|nullable|string',
        'status' => 'required|string|string',
        'is_home' => 'nullable|boolean',
        'is_contact' => 'nullable|boolean',
        'is_page' => 'nullable|boolean',
        'is_event' => 'nullable|boolean',
        'created_at' => 'nullable|nullable',
        'updated_at' => 'nullable|nullable'
    ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::observe(PageObserverBefore::class);
        self::observe(PageObserverAfter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     **/
    public function author()
    {
        return $this->belongsTo(\App\Models\User::class, 'author_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function featureSections()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\FeatureSection::class, 'page_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function features()
    {
        return $this->hasMany(\Modules\YindulaCms\app\Models\Feature::class, 'page_id');
    }

    public function featureSection()
    {
        return $this->hasMany(FeatureSection::class);
    }


    /**
     * Scope a query to only include active pages.
     *
     * @param  $query  \Illuminate\Database\Eloquent\Builder
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', PageStatus::STATUS_ACTIVE);
    }


    public function getTruncatedBodyAttribute()
    {
        $body = Str::words($this->body, 30);
        return strip_tags($body);
    }

    public function getHomePageImage()
    {
        return self::where('is_home', 1)->pluck('image')->first();
    }


    public function scopeIsPage($query)
    {
        return $query->where('is_page', 1);
    }


    public function scopeIsAbout($query)
    {
        return $query->where('is_about', 1);
    }


    public function scopeAllPages($query)
    {
        return $query->where('is_page', 1)->where('is_page', 0);
    }

    /*
    |---------------------------------------------------------------------------------------------
    | Custom Attributes
    |---------------------------------------------------------------------------------------------
    */
    /**
     * This attribute converts the "created_at" field into a more readable attribute.
     * It does not have a corresponding column in the database. It can be accessed
     * using the "snake case", e.g. $game_participant->readable_created_at.
     * @return void
     */
    public function getReadableIsHomeAttribute()
    {
        return ($this->attributes['is_home']) ? "Yes" : "No";
    }

    /**
     * This attribute converts the "created_at" field into a more readable attribute. It tells
     * the time ago instead of the exact time and does not have a corresponding column in the
     * database. It can be accessed using the "snake case", e.g. $game_participant->time_ago_created_at.
     * @return void
     */
    public function getReadableIsPageAttribute()
    {
        return ($this->attributes['is_page']) ? "Yes" : "No";
    }

    /**
     *
     * @return string
     */
    public function getImageUrlAttribute(): string
    {
        return PageHelper::getImageAttribute($this->id);
    }
}
