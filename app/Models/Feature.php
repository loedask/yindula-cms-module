<?php

namespace Modules\YindulaCms\app\Models;

use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\YindulaCms\app\Utilities\ModelMediaHelper;
use Modules\YindulaCore\Traits\CanGetTableNameStatically;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

/**
 * Class Feature
 * @package Modules\YindulaCms\app\Models
 * @version January 3, 2022, 3:11 pm UTC
 *
 * @property \Modules\YindulaCms\app\Models\Category $category
 * @property \Modules\YindulaCms\app\Models\FeatureSection $featureSection
 * @property \Modules\YindulaCms\app\Models\Page $page
 * @property integer $feature_section_id
 * @property string $title
 * @property string $content
 * @property string $excerpt
 * @property integer $redirect_to_page_id
 * @property string $description
 * @property string $image
 * @property integer $sort_order
 * @property boolean $is_active
 * @property string $icon
 * @property string $url
 * @property integer $category_id
 */
class Feature extends Model
{

    use HasFactory, UsesCmsDatabaseConnection, CanGetTableNameStatically;

    public $table = 'features';




    public $fillable = [
        'feature_section_id',
        'title',
        'content',
        'excerpt',
        'redirect_to_page_id',
        'description',
        'image',
        'sort_order',
        'is_active',
        'icon',
        'url',
        'category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'feature_section_id' => 'integer',
        'title' => 'string',
        'content' => 'string',
        'excerpt' => 'string',
        'redirect_to_page_id' => 'integer',
        'description' => 'string',
        'image' => 'string',
        'sort_order' => 'integer',
        'is_active' => 'boolean',
        'icon' => 'string',
        'url' => 'string',
        'category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'feature_section_id' => 'required',
        'title' => 'required|string|max:100|string|max:100',
        'content' => 'string|max:100|nullable|max:100',
        'excerpt' => 'nullable|string|nullable|string',
        'redirect_to_page_id' => 'integer|nullable|integer',
        'description' => 'nullable|string|nullable|string',
        'image' => 'nullable|string|max:255|nullable|string|max:255',
        'sort_order' => 'nullable|integer|nullable|integer',
        'is_active' => 'required|boolean|boolean',
        'created_at' => 'nullable|nullable',
        'updated_at' => 'nullable|nullable',
        'icon' => 'nullable|string|nullable|string',
        'url' => 'nullable|string|max:255|nullable|string|max:255',
        'category_id' => 'nullable|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\Modules\YindulaCms\app\Models\Category::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function featureSection()
    {
        return $this->belongsTo(\Modules\YindulaCms\app\Models\FeatureSection::class, 'feature_section_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function page()
    {
        return $this->belongsTo(\Modules\YindulaCms\app\Models\Page::class, 'redirect_to_page_id');
    }

    public function getExternalUrlAttribute()
    {
        return redirect()->away($this->attributes['url']);
    }

    public function getReadableUrlAttribute()
    {
        return $this->attributes['url'];
    }

    public function getTruncatedDescriptionAttribute()
    {
        $body = Str::words($this->description, 20);
        return strip_tags($body);
    }

    public function hasField($value)
    {
        if ($value == true) {
            return true;
        }

        return false;
    }

    public static function getByFeatureSectionId($featureSection)
    {
        return static::where('feature_section_id', '=', $featureSection)->get();
    }

    /*
    |---------------------------------------------------------------------------------------------
    | Custom Attributes
    |---------------------------------------------------------------------------------------------
    */
    /**
     * This attribute converts the "created_at" field into a more readable attribute.
     * It does not have a corresponding column in the database. It can be accessed
     * using the "snake case", e.g. $game_participant->readable_created_at.
     * @return void
     */
    public function getIsActiveAttribute()
    {
        return ($this->attributes['is_active']) ? "Yes" : "No";
    }

    /**
     *
     * @return string
     */
    public function getImageUrlAttribute(): string
    {
        return ModelMediaHelper::getImageAttribute(self::tableName(), $this->id);
    }

    public function getRedirectToPageAttribute()
    {
        // Check if the feature has an associated page
        if ($this->page) {
            // Check if the "url" attribute is empty
            if (empty($this->attributes['url'])) {
                // Return the URL based on the associated page's slug
                return url($this->page->slug);
            } else {
                // Return the value of the "url" attribute
                return url($this->attributes['url']);
            }
        }

        // Return a default value or handle the case where there is no associated page
        return '';
    }
}
