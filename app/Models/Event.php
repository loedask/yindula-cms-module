<?php

namespace Modules\YindulaCms\app\Models;

use Eloquent as Model;

use Illuminate\Support\Str;
use App\Models\Event as BaseEvent;
use Illuminate\Support\Facades\App;
use Cviebrock\EloquentSluggable\Sluggable;
use Modules\YindulaCore\Utilities\DateTimeFormat;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\YindulaCms\app\Traits\UsesCmsDatabaseConnection;

class Event extends BaseEvent
{
    /*
    |---------------------------------------------------------------------------------------------
    | Custom Attributes
    |---------------------------------------------------------------------------------------------
    */

    /**
     * This attribute converts the "created_at" field into a more readable attribute.
     * It does not have a corresponding column in the database. It can be accessed
     * using the "snake case", e.g. $game_session->readable_created_at.
     * @return void
     */
    public function getEventDateAtAttribute()
    {
        return (new DateTimeFormat($this->created_at))->getReadable();
    }

    /**
     * This attribute converts the "created_at" field into a more readable attribute.
     * It does not have a corresponding column in the database. It can be accessed
     * using the "snake case", e.g. $game_session->readable_created_at.
     * @return void
     */
    public function getEventDayAttribute()
    {
        return (new DateTimeFormat($this->created_at))->getDay();
    }

    /**
     * This attribute converts the "created_at" field into a more readable attribute.
     * It does not have a corresponding column in the database. It can be accessed
     * using the "snake case", e.g. $game_session->readable_created_at.
     * @return void
     */
    public function getEventMonthAndYearAttribute()
    {
        return (new DateTimeFormat($this->created_at))->getMonthAndYear();
    }

    public function getTruncatedDescriptionAttribute()
    {
        $description = Str::words($this->description, 30);
        return strip_tags($description);
    }

    public function link()
    {
        return 'event/' . $this->slug;
    }

    public function getEvents()
    {
        return self::orderBy('created_at', 'DESC')->get();
    }

    public function findBySlug($slug)
    {
        try {
            return self::where('slug', $slug)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return json_encode(['error' => 'No query results for model']);
        }
    }
}
