<?php

namespace Modules\YindulaCms\app\Http\Observers\After;

use Modules\YindulaCms\app\Models\FeatureSection;

class FeatureSectionObserverAfter
{
    /**
     * Handle the modules institution entities guardian "updated" event.
     *
     * @param  \Modules\YindulaCms\app\Models\FeatureSection  $model
     * @return void
     */
    public function updated(FeatureSection $model)
    {
        //
    }

    /**
     * Handle the modules institution entities guardian "deleted" event.
     *
     * @param  \Modules\YindulaCms\app\Models\FeatureSection  $model
     * @return void
     */
    public function deleted(FeatureSection $model)
    {
        //
    }

    /**
     * Handle the modules institution entities guardian "restored" event.
     *
     * @param  \Modules\YindulaCms\app\Models\FeatureSection  $model
     * @return void
     */
    public function restored(FeatureSection $model)
    {
        //
    }

    /**
     * Handle the modules institution entities guardian "force deleted" event.
     *
     * @param  \Modules\YindulaCms\app\Models\FeatureSection  $model
     * @return void
     */
    public function forceDeleted(FeatureSection $model)
    {
        //
    }
}
