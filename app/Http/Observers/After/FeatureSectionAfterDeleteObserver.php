<?php

namespace Modules\YindulaCms\app\Http\Observers\After;

use Modules\YindulaCms\app\Models\FeatureSection;
use Modules\YindulaCms\app\Utilities\FeatureSectionContentUtility;

class FeatureSectionAfterDeleteObserver
{

    /**
     *
     * Handle the feature section "deleted" event.
     *
     * @param  \Modules\YindulaCms\app\Models\FeatureSection  $featureSection
     * @return void
     */
    public function deleted(FeatureSection $featureSection)
    {
        $content = new FeatureSectionContentUtility($featureSection);
        $content->delete();
    }
}
