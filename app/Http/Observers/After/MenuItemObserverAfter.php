<?php

namespace Modules\YindulaCms\app\Http\Observers\After;

use Modules\YindulaCms\app\Models\MenuItem;

class MenuItemObserverAfter
{
    /**
     * Handle the Menu Item Entity "created" event.
     *
     * @param  \Modules\YindulaCms\app\Models\MenuItem  $model
     * @return void
     */
    public function created(MenuItem $model)
    {
        $model->menu->removeMenuFromCache();
    }

    /**
     * Handle the Menu Item Entity "updated" event.
     *
     * @param  \Modules\YindulaCms\app\Models\MenuItem  $model
     * @return void
     */
    public function updated(MenuItem $model)
    {
        $model->menu->removeMenuFromCache();
    }

    /**
     * Handle the Menu Item Entity "deleted" event.
     *
     * @param  \Modules\YindulaCms\app\Models\MenuItem  $model
     * @return void
     */
    public function deleted(MenuItem $model)
    {
        $model->menu->removeMenuFromCache();
    }

    /**
     * Handle the Menu Item Entity "restored" event.
     *
     * @param  \Modules\YindulaCms\app\Models\MenuItem  $model
     * @return void
     */
    public function restored(MenuItem $model)
    {
        //
    }

    /**
     * Handle the Menu Item Entity "force deleted" event.
     *
     * @param  \Modules\YindulaCms\app\Models\MenuItem  $model
     * @return void
     */
    public function forceDeleted(MenuItem $model)
    {
        //
    }
}
