<?php

namespace Modules\YindulaCms\app\Http\Observers\After;

use Modules\YindulaCms\app\Models\FeatureSection;
use Modules\YindulaCms\app\Utilities\FeatureSectionContentUtility;

class FeatureSectionAfterSaveObserver
{

    /**
     * Handle the feature section "created" event.
     *
     * @param  \Modules\YindulaCms\app\Models\FeatureSection  $featureSection
     * @return void
     */
    public function created(FeatureSection $featureSection)
    {
        $content = new FeatureSectionContentUtility($featureSection);
        $content->save();
    }
}
