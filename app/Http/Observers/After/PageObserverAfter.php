<?php

namespace Modules\YindulaCms\app\Http\Observers\After;

use Modules\YindulaCms\app\Models\Page;
use Modules\YindulaCms\app\Repositories\MenuItemRepository;
use Modules\YindulaCms\app\Repositories\PageRepository;

class PageObserverAfter
{
    /**
     * Handle the modules institution entities guardian "created" event.
     *
     * @param  \Modules\Institution\Entities\Page  $model
     * @return void
     */
    public function created(Page $model)
    {
        if ($model->is_home) {
            app(PageRepository::class)->setHomePage($model->id);
        }
    }

    /**
     * Handle the modules institution entities guardian "updated" event.
     *
     * @param  \Modules\Institution\Entities\Page  $model
     * @return void
     */
    public function updated(Page $model)
    {
        if ($model->isDirty('is_home')) {
            app(PageRepository::class)->setHomePage($model->id);
        }

        if ($model->isDirty('slug')) {
            app(MenuItemRepository::class)->setMenuItemUrl($model->slug);
        }
    }

    /**
     * Handle the modules institution entities guardian "deleted" event.
     *
     * @param  \Modules\Institution\Entities\Page  $model
     * @return void
     */
    public function deleted(Page $model)
    {
    }

    /**
     * Handle the modules institution entities guardian "restored" event.
     *
     * @param  \Modules\Institution\Entities\Page  $model
     * @return void
     */
    public function restored(Page $model)
    {
        //
    }

    /**
     * Handle the modules institution entities guardian "force deleted" event.
     *
     * @param  \Modules\Institution\Entities\Page  $model
     * @return void
     */
    public function forceDeleted(Page $model)
    {
        //
    }
}
