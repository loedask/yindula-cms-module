<?php

namespace Modules\YindulaCms\app\Http\Observers\Before;

use Illuminate\Support\Facades\Auth;
use Modules\YindulaCms\app\Models\Page;

class PageObserverBefore
{
    /**
     * Handle the Page entity "saving" event.
     *
     * @param  \Modules\Institution\Entities\Page  $model
     * @return void
     */
    public function saving(Page $model)
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$model->author_id && Auth::user()) {
            $model->author_id = Auth::user()->getKey();
        }
    }
}
