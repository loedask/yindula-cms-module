<?php

namespace Modules\YindulaCms\app\Http\Controllers;

use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Utilities\Application;
use Modules\YindulaCms\app\Utilities\ContentTypes\CmsImage as Image;
use Modules\YindulaCms\app\DataTables\PostDataTable;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Http\Requests\CreatePostRequest;
use Modules\YindulaCms\app\Http\Requests\UpdatePostRequest;
use Modules\YindulaCms\app\Repositories\PostRepository;
use Modules\YindulaCore\Utilities\FilepondManager;
use Response;

class PostController extends CmsBaseController
{
    /** @var  PostRepository */
    private $postRepository;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepository = $postRepo;
    }

    /**
     * Display a listing of the Page.
     *
     * @return Response
     */
    public function index()
    {
        return view('yindulacms::posts.index');
    }


    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create()
    {
        return view('yindulacms::posts.create');
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $input = $request->all();

        $stored_file = new FilepondManager($request->image);


        if ($request->image) {
            $stored_file = new FilepondManager($request->image);

            $input['image'] = (new Image(
                $stored_file->getStoredFile(),
                $this->postRepository->modelName()
            ))->handle();
        }


        $post = $this->postRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/posts.singular')]));

        return redirect(route('cms.posts.index'));
    }

    /**
     * Display the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error(__('messages.not_found', ['model' => __('models/posts.singular')]));

            return redirect(route('cms.posts.index'));
        }

        return view('yindulacms::posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error(__('messages.not_found', ['model' => __('models/posts.singular')]));

            return redirect(route('cms.posts.index'));
        }

        return view('yindulacms::posts.edit')->with('post', $post);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  int              $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error(__('messages.not_found', ['model' => __('models/posts.singular')]));

            return redirect(route('cms.posts.index'));
        }


        $input = $request->all();


        if (isset($request->image)) {
            $stored_file = new FilepondManager($request->image);

            $input['image'] = (new Image(
                $stored_file->getStoredFile(),
                $this->postRepository->modelName()
            ))->handle();
        } else {
            $input['image'] = $post->image;
        }

        $post = $this->postRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/posts.singular')]));

        return redirect(route('cms.posts.index'));
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            Flash::error(__('messages.not_found', ['model' => __('models/posts.singular')]));

            return redirect(route('cms.posts.index'));
        }

        if (!is_null($post->image)) {
            (new Image(
                $post->image,
                $this->postRepository->modelName()
            ))->remove();
        }

        $this->postRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/posts.singular')]));

        return redirect(route('cms.posts.index'));
    }
}
