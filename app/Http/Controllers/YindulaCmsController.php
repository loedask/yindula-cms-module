<?php

namespace Modules\YindulaCms\app\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;

class YindulaCmsController extends CmsBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        return view('yindulacore::dashboard');
    }

    public function changeLanquage($lang)
    {
        $user       = Auth::user();
        $user->language = $lang;
        $user->save();

        return redirect()->back()->with('success', __('Language change successfully.'));
    }
}
