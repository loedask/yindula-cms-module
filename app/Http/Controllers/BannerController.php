<?php

namespace Modules\YindulaCms\app\Http\Controllers;

use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Http\Requests\CreateBannerRequest;
use Modules\YindulaCms\app\Http\Requests\UpdateBannerRequest;
use Modules\YindulaCms\app\Repositories\BannerRepository;
use Modules\YindulaCms\app\Utilities\ContentTypes\CmsImage as Image;
use Modules\YindulaCore\Utilities\FilepondManager;
use Response;

class BannerController extends CmsBaseController
{
    /** @var BannerRepository $bannerRepository*/
    private $bannerRepository;

    public function __construct(BannerRepository $bannerRepo)
    {
        $this->bannerRepository = $bannerRepo;
    }

    /**
     * Display a listing of the Banner.
     *
     *
     * @return Response
     */
    public function index()
    {
        return view('yindulacms::banners.index');
    }

    /**
     * Show the form for creating a new Banner.
     *
     * @return Response
     */
    public function create()
    {
        return view('yindulacms::banners.create');
    }

    /**
     * Store a newly created Banner in storage.
     *
     * @param CreateBannerRequest $request
     *
     * @return Response
     */
    public function store(CreateBannerRequest $request)
    {
        $input = $request->all();

        $stored_file = new FilepondManager($request->image);

        $input['image'] = (new Image(
            $stored_file->getStoredFile(),
            $this->bannerRepository->modelName()
        ))->handle();

        $banner = $this->bannerRepository->create($input);

        Flash::success('Banner saved successfully.');

        return redirect(route('cms.banners.index'));
    }

    /**
     * Display the specified Banner.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            Flash::error('Banner not found');

            return redirect(route('cms.banners.index'));
        }

        return view('yindulacms::banners.show')->with('banner', $banner);
    }

    /**
     * Show the form for editing the specified Banner.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            Flash::error('Banner not found');

            return redirect(route('cms.banners.index'));
        }

        return view('yindulacms::banners.edit')->with('banner', $banner);
    }

    /**
     * Update the specified Banner in storage.
     *
     * @param int $id
     * @param UpdateBannerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBannerRequest $request)
    {
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            Flash::error('Banner not found');

            return redirect(route('cms.banners.index'));
        }

        $input = $request->all();

        if ($request->image) {
            $stored_file = new FilepondManager($request->image);

            $input['image'] = (new Image(
                $stored_file->getStoredFile(),
                $this->bannerRepository->modelName()
            ))->handle();
        } else {
            $input['image'] = $banner->image;
        }


        $banner = $this->bannerRepository->update($input, $id);

        Flash::success('Banner updated successfully.');

        return redirect(route('cms.banners.index'));
    }

    /**
     * Remove the specified Banner from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $banner = $this->bannerRepository->find($id);

        if (empty($banner)) {
            Flash::error('Banner not found');

            return redirect(route('cms.banners.index'));
        }

        $this->bannerRepository->delete($id);

        Flash::success('Banner deleted successfully.');

        return redirect(route('cms.banners.index'));
    }
}
