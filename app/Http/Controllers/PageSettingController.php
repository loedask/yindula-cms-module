<?php

namespace Modules\YindulaCms\app\Http\Controllers;

use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Repositories\PageRepository;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Repositories\PageSettingRepository;
use Modules\YindulaCms\app\Http\Requests\CreatePageSettingRequest;
use Modules\YindulaCms\app\Http\Requests\UpdatePageSettingRequest;

class PageSettingController extends CmsBaseController
{
    /** @var PageSettingRepository $pageSettingRepository*/
    private $pageSettingRepository;

    /** @var PageRepository $pageRepository*/
    private $pageRepository;

    public function __construct(PageSettingRepository $pageSettingRepo, PageRepository $pageRepo)
    {
        $this->pageSettingRepository = $pageSettingRepo;
        $this->pageRepository = $pageRepo;
    }

    /**
     * Redirect to PageSetting Index.
     *
     * @return Redirector
     */
    private function redirectToIndex()
    {
        return redirect(route('cms.pageSettings.index'));
    }

    private function findPageSetting($id)
    {
        $page_setting = $this->pageSettingRepository->find($id);

        if (empty($page_setting)) {
            Flash::error('Page Setting not found');

            $this->redirectToIndex();
        }

        return $page_setting;
    }

    private function findPage($id)
    {
        $page = $this->pageRepository->find($id);

        if (empty($page)) {
            Flash::error(__('messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('cms.pages.index'));
        }

        return $page;
    }

    /**
     * Redirect based on the presence of page ID in the current URL.
     *
     * @return Redirector
     */
    private function redirectToPageShow($pageId)
    {
        // Redirect to the current page ID
        return redirect()->route('cms.pages.show', ['page' => $pageId]);
    }

    /**
     * Display a listing of the PageSetting.
     */
    public function index(Request $request)
    {
        return view('yindulacms::page_settings.index');
    }

    /**
     * Show the form for creating a new PageSetting.
     */
    public function create()
    {
        return view('yindulacms::page_settings.create');
    }

    public function createOrEdit($pageId)
    {
        // Check if the page with the given ID exists
        $page_setting = $this->findPageSetting($pageId);

        // If the page exists, redirect to the edit page
        if ($page_setting) {
            return redirect()->route('cms.pageSettings.edit', ['pageSetting' => $pageId]);
        }

        return redirect()->route('cms.pageSettings.add', ['pageId' => $pageId]);
    }

    /**
     * Show the form for creating a new PageSetting.
     */
    public function add($pageId)
    {
        // Check if the page with the given ID exists
        $page = $this->findPage($pageId);

        // If the page doesn't exist, create a new page
        return view('yindulacms::page_settings.create')->with('page', $page);
    }

    /**
     * Store a newly created PageSetting in storage.
     */
    public function store(CreatePageSettingRequest $request)
    {
        $input = $request->all();

        $page_setting = $this->pageSettingRepository->create($input);

        Flash::success('Page Setting saved successfully.');

        return $this->redirectToPageShow($page_setting->page_id);
    }

    /**
     * Display the specified PageSetting.
     */
    public function show($id)
    {
        $page_setting = $this->findPageSetting($id);

        return view('yindulacms::page_settings.show')->with('pageSetting', $page_setting);
    }

    /**
     * Show the form for editing the specified PageSetting.
     */
    public function edit($id)
    {
        $page_setting = $this->findPageSetting($id);

        // Check if the page with the given ID exists
        $page = $this->findPage($page_setting->page_id);

        return view('yindulacms::page_settings.edit', compact('page_setting', 'page'));
    }

    /**
     * Update the specified PageSetting in storage.
     */
    public function update($id, UpdatePageSettingRequest $request)
    {
        $this->findPageSetting($id);

        $page_setting = $this->pageSettingRepository->update($request->all(), $id);

        Flash::success('Page Setting updated successfully.');

        return $this->redirectToPageShow($page_setting->page_id);
    }

    /**
     * Remove the specified PageSetting from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $page_setting = $this->findPageSetting($id);

        $this->pageSettingRepository->delete($id);

        Flash::success('Page Setting deleted successfully.');

        return $this->redirectToIndex();
    }
}
