<?php

namespace Modules\YindulaCms\app\Http\Controllers;


use Response;
use Laracasts\Flash\Flash;
use Modules\YindulaCore\Utilities\FilepondManager;
use Modules\YindulaCms\app\Repositories\PageRepository;
use Modules\YindulaCms\app\Repositories\FeatureRepository;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Http\Requests\CreateFeatureRequest;
use Modules\YindulaCms\app\Http\Requests\UpdateFeatureRequest;
use Modules\YindulaCms\app\Repositories\FeatureSectionRepository;
use Modules\YindulaCms\app\Utilities\ContentTypes\CmsImage as Image;

class FeatureController extends CmsBaseController
{
    /** @var  FeatureRepository */
    private $featureRepository;

    /** @var  FeatureSectionRepository */
    private $featureSectionRepository;

    /** @var  PageRepository */
    private $pageRepository;

    public function __construct(
        FeatureRepository $featureRepo,
        FeatureSectionRepository $featureSectionRepo,
        PageRepository $pageRepo
    ) {
        $this->featureSectionRepository = $featureSectionRepo;
        $this->pageRepository = $pageRepo;
        $this->featureRepository = $featureRepo;
        $this->featureSectionRepository = $featureSectionRepo;
    }



    /**
     * Redirect based on the presence of page ID in the current URL.
     *
     * @return Redirector
     */
    private function redirectToPageShow($feature)
    {
        $feature_section = $this->featureSectionRepository->find($feature->feature_section_id);

        // Redirect to the current page ID
        return redirect()->route('cms.pages.show', ['page' => $feature_section->page->id]);
    }

    /**
     * Redirect to the 'cms.features.index' route.
     *
     * @return Redirector
     */
    private function redirectToFeaturesIndex()
    {
        return redirect()->route('cms.features.index');
    }

    /**
     * Display a listing of the Page.
     *
     * @return Response
     */
    public function index()
    {
        return view('yindulacms::features.index');
    }

    /**
     * Show the form for creating a new Feature.
     *
     * @return Response
     */
    public function create()
    {
        return view('yindulacms::features.create');
    }

    /**
     * Show the form for creating a new FeatureSection.
     *
     * @return Response
     */
    public function add($pageId)
    {
        $page = $this->pageRepository->find($pageId);
        return view('yindulacms::features.create')->with('page', $page);
    }

    /**
     * Store a newly created Feature in storage.
     *
     * @param CreateFeatureRequest $request
     *
     * @return Response
     */
    public function store(CreateFeatureRequest $request)
    {
        $input = $request->all();

        $stored_file = new FilepondManager($request->image);

        $input['image'] = (new Image(
            $stored_file->getStoredFile(),
            $this->featureRepository->modelName()
        ))->handle();

        $feature = $this->featureRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/features.singular')]));

        return $this->redirectToPageShow($feature);
    }

    /**
     * Display the specified Feature.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $feature = $this->featureRepository->find($id);

        if (empty($feature)) {
            Flash::error(__('messages.not_found', ['model' => __('models/features.singular')]));

            return $this->redirectToFeaturesIndex();
        }

        return view('yindulacms::features.show')->with('feature', $feature);
    }

    /**
     * Show the form for editing the specified Feature.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $feature = $this->featureRepository->find($id);

        if (empty($feature)) {
            Flash::error(__('messages.not_found', ['model' => __('models/features.singular')]));

            return $this->redirectToFeaturesIndex();
        }

        return view('yindulacms::features.edit')->with('feature', $feature);
    }

    /**
     * Update the specified Feature in storage.
     *
     * @param  int              $id
     * @param UpdateFeatureRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFeatureRequest $request)
    {
        $feature = $this->featureRepository->find($id);

        if (empty($feature)) {
            Flash::error(__('messages.not_found', ['model' => __('models/features.singular')]));

            return $this->redirectToFeaturesIndex();
        }


        $input = $request->all();


        if ($request->image) {
            $stored_file = new FilepondManager($request->image);

            $input['image'] = (new Image(
                $stored_file->getStoredFile(),
                $this->featureRepository->modelName()
            ))->handle();
        } else {
            $input['image'] = $feature->image;
        }

        $feature = $this->featureRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/features.singular')]));

        return $this->redirectToFeaturesIndex();
    }

    /**
     * Remove the specified Feature from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $feature = $this->featureRepository->find($id);

        if (empty($feature)) {
            Flash::error(__('messages.not_found', ['model' => __('models/features.singular')]));

            return $this->redirectToFeaturesIndex();
        }

        $this->featureRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/features.singular')]));

        return $this->redirectToFeaturesIndex();
    }
}
