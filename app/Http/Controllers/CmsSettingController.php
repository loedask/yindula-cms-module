<?php

namespace Modules\YindulaCms\app\Http\Controllers;

use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Http\Requests\CreateCmsSettingRequest;
use Modules\YindulaCms\app\Http\Requests\UpdateCmsSettingRequest;
use Modules\YindulaCms\app\DataTables\CmsSettingDataTable;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Repositories\CmsSettingRepository;
use Response;

class CmsSettingController extends CmsBaseController
{
    /** @var CmsSettingRepository $cmsSettingRepository*/
    private $cmsSettingRepository;

    public function __construct(CmsSettingRepository $cmsSettingRepo)
    {
        $this->cmsSettingRepository = $cmsSettingRepo;
    }

    /**
     * Display a listing of the Page.
     *
     * @return Response
     */
    public function index()
    {
        return view('yindulacms::cms_settings.index');
    }

    /**
     * Show the form for creating a new CmsSetting.
     *
     * @return Response
     */
    public function create()
    {
        return view('yindulacms::cms_settings.create');
    }

    /**
     * Store a newly created CmsSetting in storage.
     *
     * @param CreateCmsSettingRequest $request
     *
     * @return Response
     */
    public function store(CreateCmsSettingRequest $request)
    {
        $input = $request->all();

        $cmsSetting = $this->cmsSettingRepository->create($input);

        Flash::success('Cms Setting saved successfully.');

        return redirect(route('cms.cmsSettings.index'));
    }

    /**
     * Display the specified CmsSetting.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cmsSetting = $this->cmsSettingRepository->find($id);

        if (empty($cmsSetting)) {
            Flash::error('Cms Setting not found');

            return redirect(route('cms.cmsSettings.index'));
        }

        return view('yindulacms::cms_settings.show')->with('cmsSetting', $cmsSetting);
    }

    /**
     * Show the form for editing the specified CmsSetting.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cmsSetting = $this->cmsSettingRepository->find($id);

        if (empty($cmsSetting)) {
            Flash::error('Cms Setting not found');

            return redirect(route('cms.cmsSettings.index'));
        }

        return view('yindulacms::cms_settings.edit')->with('cmsSetting', $cmsSetting);
    }

    /**
     * Update the specified CmsSetting in storage.
     *
     * @param int $id
     * @param UpdateCmsSettingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCmsSettingRequest $request)
    {
        $cmsSetting = $this->cmsSettingRepository->find($id);

        if (empty($cmsSetting)) {
            Flash::error('Cms Setting not found');

            return redirect(route('cms.cmsSettings.index'));
        }

        $cmsSetting = $this->cmsSettingRepository->update($request->all(), $id);

        Flash::success('Cms Setting updated successfully.');

        return redirect(route('cms.cmsSettings.index'));
    }

    /**
     * Remove the specified CmsSetting from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cmsSetting = $this->cmsSettingRepository->find($id);

        if (empty($cmsSetting)) {
            Flash::error('Cms Setting not found');

            return redirect(route('cms.cmsSettings.index'));
        }

        $this->cmsSettingRepository->delete($id);

        Flash::success('Cms Setting deleted successfully.');

        return redirect(route('cms.cmsSettings.index'));
    }
}
