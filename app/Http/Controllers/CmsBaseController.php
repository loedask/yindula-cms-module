<?php

namespace Modules\YindulaCms\app\Http\Controllers;

use Modules\YindulaCore\app\Http\Controllers\BaseController;

/**
 * This class should be parent class for the CmsBaseController
 *
 * Class CmsBaseController
 */
class CmsBaseController extends BaseController
{

}
