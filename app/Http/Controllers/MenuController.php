<?php
namespace Modules\YindulaCms\app\Http\Controllers;

use Flash;
use Modules\Cms\Http\Requests;
use Modules\YindulaCms\app\Http\Requests\CreateMenuRequest;
use Modules\YindulaCms\app\Http\Requests\UpdateMenuRequest;
use Modules\YindulaCms\app\DataTables\MenuDataTable;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Repositories\MenuRepository;
use Response;

class MenuController extends CmsBaseController
{
    /** @var  MenuRepository */
    private $menuRepository;

    public function __construct(MenuRepository $menuRepo)
    {
        $this->menuRepository = $menuRepo;
    }


    /**
     * Display a listing of the Page.
     *
     * @return Response
     */
    public function index()
    {
        return view('yindulacms::menus.index');
    }

    /**
     * Show the form for creating a new Menu.
     *
     * @return Response
     */
    public function create()
    {
        return view('yindulacms::menus.create');
    }

    /**
     * Store a newly created Menu in storage.
     *
     * @param CreateMenuRequest $request
     *
     * @return Response
     */
    public function store(CreateMenuRequest $request)
    {
        $input = $request->all();

        $menu = $this->menuRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/menus.singular')]));

        return redirect(route('menus.index'));
    }

    /**
     * Display the specified Menu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $menu = $this->menuRepository->find($id);

        if (empty($menu)) {
            Flash::error(__('messages.not_found', ['model' => __('models/menus.singular')]));

            return redirect(route('menus.index'));
        }

        return view('yindulacms::menus.show')->with('menu', $menu);
    }

    /**
     * Show the form for editing the specified Menu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $menu = $this->menuRepository->find($id);

        if (empty($menu)) {
            Flash::error(__('messages.not_found', ['model' => __('models/menus.singular')]));

            return redirect(route('menus.index'));
        }

        return view('yindulacms::menus.edit')->with('menu', $menu);
    }

    /**
     * Update the specified Menu in storage.
     *
     * @param  int              $id
     * @param UpdateMenuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMenuRequest $request)
    {
        $menu = $this->menuRepository->find($id);

        if (empty($menu)) {
            Flash::error(__('messages.not_found', ['model' => __('models/menus.singular')]));

            return redirect(route('menus.index'));
        }

        $menu = $this->menuRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/menus.singular')]));

        return redirect(route('menus.index'));
    }

    /**
     * Remove the specified Menu from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $menu = $this->menuRepository->find($id);

        if (empty($menu)) {
            Flash::error(__('messages.not_found', ['model' => __('models/menus.singular')]));

            return redirect(route('cms.menus.index'));
        }

        $this->menuRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/menus.singular')]));

        return redirect(route('cms.menus.index'));
    }
}
