<?php

namespace Modules\YindulaCms\app\Http\Controllers;


use Response;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use Modules\YindulaCore\Utilities\FilepondManager;
use Modules\YindulaCms\app\Repositories\PageRepository;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Repositories\FeatureSectionRepository;
use Modules\YindulaCms\app\Utilities\ContentTypes\CmsImage as Image;
use Modules\YindulaCms\app\Http\Requests\CreateFeatureSectionRequest;
use Modules\YindulaCms\app\Http\Requests\UpdateFeatureSectionRequest;

class FeatureSectionController extends CmsBaseController
{
    /** @var  FeatureSectionRepository */
    private $featureSectionRepository;

    /** @var  PageRepository */
    private $pageRepository;

    public function __construct(FeatureSectionRepository $featureSectionRepo, PageRepository $pageRepo)
    {
        $this->featureSectionRepository = $featureSectionRepo;
        $this->pageRepository = $pageRepo;
    }

    /**
     * Redirect based on the presence of page ID in the current URL.
     *
     * @return Redirector
     */
    private function redirectToPageOrIndex($pageId = null)
    {
        /* // Check if there is a page ID in the current URL
        $currentPageId = Request::route('pageId');

        // if (Route::currentRouteName() == 'cms.featureSections.add') {
        if ($currentPageId) { */

            // Redirect to the current page ID
            return redirect()->route('cms.pages.show', ['page' => $pageId]);
            
        // } else {
        //     // Redirect to cms.featureSections.index if no page ID is present
        //     return redirect()->route('cms.featureSections.index');
        // }
    }

    /**
     * Display a listing of the FeatureSection.
     *
     * @return Response
     */
    public function index()
    {
        return view('yindulacms::feature_sections.index');
    }

    /**
     * Show the form for creating a new FeatureSection.
     *
     * @return Response
     */
    public function create()
    {
        return view('yindulacms::feature_sections.create');
    }

    /**
     * Show the form for creating a new FeatureSection.
     *
     * @return Response
     */
    public function add($pageId)
    {
        $page = $this->pageRepository->find($pageId);
        return view('yindulacms::feature_sections.create')->with('page', $page);
    }

    /**
     * Store a newly created FeatureSection in storage.
     *
     * @param CreateFeatureSectionRequest $request
     *
     * @return Response
     */
    public function store(CreateFeatureSectionRequest $request)
    {
        $input = $request->all();

        $stored_file = new FilepondManager($request->image);

        $input['image'] = (new Image(
            $stored_file->getStoredFile(),
            $this->featureSectionRepository->modelName()
        ))->handle();

        $featureSection = $this->featureSectionRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/featureSections.singular')]));

        // Use the method to redirect based on the page ID
        return $this->redirectToPageOrIndex($input['page_id']);
    }

    /**
     * Display the specified FeatureSection.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $featureSection = $this->featureSectionRepository->find($id);

        if (empty($featureSection)) {
            Flash::error(__('messages.not_found', ['model' => __('models/featureSections.singular')]));

            return redirect(route('cms.featureSections.index'));
        }

        return view('yindulacms::feature_sections.show')->with('featureSection', $featureSection);
    }

    /**
     * Show the form for editing the specified FeatureSection.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $featureSection = $this->featureSectionRepository->find($id);

        if (empty($featureSection)) {
            Flash::error(__('messages.not_found', ['model' => __('models/featureSections.singular')]));

            return redirect(route('cms.featureSections.index'));
        }

        return view('yindulacms::feature_sections.edit')->with('featureSection', $featureSection);
    }

    /**
     * Update the specified FeatureSection in storage.
     *
     * @param  int              $id
     * @param UpdateFeatureSectionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFeatureSectionRequest $request)
    {
        $featureSection = $this->featureSectionRepository->find($id);

        if (empty($featureSection)) {
            Flash::error(__('messages.not_found', ['model' => __('models/featureSections.singular')]));

            return redirect(route('cms.featureSections.index'));
        }


        $input = $request->all();


        if (isset($request->image)) {
            $stored_file = new FilepondManager($request->image);

            $input['image'] = (new Image(
                $stored_file->getStoredFile(),
                $this->featureSectionRepository->modelName()
            ))->handle();
        } else {
            $input['image'] = $featureSection->image;
        }

        $featureSection = $this->featureSectionRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/featureSections.singular')]));

        return redirect(route('cms.featureSections.index'));
    }

    /**
     * Remove the specified FeatureSection from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $featureSection = $this->featureSectionRepository->find($id);

        if (empty($featureSection)) {
            Flash::error(__('messages.not_found', ['model' => __('models/featureSections.singular')]));

            return redirect(route('cms.featureSections.index'));
        }

        $this->featureSectionRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/featureSections.singular')]));

        return redirect(route('cms.featureSections.index'));
    }
}
