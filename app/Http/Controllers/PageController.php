<?php

namespace Modules\YindulaCms\app\Http\Controllers;


use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Http\Requests\CreatePageRequest;
use Modules\YindulaCms\app\Http\Requests\UpdatePageRequest;
use Modules\YindulaCms\app\Repositories\PageRepository;
use Modules\YindulaCms\app\Repositories\PageSettingRepository;
use Modules\YindulaCms\app\Utilities\ContentTypes\CmsImage as Image;
use Modules\YindulaCore\Utilities\FilepondManager;
use Response;

class PageController extends CmsBaseController
{
    /* @var  PageRepository $pageRepository*/
    private $pageRepository;

    /** @var PageSettingRepository $pageSettingRepository*/
    private $pageSettingRepository;

    public function __construct(PageRepository $pageRepo, PageSettingRepository $pageSettingRepo)
    {
        $this->pageRepository = $pageRepo;
        $this->pageSettingRepository = $pageSettingRepo;
    }

    private function findPage($id)
    {
        $page = $this->pageRepository->find($id);

        if (empty($page)) {
            Flash::error(__('messages.not_found', ['model' => __('models/pages.singular')]));

            return redirect(route('cms.pages.index'));
        }

        return $page;
    }

    private function findPageSetting($id)
    {
        $page_setting = $this->pageSettingRepository->find($id);

        if (empty($page_setting)) {
            Flash::error('Page Setting not found');

            $this->redirectToIndex();
        }

        return $page_setting;
    }

    /**
     * Redirect to PageSetting Index.
     *
     * @return Redirector
     */
    private function redirectToIndex()
    {
        return redirect(route('cms.pages.index'));
    }

    /**
     * Redirect based on the presence of page ID in the current URL.
     *
     * @return Redirector
     */
    private function redirectToShow($pageId)
    {
        // Redirect to the current page ID
        return redirect()->route('cms.pages.show', ['page' => $pageId]);
    }

    /**
     * Display a listing of the Page.
     *
     * @return Response
     */
    public function index()
    {
        return view('yindulacms::pages.index');
    }

    /**
     * Show the form for creating a new Page.
     *
     * @return Response
     */
    public function create()
    {
        return view('yindulacms::pages.create');
    }

    /**
     * Store a newly created Page in storage.
     *
     * @param CreatePageRequest $request
     *
     * @return Response
     */
    public function store(CreatePageRequest $request)
    {
        $input = $request->all();

        $stored_file = new FilepondManager($request->image);

        $input['image'] = (new Image(
            $stored_file->getStoredFile(),
            $this->pageRepository->modelName()
        ))->handle();

        $page = $this->pageRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/pages.singular')]));

        return redirect(route('cms.pages.index'));
    }

    /**
     * Display the specified Page.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $page = $this->findPage($id);

        $page_setting = $this->findPageSetting($id);

        return view(
            'yindulacms::pages.show',
            compact('page_setting', 'page')
        );
    }

    /**
     * Show the form for editing the specified Page.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $page = $this->findPage($id);

        $page_setting = $this->findPageSetting($id);

        return view('yindulacms::pages.edit', compact('page_setting', 'page'));
    }

    /**
     * Update the specified Page in storage.
     *
     * @param  int              $id
     * @param UpdatePageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageRequest $request)
    {
        $page = $this->findPage($id);

        $input = $request->all();


        if (isset($request->image)) {
            $stored_file = new FilepondManager($request->image);

            $input['image'] = (new Image(
                $stored_file->getStoredFile(),
                $this->pageRepository->modelName()
            ))->handle();
        } else {
            $input['image'] = $page->image;
        }

        $page = $this->pageRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/pages.singular')]));

        return redirect(route('cms.pages.index'));
    }

    /**
     * Remove the specified Page from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $page = $this->findPage($id);

        $this->pageRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/pages.singular')]));

        return redirect(route('cms.pages.index'));
    }

    /**
     * Remove the specified AcademicYear from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function setHomePage($id)
    {
        $page = $this->pageRepository->setHomePage($id);

        Flash::success(__('messages.deleted', ['model' => __('models/pages.singular')]));

        return redirect(route('cms.pages.index'));
    }

    /**
     * Remove the specified AcademicYear from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function setContactPage($id)
    {
        $page = $this->pageRepository->setContactPage($id);

        Flash::success(__('messages.deleted', ['model' => __('models/pages.singular')]));

        return redirect(route('cms.pages.index'));
    }
}
