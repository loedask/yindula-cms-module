<?php

namespace Modules\YindulaCms\app\Http\Controllers;

use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Utilities\ContentTypes\CmsImage;
use Modules\YindulaCms\app\Http\Requests\CreateCmsEventRequest;
use Modules\YindulaCms\app\Http\Requests\UpdateCmsEventRequest;
use Modules\YindulaCms\app\Repositories\CmsEventRepository;
use Modules\YindulaCore\Http\Controllers\BaseController;
use Modules\YindulaCore\Utilities\FilepondManager;

class CmsEventController extends CmsBaseController
{
    /** @var CmsEventRepository $cmsEventRepository*/
    private $cmsEventRepository;

    public function __construct(CmsEventRepository $cmsEventRepo)
    {
        $this->cmsEventRepository = $cmsEventRepo;
    }

    /**
     * Display a listing of the CmsEvent.
     */
    public function index()
    {
        return view('yindulacms::cms_events.index');
    }

    /**
     * Show the form for creating a new CmsEvent.
     */
    public function create()
    {
        return view('yindulacms::cms_events.create');
    }

    /**
     * Store a newly created CmsEvent in storage.
     */
    public function store(CreateCmsEventRequest $request)
    {
        $input = $request->all();

        $stored_file = new FilepondManager($request->image);

        $input['image'] = (new CmsImage(
            $stored_file->getStoredFile(),
            $this->cmsEventRepository->modelName()
        ))->handle();

        $cmsEvent = $this->cmsEventRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/cmsEvents.singular')]));

        return redirect(route('cms.cmsEvents.index'));
    }

    /**
     * Display the specified CmsEvent.
     */
    public function show($id)
    {
        $cmsEvent = $this->cmsEventRepository->find($id);

        if (empty($cmsEvent)) {
            Flash::error(__('models/cmsEvents.singular') . ' ' . __('messages.not_found'));

            return redirect(route('cms.cmsEvents.index'));
        }

        return view('yindulacms::cms_events.show')->with('cmsEvent', $cmsEvent);
    }

    /**
     * Show the form for editing the specified CmsEvent.
     */
    public function edit($id)
    {
        $cmsEvent = $this->cmsEventRepository->find($id);

        if (empty($cmsEvent)) {
            Flash::error(__('models/cmsEvents.singular') . ' ' . __('messages.not_found'));

            return redirect(route('cms.cmsEvents.index'));
        }

        return view('yindulacms::cms_events.edit')->with('cmsEvent', $cmsEvent);
    }

    /**
     * Update the specified CmsEvent in storage.
     */
    public function update($id, UpdateCmsEventRequest $request)
    {
        $cmsEvent = $this->cmsEventRepository->find($id);

        if (empty($cmsEvent)) {
            Flash::error(__('models/cmsEvents.singular') . ' ' . __('messages.not_found'));

            return redirect(route('cms.cmsEvents.index'));
        }


        $input = $request->all();


        if ($request->image) {
            $stored_file = new FilepondManager($request->image);

            $input['image'] = (new CmsImage(
                $stored_file->getStoredFile(),
                $this->cmsEventRepository->modelName()
            ))->handle();
        } else {
            $input['image'] = $cmsEvent->image;
        }

        $cmsEvent = $this->cmsEventRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/cmsEvents.singular')]));

        return redirect(route('cms.cmsEvents.index'));
    }

    /**
     * Remove the specified CmsEvent from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $cmsEvent = $this->cmsEventRepository->find($id);

        if (empty($cmsEvent)) {
            Flash::error(__('models/cmsEvents.singular') . ' ' . __('messages.not_found'));

            return redirect(route('cms.cmsEvents.index'));
        }

        $this->cmsEventRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/cmsEvents.singular')]));

        return redirect(route('cms.cmsEvents.index'));
    }
}
