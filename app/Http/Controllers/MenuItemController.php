<?php
namespace Modules\YindulaCms\app\Http\Controllers;


use Flash;
use Modules\YindulaCms\app\Http\Controllers\CmsBaseController;
use Modules\YindulaCms\app\Http\Requests\CreateMenuItemRequest;
use Modules\YindulaCms\app\Http\Requests\UpdateMenuItemRequest;
use Modules\YindulaCms\app\Repositories\MenuItemRepository;
use Response;

class MenuItemController extends CmsBaseController
{
    /** @var  MenuItemRepository */
    private $menuItemRepository;

    public function __construct(MenuItemRepository $menuItemRepo)
    {
        $this->menuItemRepository = $menuItemRepo;
    }

    /**
     * Display a listing of the MenuItem.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $menuItems = $this->menuItemRepository->all();

        return view('menu_items.index')
            ->with('menuItems', $menuItems);
    }

    /**
     * Show the form for creating a new MenuItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('menu_items.create');
    }

    /**
     * Store a newly created MenuItem in storage.
     *
     * @param CreateMenuItemRequest $request
     *
     * @return Response
     */
    public function store(CreateMenuItemRequest $request)
    {
        $input = $request->all();

        $menuItem = $this->menuItemRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/menuItems.singular')]));

        return redirect(route('menuItems.index'));
    }

    /**
     * Display the specified MenuItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $menuItem = $this->menuItemRepository->find($id);

        if (empty($menuItem)) {
            Flash::error(__('messages.not_found', ['model' => __('models/menuItems.singular')]));

            return redirect(route('menuItems.index'));
        }

        return view('menu_items.show')->with('menuItem', $menuItem);
    }

    /**
     * Show the form for editing the specified MenuItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $menuItem = $this->menuItemRepository->find($id);

        if (empty($menuItem)) {
            Flash::error(__('messages.not_found', ['model' => __('models/menuItems.singular')]));

            return redirect(route('menuItems.index'));
        }

        return view('menu_items.edit')->with('menuItem', $menuItem);
    }

    /**
     * Update the specified MenuItem in storage.
     *
     * @param int $id
     * @param UpdateMenuItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMenuItemRequest $request)
    {
        $menuItem = $this->menuItemRepository->find($id);

        if (empty($menuItem)) {
            Flash::error(__('messages.not_found', ['model' => __('models/menuItems.singular')]));

            return redirect(route('menuItems.index'));
        }

        $menuItem = $this->menuItemRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/menuItems.singular')]));

        return redirect(route('menuItems.index'));
    }

    /**
     * Remove the specified MenuItem from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $menuItem = $this->menuItemRepository->find($id);

        if (empty($menuItem)) {
            Flash::error(__('messages.not_found', ['model' => __('models/menuItems.singular')]));

            return redirect(route('menuItems.index'));
        }

        $this->menuItemRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/menuItems.singular')]));

        return redirect(route('menuItems.index'));
    }
}
