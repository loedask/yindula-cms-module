
<?php

use Modules\YindulaCms\app\Repositories\CmsSettingRepository;

if (!function_exists('yindula_setting')) {
    function yindula_setting($type, $key)
    {
        return app(CmsSettingRepository::class)->findValueByTypeAndKey($type, $key);
    }
}
