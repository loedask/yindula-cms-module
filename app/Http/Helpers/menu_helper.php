<?php

use Modules\YindulaCms\app\Models\Menu;

if (!function_exists('yindula_menu')) {
    function yindula_menu($menuName, $type = null, array $options = [])
    {
        return (new Menu())->display($menuName, $type, $options);
    }
}
