<?php

use Illuminate\Support\Facades\Storage;
use Modules\FrontPages\Utilities\HandleUrlPrefix;


if (!function_exists('yindula_image')) {
    function yindula_image($file, $default = "")
    {
        $path_prefix = (new HandleUrlPrefix())->handle();

        $storage_prefix = "storage/";

        if (!empty($file)) {
            return str_replace('\\', '/', asset($path_prefix . 'yindula-cms' . DIRECTORY_SEPARATOR . $file));
        }
        return $default;
    }
}



if (!function_exists('yindula_static_image')) {
    function yindula_static_image($file, $default = "")
    {
        $path_prefix = (new HandleUrlPrefix())->handle();

        $storage_prefix = "storage/";

        if (!empty($file)) {
            return str_replace('\\', '/', asset($path_prefix . $storage_prefix  . $file));
        }
        return $default;
    }
}
