<?php

namespace Modules\YindulaCms\app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\YindulaCms\app\Models\Feature;

class CreateFeatureRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Feature::$rules;
    }
}
