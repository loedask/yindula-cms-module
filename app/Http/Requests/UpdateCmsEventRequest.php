<?php

namespace Modules\YindulaCms\app\Http\Requests;

use Modules\YindulaCms\app\Models\CmsEvent;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCmsEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = CmsEvent::$rules;

        return $rules;
    }
}
