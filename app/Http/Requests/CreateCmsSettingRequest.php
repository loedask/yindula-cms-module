<?php

namespace Modules\YindulaCms\app\Http\Requests;

use Modules\YindulaCms\app\Models\CmsSetting;
use Illuminate\Foundation\Http\FormRequest;

class CreateCmsSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return CmsSetting::$rules;
    }
}
