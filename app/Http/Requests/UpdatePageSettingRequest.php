<?php

namespace Modules\YindulaCms\app\Http\Requests;

use Modules\YindulaCms\app\Models\PageSetting;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePageSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = PageSetting::$rules;

        return $rules;
    }
}
