<?php

namespace Modules\YindulaCms\app\DataTables;

use Modules\YindulaCms\app\Models\Post;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PostDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'yindulacms::posts.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \Modules\YindulaCms\app\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Post $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                       'extend' => 'create',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-plus"></i> ' .__('auth.app.create').''
                    ],
                    [
                       'extend' => 'export',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-download"></i> ' .__('auth.app.export').''
                    ],
                    [
                       'extend' => 'print',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-print"></i> ' .__('auth.app.print').''
                    ],
                    [
                       'extend' => 'reset',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-undo"></i> ' .__('auth.app.reset').''
                    ],
                    [
                       'extend' => 'reload',
                       'className' => 'btn btn-default btn-sm no-corner',
                       'text' => '<i class="fa fa-refresh"></i> ' .__('auth.app.reload').''
                    ],
                ],
                 'language' => [
                   'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                 ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'category_id' => new Column(['title' => __('models/posts.fields.category_id'), 'data' => 'category_id']),
            'title' => new Column(['title' => __('models/posts.fields.title'), 'data' => 'title']),
            'seo_title' => new Column(['title' => __('models/posts.fields.seo_title'), 'data' => 'seo_title']),
            // 'excerpt' => new Column(['title' => __('models/posts.fields.excerpt'), 'data' => 'excerpt']),
            // 'body' => new Column(['title' => __('models/posts.fields.body'), 'data' => 'body']),
            // 'image' => new Column(['title' => __('models/posts.fields.image'), 'data' => 'image']),
            // 'slug' => new Column(['title' => __('models/posts.fields.slug'), 'data' => 'slug']),
            // 'meta_description' => new Column(['title' => __('models/posts.fields.meta_description'), 'data' => 'meta_description']),
            // 'meta_keywords' => new Column(['title' => __('models/posts.fields.meta_keywords'), 'data' => 'meta_keywords']),
            'status' => new Column(['title' => __('models/posts.fields.status'), 'data' => 'status']),
            'featured' => new Column(['title' => __('models/posts.fields.featured'), 'data' => 'featured']),
            'author_id' => new Column(['title' => __('models/posts.fields.author_id'), 'data' => 'author_id'])
        ];
    }

   /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'posts_datatable_' . time();
    }
}
