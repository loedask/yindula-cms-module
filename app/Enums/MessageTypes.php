<?php

namespace Modules\YindulaCms\app\Enums;



/**
 * Generates Widget Text
 * @return string
 */
class MessageTypes
{
    /**
     * Application version.
     *
     * @var string
     */
    const SUCCESS = 'success';

    /**
     * Application version.
     *
     * @var string
     */
    const ERROR = 'error';
}
