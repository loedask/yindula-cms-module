<?php

namespace Modules\YindulaCms\app\Enums;



/**
 * Generates Widget Text
 * @return string
 */
class PageStatus
{

    /**
     * Statuses.
     */
    public const STATUS_ACTIVE = 'ACTIVE';
    public const STATUS_INACTIVE = 'INACTIVE';

    /**
     * List of statuses.
     *
     * @var array
     */
    public static $statuses = [self::STATUS_ACTIVE, self::STATUS_INACTIVE];
}
