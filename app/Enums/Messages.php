<?php

namespace Modules\YindulaCms\app\Enums;


/**
 * Generates Widget Text
 * @return string
 */
class Messages
{
    /**
     * Application version.
     *
     * @var string
     */
    const COPIED = 'messages.copied';


    /**
     * Application version.
     *
     * @var string
     */
    const DELETED = 'messages.deleted';


    /**
     * Application version.
     *
     * @var string
     */
    const NOT_FOUND = 'messages.not_found';


    /**
     * Application version.
     *
     * @var string
     */
    const SAVED = 'messages.saved';


    /**
     * Application version.
     *
     * @var string
     */
    const UPDATED = 'messages.updated';
}
