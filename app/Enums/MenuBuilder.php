<?php
namespace Modules\YindulaCms\app\Enums;

/**
 * Generates Widget Text
 * @return string
 */
class MenuBuilder
{
    /**
     * Application version.
     *
     * @var string
     */
    const CACHE_NAME = 'yindula_cms_menu';
}
