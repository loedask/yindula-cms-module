<?php

namespace Modules\YindulaCms\app\Enums;



/**
 * Generates Widget Text
 * @return string
 */
class SettingType
{
    /**
     * Application version.
     *
     * @var string
     */
    const APPLICATION = 'app';

    /**
     * Application version.
     *
     * @var string
     */
    const SITE = 'site';

    /**
     * List of types.
     *
     * @var array
     */
    public static $types = [self::APPLICATION, self::SITE];
}
