<?php

namespace Modules\YindulaCms\app\Providers;

use Illuminate\Support\Facades\View;

use Illuminate\Support\ServiceProvider;
use Modules\YindulaCms\app\Models\Page;
use Modules\YindulaCms\app\Enums\PageStatus;
use Modules\YindulaCms\app\Repositories\PageRepository;
use Modules\YindulaCms\app\Repositories\CategoryRepository;
use Modules\YindulaCms\app\Repositories\FeatureSectionRepository;

class CmsViewServiceProvider extends ServiceProvider
{

    protected string $moduleName = 'YindulaCms';

    protected string $moduleNameLower = 'yindulacms';

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadHelpers();
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['yindulacms::feature_sections._fields'], function ($view) {
            $view->with([
                'pageItems' => app(PageRepository::class)->dropDownItems(),
                'categoryItems' => app(CategoryRepository::class)->dropDownItems(),
            ]);
        });

        View::composer(['yindulacms::features._fields'], function ($view) {
            $view->with([
                'feature_sectionItems' => app(FeatureSectionRepository::class)->dropDownItemsForLabel(),
                'categoryItems' => app(CategoryRepository::class)->dropDownItems(),
                'pageItems' => Page::pluck('title', 'id')->toArray()
            ]);
        });
        View::composer(['yindulacms::pages._fields'], function ($view) {
            $view->with(['statuses' => PageStatus::$statuses]);
        });
        //
    }

    /**
     * Load helpers.
     */
    protected function loadHelpers()
    {
        foreach (glob(module_path($this->moduleName, 'app/Http/Helpers/*.php')) as $filename) {
            require_once $filename;
        }
    }
}
