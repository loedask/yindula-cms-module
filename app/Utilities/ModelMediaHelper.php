<?php

namespace Modules\YindulaCms\app\Utilities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Modules\YindulaCms\app\Repositories\BannerRepository;

class ModelMediaHelper
{

    public static function getImageAttribute($modelName, $primaryKey): string
    {
        $repositoryClass = self::getRepositoryClass($modelName);
        $page = self::findModel($repositoryClass, $primaryKey);

        if (!empty($page->image)) {
            return ImageUrlTransformer::transform('yindula-cms' . DIRECTORY_SEPARATOR . $page->image);
        }

        return ImageUrlTransformer::transform('build-bazintemplate' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'no-image.png');
    }

    public static function getImageTwoAttribute($modelName, $primaryKey): string
    {
        $repositoryClass = self::getRepositoryClass($modelName);
        $page = self::findModel($repositoryClass, $primaryKey);

        if (!empty($page->image_two)) {
            return ImageUrlTransformer::transform('yindula-cms' . DIRECTORY_SEPARATOR . $page->image_two);
        }

        return ImageUrlTransformer::transform('build-bazintemplate' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'no-image.png');
    }

    protected static function getRepositoryClass($modelName): string
    {
        $singularModelName = Str::singular($modelName);
        return "Modules\\YindulaCms\\app\\Repositories\\" . ucfirst($singularModelName) . "Repository";
    }

    protected static function findModel($repositoryClass, $primaryKey): Model
    {
        $repository = app($repositoryClass);

        try {
            return $repository->find($primaryKey);
        } catch (ModelNotFoundException $exception) {
            // Handle not found exception as needed
            // For now, let's return an empty model instance
            return new $repository->model;
        }
    }
}
