<?php

namespace Modules\YindulaCms\app\Utilities;

use Modules\YindulaCms\app\Models\FeatureSection;
use Modules\YindulaCms\app\Utilities\FileUtility;


class FeatureSectionContentUtility
{
    private $content;
    private $viewsPath;
    private $featureSection;

    public function __construct(FeatureSection $featureSection)
    {
        $this->content = $this->generateContent($featureSection);
        $this->viewsPath = config('yindulacms.path.frontend_features_path');

        $this->featureSection = $featureSection;
    }

    public function save()
    {
        // Ensure the features directory exists
        $this->ensureFeaturesDirectoryExists();

        $file_name = $this->content['fileName'];
        $file_path = $this->viewsPath . DIRECTORY_SEPARATOR . $file_name;
        file_put_contents($file_path, $this->content['content']);

        $this->includeFeatureSection($this->featureSection);
    }

    private function ensureFeaturesDirectoryExists()
    {
        $features_path = $this->viewsPath;

        // Check if the features directory exists, create it if not
        if (!is_dir($features_path)) {
            mkdir($features_path, 0755, true);
        }
    }

    public function delete()
    {

        $file_name = $this->featureSection->key . '.blade.php';

        $file_path = $this->viewsPath . DIRECTORY_SEPARATOR . $file_name;

        FileUtility::deleteFile($file_path);

        // Remove the include statement from site.blade.php
        $site_blade_utility = new SiteBladeUtility();
        $site_blade_utility->removeIncludeStatement($this->featureSection->key);
    }

    private function generateContent(FeatureSection $featureSection)
    {
        $key = $featureSection->key;

        $content = "@if (\$feature->findBySectionKey('$key')->count() > 0)
    <div class=\"callout container\">
        <div class=\"row\">
            <div class=\"col-md-6 col-12 mb-4 mb-lg-0\">
                <div class=\"text-job text-muted text-14\">
                    {{ \$feature_section->findTitleByKey('$key') }}
                </div>
                <div class=\"h1 mb-0 font-weight-bold\">
                    <span class=\"font-weight-500\">
                        {!! \$feature_section->findDescriptionByKey('$key') !!}
                    </span>
                </div>
            </div>

            @foreach (\$feature->findBySectionKey('$key') as \$item)
                <div class=\"col-4 col-md-2 text-center\">
                    <div class=\"h2 font-weight-bold\">
                        {{ \$item->title }}
                    </div>
                    <p>{!! \$item->truncated_description !!}</p>
                </div>
            @endforeach
        </div>
    </div>
@endif";

        $file_name = $key . '.blade.php';

        return [
            'content' => $content,
            'fileName' => $file_name,
        ];
    }

    private function includeFeatureSection($featureSection)
    {
        $file_path = base_path('Modules' . DIRECTORY_SEPARATOR . 'FrontPages' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'site.blade.php');

        // Check if the file exists
        if (!file_exists($file_path)) {
            throw new \Exception('site.blade.php file not found.');
        }

        // Read the file contents
        $file_contents = file_get_contents($file_path);

        // Check if the file already includes the feature section
        $feature_section_key = $featureSection->key;
        $includeStatement = "@include('frontpages::features.$feature_section_key')";

        $ifStatement = "@if (\$section->contains('$feature_section_key'))
    $includeStatement
@endif";

        if (strpos($file_contents, $includeStatement) !== false) {
            return;
        }

        // Add the feature section include statement at the end of the file
        $file_contents .= "\n\n$ifStatement";

        // Save the updated file contents
        file_put_contents($file_path, $file_contents);
    }
}
