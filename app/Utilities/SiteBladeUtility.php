<?php

namespace Modules\YindulaCms\app\Utilities;

class SiteBladeUtility
{
    private $siteFilePath;

    public function __construct()
    {
        $this->siteFilePath = base_path('Modules' . DIRECTORY_SEPARATOR . 'FrontPages' . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'site.blade.php');
    }

    public function removeIncludeStatement(string $featureSectionKey)
    {
        $includeStatement = "@include('frontpages::features.$featureSectionKey')";
        $ifStatement = "@if (\$section->contains('$featureSectionKey'))\n    $includeStatement\n@endif";

        // Check if the file exists
        if (!file_exists($this->siteFilePath)) {
            throw new \Exception('site.blade.php file not found.');
        }

        // Read the file contents
        $fileContents = file_get_contents($this->siteFilePath);

        // Remove the include statement
        $fileContents = str_replace("\n\n$includeStatement", '', $fileContents);

        // Remove the if statement
        $fileContents = str_replace("\n\n$ifStatement", '', $fileContents);

        // Save the updated file contents
        file_put_contents($this->siteFilePath, $fileContents);
    }
}
