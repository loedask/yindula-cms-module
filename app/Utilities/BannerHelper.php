<?php

namespace Modules\YindulaCms\app\Utilities;

use Modules\YindulaCms\app\Repositories\BannerRepository;

class BannerHelper
{

    public static function getImageAttribute($primaryKey): string
    {
        $page = app(BannerRepository::class)->find($primaryKey);

        if (!empty($page->image)) {
            $imageUrl = $page->image;

            $imageUrl = str_replace('\\', '/', asset('yindula-cms' . DIRECTORY_SEPARATOR . $imageUrl));
            return $imageUrl;
        }

        return str_replace('\\', '/', asset('build-bazintemplate' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'no-image.png'));
    }

    public static function getImageTwoAttribute($primaryKey): string
    {
        $page = app(BannerRepository::class)->find($primaryKey);

        if (!empty($page->image_two)) {
            $imageUrl = $page->image_two;

            $imageUrl = str_replace('\\', '/', asset('yindula-cms' . DIRECTORY_SEPARATOR . $imageUrl));
            return $imageUrl;
        }

        return str_replace('\\', '/', asset('build-bazintemplate' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'no-image.png'));
    }
}
