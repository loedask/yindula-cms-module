<?php

namespace Modules\YindulaCms\app\Utilities;


class ImageUrlTransformer
{
    public static function transform($path): string
    {
        return str_replace('\\', '/', asset($path));
    }
}
