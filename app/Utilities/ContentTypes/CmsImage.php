<?php

namespace Modules\YindulaCms\app\Utilities\ContentTypes;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image as InterventionImage;

class CmsImage extends BaseContentType
{
    public function handle()
    {
        if (!isset($this->file)) {

            return null;
        }

        $file = $this->file;

        $path =  $this->modelName . DIRECTORY_SEPARATOR . date('FY') . DIRECTORY_SEPARATOR;

        $filename = $this->generateFileName($file, $path);

        $filename_with_extension = $filename . '.' . $file->getClientOriginalExtension();

        $file->move($this->publicFolder() . $path, $filename_with_extension);

        return $path . $filename_with_extension;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param $path
     *
     * @return string
     */
    protected function generateFileName($file, $path)
    {
        $filename = Str::random(self::FILE_NAME_LENGTH);

        // Make sure the filename does not exist, if it does, just regenerate
        while (Storage::disk(config('config.storage.disk'))->exists($this->publicFolder() . $path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = Str::random(self::FILE_NAME_LENGTH);
        }

        return $filename;
    }

    private function publicFolder()
    {
        return 'yindula-cms' . DIRECTORY_SEPARATOR;
    }


    public function remove() // FileIfExists
    {
        if (Storage::disk(config('config.storage.disk'))->exists($this->publicFolder() . $this->file)) {
            Storage::disk(config('config.storage.disk'))->delete($this->publicFolder() . $this->file);
        }
    }
}
