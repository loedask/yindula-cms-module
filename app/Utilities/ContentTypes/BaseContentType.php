<?php

namespace Modules\YindulaCms\app\Utilities\ContentTypes;

abstract class BaseContentType
{

    const FILE_NAME_LENGTH = 20;

    /**
     * @var
     */
    protected $modelName;

    /**
     * @var
     */
    protected $file;

    /**
     * Password constructor.
     *
     * @param $file
     * @param $modelName
     */
    public function __construct($file, $modelName)
    {
        $this->file = $file;
        $this->modelName = $modelName;
    }

    /**
     * @return mixed
     */
    abstract public function handle();
}
