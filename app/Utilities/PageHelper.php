<?php

namespace Modules\YindulaCms\app\Utilities;

use Modules\YindulaCms\app\Repositories\PageRepository;

class PageHelper
{

    public static function getImageAttribute($primaryKey): string
    {
        $page = app(PageRepository::class)->find($primaryKey);

        if (!empty($page->image)) {
            $imageUrl = $page->image;

            $imageUrl = str_replace('\\', '/', asset('yindula-cms' . DIRECTORY_SEPARATOR . $imageUrl));
            return $imageUrl;
        }

        return str_replace('\\', '/', asset('build-bazintemplate' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'no-image.png'));
    }
}
