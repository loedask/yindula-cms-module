<?php

namespace Modules\YindulaCms\app\Utilities;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

/**
 * Generates Widget Text
 * @return string
 */
class Application
{
    /**
     * Application version.
     *
     * @var string
     */
    const VERSION = '1.4.2';

    /**
     * Application version.
     *
     * @var string
     */
    const SWAL_MODAL = 'swal:modal';

    /**
     * Application environment.
     *
     * @var string
     */
    const ENVIRONMENT_LIVE = 'production';

    /**
     * value
     *
     * @var mixed
     */
    public $value;

    public function __construct($value = null)
    {
        $this->value = $value;
    }


    /**
     * Get the version number of the application.
     *
     * @return string
     */
    public function version()
    {
        return static::VERSION;
    }


    /**
     * Get the name of the application.
     *
     * @return string
     */
    public function name()
    {
        return appName();
    }


    /**
     * Get the name of the application.
     *
     * @return string
     */
    public function copyright()
    {
        return companyName() . ' - ' .  date('Y') . ' .' . 'All Rights Reserved.';
    }


    public function replaceBackSlashesWithForwardSlashes()
    {
        return str_replace('\\', '/', $this->value);
    }
}
