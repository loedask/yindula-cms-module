<?php

namespace Modules\YindulaCms\app\Utilities;

use Illuminate\Support\Facades\File;

class FileUtility
{
    public static function createFile($path, $fileName, $content = '')
    {
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        $full_path = $path . DIRECTORY_SEPARATOR . $fileName;

        $file = fopen($full_path, "w");
        fwrite($file, $content);
        fclose($file);
    }

    public static function createDirectoryIfNotExist($path, $replace = false)
    {
        if (file_exists($path) && $replace) {
            rmdir($path);
        }

        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }
    }

    public static function deleteFile($path, $fileName = null)
    {
        if ($fileName) {
            $filePath = $path . DIRECTORY_SEPARATOR . $fileName;
        } else {
            $filePath = $path;
        }

        if (file_exists($filePath)) {
            return unlink($filePath);
        }

        return false;
    }
}
