<?php

namespace Modules\YindulaCms\app\Utilities;

use Illuminate\Support\Facades\App;

/**
 * Generates Widget Text
 * @return string
 */
class CmsUrlPrefix
{
    private bool $enablePrefix;

    public function __construct(bool $enablePrefix = false)
    {
        $this->enablePrefix = $enablePrefix;
    }

    public function handle(): string
    {
        if ($this->enablePrefix && App::environment('production')) {
            return "public" . DIRECTORY_SEPARATOR;
        }

        return "";
    }

    public function getModulesFolderName(): string
    {
        return "modules" . DIRECTORY_SEPARATOR;
    }
}
