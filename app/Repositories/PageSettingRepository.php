<?php

namespace Modules\YindulaCms\app\Repositories;

use Modules\YindulaCms\app\Models\PageSetting;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

class PageSettingRepository extends CmsBaseRepository
{
    protected $fieldSearchable = [
        'page_id',
        'has_subtitle',
        'has_meta_description',
        'has_meta_keywords',
        'has_excerpt',
        'has_body',
        'has_image',
        'has_image_two'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return PageSetting::class;
    }
}
