<?php
namespace Modules\YindulaCms\app\Repositories;

use Modules\YindulaCms\app\Models\Menu;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class MenuRepository
 * @package Modules\YindulaCms\app\Repositories
 * @version January 4, 2022, 9:32 am UTC
*/

class MenuRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Menu::class;
    }
}
