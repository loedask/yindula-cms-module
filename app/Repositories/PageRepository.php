<?php

namespace Modules\YindulaCms\app\Repositories;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\YindulaCms\app\Models\Page;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class PageRepository
 * @package Modules\Cms\Repositories
 * @version January 3, 2022, 12:01 pm UTC
 */

class PageRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'author_id',
        'title',
        'excerpt',
        'body',
        'image',
        'slug',
        'meta_description',
        'meta_keywords',
        'status',
        'is_home',
        'is_page'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Page::class;
    }

    public function findHomePageSlug()
    {
        try {
            return $this->model::whereIsHome(true)->first()->slug;
        } catch (Exception $e) {
            throw new ModelNotFoundException(__('Home page not found. Please select one on the backend'));
        }
    }

    /**
     * Configure the Model
     **/
    public function findHomePage()
    {
        $page = $this->model::whereIsHome(true)->first();

        // if (empty($page)) {
        //     throw new ModelNotFoundException(__('messages.not_found', ['model' => __('models/pages.singular')]));
        // }

        return $page;
    }

    /**
     * Configure the Model
     **/
    public function findContactPage()
    {
        $page = $this->model::whereIsContact(true)->first();

        if (empty($page)) {
            throw new ModelNotFoundException(__('messages.not_found', ['model' => __('models/pages.singular')]));
        }

        return $page;
    }

    /**
     * Configure the Model
     **/
    public function setHomePage($id)
    {
        $this->model::where('is_home', true)->update(['is_home' => false]);
        $this->model::where('id', $id)->update(['is_home' => true]);
        // return 'Contact page set as default successfully';
    }

    /**
     * Configure the Model
     **/
    public function setContactPage($id)
    {
        $this->model::where('is_contact', true)->update(['is_contact' => false]);
        $this->model::where('id', $id)->update(['is_contact' => true]);
    }

    /**
     * Configure the Model
     **/
    public function setEventPage($id)
    {
        $this->model::where('is_event', true)->update(['is_event' => false]);
        $this->model::where('id', $id)->update(['is_event' => true]);
    }


    /**
     * Configure the Model
     **/
    public function dropDownItems()
    {
        return $this->model::pluck('title', 'id')->toArray();
    }


    public function findBySlug($slug)
    {
        try {
            // return self::whereTranslation('slug', $slug, [App::getLocale()])->firstOrFail();
            return $this->model::where('slug', $slug)->first();
        } catch (Exception $e) {
            throw new ModelNotFoundException(__('messages.not_found', ['model' => __('models/pages.singular')]));
        }
    }

    public function isHome($slug =  null)
    {
        $model = $this->findBySlug($slug);

        if (isset($model->is_home) && $model->is_home) {
            return true;
        }

        return false;
    }

    public function isContact($slug =  null)
    {
        $model = $this->findBySlug($slug);

        if (isset($model->is_contact) && $model->is_contact) {
            return true;
        }

        return false;
    }

    public function isEvent($slug =  null)
    {
        $model = $this->findBySlug($slug);

        if (isset($model->is_event) && $model->is_event) {
            return true;
        }

        return false;
    }
}
