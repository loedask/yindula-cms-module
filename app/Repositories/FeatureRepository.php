<?php

namespace Modules\YindulaCms\app\Repositories;

use Modules\YindulaCms\app\Models\Feature;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class FeatureRepository
 * @package Modules\Cms\Repositories
 * @version January 3, 2022, 3:11 pm UTC
*/

class FeatureRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'feature_section_id',
        'title',
        'excerpt',
        'page_id',
        'description',
        'image',
        'sort_order',
        'is_active',
        'icon',
        'url'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Feature::class;
    }


    public function findBySectionKey($key)
    {
        return $this->model::join('feature_sections as fs', 'fs.id', "=", 'features.feature_section_id')
            ->where('fs.key', "=", $key)
            ->where('features.is_active', true)
            ->get('features.*');
    }
}
