<?php

namespace Modules\YindulaCms\app\Repositories;

use Modules\YindulaCms\app\Models\MenuItem;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class MenuItemRepository
 * @package Modules\YindulaCms\app\Repositories
 * @version January 4, 2022, 10:27 am UTC
 */

class MenuItemRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'menu_id',
        'title',
        'url',
        'target',
        'icon_class',
        'color',
        'parent_id',
        'order',
        'route',
        'parameters'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MenuItem::class;
    }

    /**
     * Configure the Model
     **/
    public function findMenuItemByUrl($url)
    {
        return $this->model::where('url', $url)->first();
    }

    /**
     * Configure the Model
     **/
    public function setMenuItemUrl($url)
    {
        $entity = $this->findMenuItemByUrl($url);

        if (!empty($entity)) {
            $found_entity = $this->find($entity->id);
            $found_entity->url = $url;
            $found_entity->save();
        }

        return null;
    }
}
