<?php

namespace Modules\YindulaCms\app\Repositories;

use Modules\YindulaCms\app\Models\Slider;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class SliderRepository
 * @package App\Repositories
 * @version September 11, 2022, 6:56 am UTC
*/

class SliderRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'subtitle',
        'excerpt',
        'description',
        'image',
        'image_two',
        'video',
        'url',
        'is_default'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Slider::class;
    }

    public function fetchAll(){
        return $this->model::all();
    }
}
