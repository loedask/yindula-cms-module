<?php

namespace Modules\YindulaCms\app\Repositories;

use Exception;
use Illuminate\Support\Facades\DB;
use Modules\YindulaCms\app\Models\Banner;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;
use Modules\YindulaCore\Utilities\FilepondManager;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class BannerRepository
 * @package App\Repositories
 * @version September 10, 2022, 5:51 pm UTC
 */

class BannerRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'excerpt',
        'description',
        'image',
        'video',
        'is_default',
        'image_two'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Banner::class;
    }

    public function fetchAll()
    {
        return $this->model::all();
    }


    /**
     * Create model record and store the file
     *
     * @param array $input
     *
     * @return Model
     */
    public function store($input, $image = null)
    {
        try {
            DB::beginTransaction();
            $model = $this->create($input);

            if (!is_null($image)) {
                $stored_file = new FilepondManager($image);

                $model->addMedia($stored_file->getStoredFile())->toMediaCollection($this->model::tableName());
            }

            DB::commit();

            return $model;
        } catch (Exception $e) {
            DB::rollBack();
            throw new UnprocessableEntityHttpException($e->getMessage());
        }
    }
}
