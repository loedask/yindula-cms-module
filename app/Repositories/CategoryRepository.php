<?php

namespace Modules\YindulaCms\app\Repositories;

use Modules\YindulaCms\Contract\AttributeInterface;
use Modules\YindulaCms\app\Models\Category;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class CategoryRepository
 * @package Modules\YindulaCms\app\Repositories
 * @version February 24, 2022, 8:23 pm UTC
 */

class CategoryRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'parent_id',
        'order',
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Category::class;
    }

    public function findById($id)
    {
        return $this->model->where('id', $id)->with('features')->first();
    }
}
