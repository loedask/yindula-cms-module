<?php

namespace Modules\YindulaCms\app\Repositories;

use Modules\YindulaCms\app\Models\CmsEvent;

class CmsEventRepository extends CmsBaseRepository
{
    protected $fieldSearchable = [
        'title',
        'description',
        'start_date',
        'end_date',
        'image',
        'start_time',
        'end_time',
        'is_full_day'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return CmsEvent::class;
    }

    public function fetchAll()
    {
        return $this->model::all();
    }
}
