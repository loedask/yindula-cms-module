<?php

namespace Modules\YindulaCms\app\Repositories;

use Modules\YindulaCms\app\Models\Gallery;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class GalleryRepository
 * @package App\Repositories
 * @version September 13, 2022, 6:06 pm UTC
*/

class GalleryRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'image',
        'image_two',
        'video'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Gallery::class;
    }

    public function fetchAll(){
        return $this->model::all();
    }
}
