<?php

namespace Modules\YindulaCms\app\Repositories;


use Illuminate\Support\Facades\Request;
use Modules\YindulaCms\app\Models\FeatureSection;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class FeatureSectionRepository
 * @package Modules\Cms\Repositories
 * @version January 3, 2022, 1:39 pm UTC
 */

class FeatureSectionRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'name',
        'title',
        'page_id',
        'description',
        'is_active'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FeatureSection::class;
    }

    public function findTitleByKey($key)
    {
        return $this->model::where('key', $key)->pluck('title')->first();
    }

    public function findImageByKey($key)
    {
        return $this->model::where('key', $key)->pluck('image')->first();
    }

    public function findDescriptionByKey($key)
    {
        return $this->model::where('key', $key)
            // ->withTranslations(App::getLocale())
            ->first()->description;
        // return $feature->getTranslatedAttribute('description');
        // return $feature;
    }


    public function ensureDirectoriesExist($viewsPath)
    {
        if (!file_exists($viewsPath . 'features')) {
            return mkdir($viewsPath . 'features', 0755, true);
        }
    }


    /**
     * Configure the Model
     **/
    public function dropDownItemsForLabel()
    {
        $current_page_id = Request::route('pageId');

        if ($current_page_id) {

            // Filter results based on the current page_id.
            $filtered_results = $this->model::where('page_id', $current_page_id)->get();

            // Return the filtered results.
            return $filtered_results->pluck('label', 'id')->toArray();
        }

        // Default behavior if $current_page_id is falsy or no modifications are made.
        return $this->model::get()->pluck('label', 'id')->toArray();
    }
}
