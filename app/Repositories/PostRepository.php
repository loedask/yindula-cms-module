<?php

namespace Modules\YindulaCms\app\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\YindulaCms\app\Models\Post;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class PostRepository
 * @package Modules\Cms\Repositories
 * @version January 8, 2022, 9:54 am UTC
 */

class PostRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'author_id',
        'category_id',
        'title',
        'seo_title',
        'excerpt',
        'body',
        'image',
        'slug',
        'meta_description',
        'meta_keywords',
        'status',
        'featured'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Post::class;
    }

    public function fetchFeaturedPosts()
    {
        return $this->model::where('status', '=', 'PUBLISHED')->where('featured', true)
            // ->withTranslations(App::getLocale())
            ->orderBy('created_at', 'DESC')->take(3)->get();
    }

    public function findBySlug($slug)
    {
        try {
            // return self::whereTranslation('slug', $slug, [App::getLocale()])->firstOrFail();
            return  $this->model::where('slug', $slug)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return json_encode(['error' => 'No query results for model']);
        }
    }

    public function findRandomPosts($model)
    {
        return $this->model::where('id', '<>', $model->id)
            // ->withTranslations(App::getLocale())
            ->take(4)->get();
    }
}
