<?php

namespace Modules\YindulaCms\app\Repositories;

use Exception;
use Illuminate\Support\Facades\Log;
use Modules\YindulaCms\app\Models\CmsSetting;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\YindulaCms\app\Repositories\CmsBaseRepository;

/**
 * Class CmsSettingRepository
 * @package App\Repositories
 * @version September 13, 2022, 10:26 am UTC
 */

class CmsSettingRepository extends CmsBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cms_setting_category_id',
        'key',
        'value',
        'category'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CmsSetting::class;
    }


    /**
     * @return mixed
     */
    public function findValueByTypeAndKey($type, $key)
    {
        try {
            return $this->model::where('type', $type)->where('key', $key)->first()->value;
        } catch (Exception $e) {
            Log::info($e);
            throw new ModelNotFoundException(__('One or more than one setting was not found.'));
        }
    }
}
