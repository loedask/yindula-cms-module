<?php
namespace Modules\YindulaCms\app\Traits;


use Modules\Yindula\Enums\MessageTypes;
use Modules\Yindula\Enums\Messages;
use Modules\Yindula\Utilities\Application;

trait ApplicationSweetAllert
{
    public function sweetAllert($model, $type = null)
    {
        if ($type == Messages::COPIED) {
            $this->browserEvent(MessageTypes::SUCCESS, Messages::COPIED, $model);
        } elseif ($type == Messages::UPDATED) {
            $this->browserEvent(MessageTypes::SUCCESS, Messages::UPDATED, $model);
        } elseif ($type == Messages::DELETED) {
            $this->browserEvent(MessageTypes::ERROR, Messages::DELETED, $model);
        } else {
            $this->browserEvent(MessageTypes::SUCCESS, Messages::SAVED, $model);
        }
    }

    public function browserEvent($type, $message, $model)
    {
        $this->dispatchBrowserEvent(Application::SWAL_MODAL, [
            'type' => $type,
            'title' => __($message, ['model' => __($model)]),
            'text' => '',
        ]);
    }
}
