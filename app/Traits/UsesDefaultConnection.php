<?php

namespace Modules\YindulaCms\app\Traits;


trait UsesDefaultConnection
{
    protected static function bootUsesDefaultConnection()
    {
        static::creating(function ($model) {
            $model->connection = 'default';
        });
    }
}

