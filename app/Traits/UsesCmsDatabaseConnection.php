<?php

namespace Modules\YindulaCms\app\Traits;

use Illuminate\Support\Facades\Log;

trait UsesCmsDatabaseConnection
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        // Set the connection dynamically from the configuration
        $this->connection = config('yindulacms.cms_database_connection');

        Log::info('Connection: ' . $this->connection);
    }
}

