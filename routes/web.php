<?php

use Illuminate\Support\Facades\Route;
use Modules\YindulaCms\app\Http\Controllers\CmsEventController;
use Modules\YindulaCms\app\Http\Controllers\FeatureController;
use Modules\YindulaCms\app\Http\Controllers\PageSettingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('cms')->name('cms.')->group(function () {

    Route::middleware('auth')->group(function () {

        Route::get('/dashboard', 'YindulaCmsController@dashboard')->name('dashboard');

        Route::resource('banners', BannerController::class);
        Route::resource('categories', CategoryController::class);
        Route::resource('cmsSettings', CmsSettingController::class);

        Route::resource('posts', PostController::class);
        Route::resource('sliders', SliderController::class);
        Route::resource('cmsEvents', CmsEventController::class);

        Route::resource('galleries', GalleryController::class);
        Route::resource('menus', MenuController::class);

        /*
        |--------------------------------------------------------------------------
        | Pages
        |--------------------------------------------------------------------------|
        */
        Route::resource('pages', PageController::class);

        Route::post(
            'pages/setHomePage/{id}',
            [Modules\YindulaCms\app\Http\Controllers\PageController::class, 'setHomePage']
        )->name('pages.setHomePage');

        Route::post(
            'pages/setContactPage/{id}',
            [Modules\YindulaCms\app\Http\Controllers\PageController::class, 'setContactPage']
        )->name('pages.setContactPage');

        /*
        |--------------------------------------------------------------------------
        | Feature Sections
        |--------------------------------------------------------------------------|
        */
        Route::resource('featureSections', FeatureSectionController::class);

        Route::get('featureSectionsAdd/{pageId}', [
            FeatureSectionController::class, 'add'
        ])->name('featureSections.add');

        /*
        |--------------------------------------------------------------------------
        | Features
        |--------------------------------------------------------------------------|
        */
        Route::resource('features', FeatureController::class);
        Route::get('featuresAdd/{pageId}', [FeatureController::class, 'add'])->name('features.add');

        /*
        |--------------------------------------------------------------------------
        | Page Settings
        |--------------------------------------------------------------------------|
        */
        Route::resource('pageSettings', PageSettingController::class);

        Route::get('pageSettingsAdd/{pageId}', [
            PageSettingController::class, 'add'
        ])->name('pageSettings.add');

        Route::get('pageSettingsCreateOrEdit/{pageId}', [
            PageSettingController::class, 'createOrEdit'
        ])->name('pageSettings.createOrEdit');

        /*
        |==========================================================================|
        */
    });
});
