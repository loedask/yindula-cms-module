<?php

namespace Modules\YindulaCms\Livewire;

use Laracasts\Flash\Flash;

use Livewire\Component;
use Modules\YindulaCms\app\Models\Slider;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\ImageColumn;

class SlidersTable extends DataTableComponent
{
    protected $model = Slider::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];

    public function deleteRecord($id)
    {
        Slider::find($id)->delete();
        Flash::success(__('messages.deleted', ['model' => __('models/sliders.singular')]));
        $this->dispatch('refreshDatatable');
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make(__('models/sliders.fields.title'), "title")
                ->sortable()
                ->searchable(),

            ImageColumn::make(__('models/sliders.fields.image'))
                ->location(function ($row) {
                    return asset($row->image_url);
                })
                ->attributes(function ($row) {
                    return [
                        'class' => 'img-fluid',
                        'style' => 'max-width: 100px; max-height: 100px;'
                    ];
                }),

            ImageColumn::make(__('models/sliders.fields.image_two'))
            ->location(function ($row) {
                return asset($row->image_two_url);
            })
            ->attributes(function ($row) {
                return [
                    'class' => 'img-fluid',
                    'style' => 'max-width: 100px; max-height: 100px;'
                ];
            }),
            Column::make(__('models/sliders.fields.video'), "video")
                ->sortable()
                ->searchable(),
            BooleanColumn::make(__('models/sliders.fields.is_default'), "is_default")
                ->sortable()
                ->searchable(),
            Column::make("Actions", 'id')
                ->format(
                    fn($value, $row, Column $column) => view('bazintemplate::common.livewire-tables.actions', [
                        'showUrl' => route('cms.sliders.show', $row->id),
                        'editUrl' => route('cms.sliders.edit', $row->id),
                        'recordId' => $row->id,
                    ])
                )
        ];
    }
}
