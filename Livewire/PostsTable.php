<?php

namespace Modules\YindulaCms\Livewire;

use Modules\YindulaCms\app\Repositories\PostRepository;
use Modules\YindulaCms\app\Models\Post;
use Modules\YindulaCore\app\Http\Livewire\LivewireModelDeleter;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\ImageColumn;

class PostsTable extends DataTableComponent
{
    protected $model = Post::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];


    public function deleteRecord($id)
    {
        $deleteRecord = new LivewireModelDeleter($this->model);
        $deleteRecord($id);
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make(__('models/posts.fields.category_id'), 'category_id')
                ->sortable()
                ->searchable(),
            Column::make(__('models/posts.fields.title'), "title")
                ->sortable()
                ->searchable(),

            ImageColumn::make(__('models/posts.fields.image'))
                ->location(function ($row) {
                    return asset($row->image_url);
                })
                ->attributes(function ($row) {
                    return [
                        'class' => 'img-fluid',
                        'style' => 'max-width: 100px; max-height: 100px;'
                    ];
                }),
            Column::make(__('models/posts.fields.slug'), "slug")
                ->sortable()
                ->searchable(),
            // Column::make(__('models/posts.fields.seo_title'), 'seo_title')
            //     ->sortable()
            //     ->searchable(),
            // Column::make(__('models/posts.fields.status'), 'status')
            //     ->sortable()
            //     ->searchable(),
            // BooleanColumn::make(__('models/posts.fields.featured'), 'featured')
            //     ->sortable()
            //     ->searchable(),
            Column::make(__('models/posts.fields.author_id'), 'author_id')
                ->sortable()
                ->searchable(),
            Column::make("Actions", 'id')
                ->format(
                    fn ($value, $row, Column $column) => view('bazintemplate::common.livewire-tables.actions', [
                        'showUrl' => route('cms.posts.show', $row->id),
                        'editUrl' => route('cms.posts.edit', $row->id),
                        'recordId' => $row->id
                    ])
                )
        ];
    }
}
