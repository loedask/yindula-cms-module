<?php

namespace Modules\YindulaCms\Livewire;

use Laracasts\Flash\Flash;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\PageSetting;

class PageSettingsTable extends DataTableComponent
{
    protected $model = PageSetting::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];

    public function deleteRecord($id)
    {
        PageSetting::find($id)->delete();
        Flash::success(__('messages.deleted', ['model' => __('models/pageSettings.singular')]));
        $this->emit('refreshDatatable');
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make("Page Id", "page_id")
                ->sortable()
                ->searchable(),
            Column::make("Has Subtitle", "has_subtitle")
                ->sortable()
                ->searchable(),
            Column::make("Has Meta Description", "has_meta_description")
                ->sortable()
                ->searchable(),
            Column::make("Has Meta Keywords", "has_meta_keywords")
                ->sortable()
                ->searchable(),
            Column::make("Has Excerpt", "has_excerpt")
                ->sortable()
                ->searchable(),
            Column::make("Has Body", "has_body")
                ->sortable()
                ->searchable(),
            Column::make("Has Image", "has_image")
                ->sortable()
                ->searchable(),
            Column::make("Has Image Two", "has_image_two")
                ->sortable()
                ->searchable(),
            Column::make("Actions", 'id')
                ->format(
                fn ($value, $row, Column $column) => view('bazintemplate::common.livewire-tables.actions', [
                        'showUrl' => route('page-settings.show', $row->id),
                        'editUrl' => route('page-settings.edit', $row->id),
                        'recordId' => $row->id,
                    ])
                )
        ];
    }
}
