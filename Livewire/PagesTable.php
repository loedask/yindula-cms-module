<?php

namespace Modules\YindulaCms\Livewire;

use Modules\YindulaCms\app\Models\Page;
use Modules\YindulaCms\app\Repositories\PageRepository;
use Modules\YindulaCore\app\Http\Livewire\LivewireModelDeleter;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\ImageColumn;

class PagesTable extends DataTableComponent
{
    protected $model = Page::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];


    public function deleteRecord($id)
    {
        $deleteRecord = new LivewireModelDeleter($this->model);
        $deleteRecord($id);
    }

    public function setHomePage($id)
    {
        app(PageRepository::class)->setHomePage($id);
    }

    public function setEventPage($id)
    {
        app(PageRepository::class)->setEventPage($id);
    }

    public function setContactPage($id)
    {
        app(PageRepository::class)->setContactPage($id);
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make(__('models/pages.fields.title'), "title")
                ->sortable()
                ->searchable(),
            Column::make(__('models/pages.fields.status'), "status")
                ->sortable()
                ->searchable(),

            ImageColumn::make(__('models/pages.fields.image'))
                ->location(function ($row) {
                    return asset($row->image_url);
                })
                ->attributes(function ($row) {
                    return [
                        'class' => 'img-fluid',
                        'style' => 'max-width: 100px; max-height: 100px;'
                    ];
                }),
            Column::make(__('models/pages.fields.slug'), "slug")
                ->sortable()
                ->searchable(),
            Column::make(__('models/pages.fields.author_id'), "author_id")
                ->sortable()
                ->searchable(),
            BooleanColumn::make(__('models/pages.fields.is_page'), "is_page")
                ->sortable()
                ->searchable(),
            BooleanColumn::make(__('models/pages.fields.is_home'), "is_home")
                ->sortable()
                ->searchable(),
            BooleanColumn::make(__('models/pages.fields.is_event'), "is_event")
                ->sortable()
                ->searchable(),
            BooleanColumn::make(__('models/pages.fields.is_contact'), "is_contact")
                ->sortable()
                ->searchable(),
            Column::make("Actions", 'id')
                ->format(
                    fn ($value, $row, Column $column) => view('yindulacms::pages.datatables_actions', [
                        'showUrl' => route('cms.pages.show', $row->id),
                        'editUrl' => route('cms.pages.edit', $row->id),
                        'recordId' => $row->id,
                        'homePageId' => $row->id,
                        'eventPageId' => $row->id,
                        'contactPageId' => $row->id,
                    ])
                )
        ];
    }
}
