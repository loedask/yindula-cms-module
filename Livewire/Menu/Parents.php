<?php

namespace Modules\YindulaCms\Livewire\Menu;

use Livewire\Component;
use Modules\YindulaCms\app\Models\Menu;
use Modules\YindulaCms\app\Models\MenuItem;
use Modules\YindulaCms\app\Enums\Messages;
use Modules\YindulaCms\app\Traits\LivewireSweetAllert;

class Parents extends Component
{
    use LivewireSweetAllert;

    /**
     * Application version.
     *
     * @var string
     */
    const MODEL_SINGULAR = 'models/menuItems.singular';

    public $showModal = false;
    public $menuId;

    public $menuItemId;
    public $model;

    protected $rules = [
        'model.menu_id' => 'nullable|integer',
        'model.title' => 'required|string|max:255',
        'model.url' => 'nullable|string|max:255',
        'model.target' => 'nullable|string|max:255',
        'model.icon_class' => 'nullable|string|max:255',
        'model.color' => 'nullable|string|max:255',
        'model.parent_id' => 'nullable|integer',
        'model.order' => 'nullable|integer',
        'model.created_at' => 'nullable',
        'model.updated_at' => 'nullable',
        'model.route' => 'nullable|string|max:255',
        'model.parameters' => 'nullable|string'
    ];

    public function mount($menuId)
    {
        $this->menuId = $menuId;
    }

    private function getMenuProperty()
    {
        return Menu::find($this->menuId);
    }

    public function render()
    {
        return view('yindulacms::livewire.menu.parents', [
            'parent_items' => $this->getMenuProperty()->parentItems
        ]);
    }

    public function updateOrder($list)
    {
        foreach ($list as $item) {
            MenuItem::find($item['value'])->update(['order' => $item['order']]);
        }
    }

    public function edit($menuItemId)
    {
        $this->showModal = true;
        $this->menuItemId = $menuItemId;
        $this->model = MenuItem::find($menuItemId);
    }

    public function create()
    {
        $this->showModal = true;
        $this->model = null;
        $this->menuItemId = null;
    }

    public function save()
    {
        $this->validate();

        if (!is_null($this->menuItemId)) {
            $this->model->save();
        } else {
            $this->model['menu_id'] = $this->menuId;
            MenuItem::create($this->model);
        }

        $this->showModal = false;
        $this->emit('refreshChildComponent');
        $this->sweetAllert(self::MODEL_SINGULAR);
    }

    public function close()
    {
        $this->showModal = false;
    }

    public function delete($menuItemId)
    {
        $menuItem = MenuItem::find($menuItemId);

        if ($menuItem) {
            $menuItem->delete();
            $this->emit('refreshChildComponent');
            $this->sweetAllert(self::MODEL_SINGULAR, Messages::DELETED);
        }
    }
}
