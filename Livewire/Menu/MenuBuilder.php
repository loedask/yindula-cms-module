<?php

namespace Modules\YindulaCms\Livewire\Menu;

use Illuminate\Database\Eloquent\Builder;
use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Repositories\MenuRepository;
use Modules\YindulaCms\app\Models\Menu;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;

class MenuBuilder extends DataTableComponent
{
    protected $model = Menu::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];

    public $pageId;


    public function deleteRecord($id)
    {
        app(MenuRepository::class)->delete($id);
        Flash::success(__('messages.deleted', ['model' => __('models/menus.singular')]));
        $this->emit('refreshDatatable');
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id')
            ->setReorderEnabled();
    }

    public function bulkActions(): array
    {
        return [
            'activate' => 'Activate',
            'deactivate' => 'Deactivate',
        ];
    }

    public function activate()
    {
        $this->model::whereIn('id', $this->getSelected())->update(['is_active' => true]);

        $this->clearSelected();
    }

    public function deactivate()
    {
        $this->model::whereIn('id', $this->getSelected())->update(['is_active' => false]);

        $this->clearSelected();
    }

    public function columns(): array
    {
        return [
            Column::make(__('models/menus.fields.name'), "name")
                ->sortable()
                ->searchable(),
            Column::make("Actions", 'id')
                ->format(
                    fn ($value, $row, Column $column) => view('bazintemplate::common.livewire-tables.actions', [
                        'showUrl' => route('cms.menus.show', $row->id),
                        'editUrl' => route('cms.menus.edit', $row->id),
                        'recordId' => $row->id,
                    ])
                )
        ];
    }
}
