<?php

namespace Modules\YindulaCms\Livewire;

use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Models\CmsEvent;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;

class CmsEventsTable extends DataTableComponent
{
    protected $model = CmsEvent::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];

    public function deleteRecord($id)
    {
        CmsEvent::find($id)->delete();
        Flash::success(__('messages.deleted', ['model' => __('models/cmsEvents.singular')]));
        $this->dispatch('refreshDatatable');
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make("Title", "title")
                ->sortable()
                ->searchable(),
            Column::make("Description", "description")
                ->sortable()
                ->searchable(),
            Column::make("Start Date", "start_date")
                ->sortable()
                ->searchable(),
            Column::make("End Date", "end_date")
                ->sortable()
                ->searchable(),
            Column::make("Image", "image")
                ->sortable()
                ->searchable(),
            Column::make("Start Time", "start_time")
                ->sortable()
                ->searchable(),
            Column::make("End Time", "end_time")
                ->sortable()
                ->searchable(),
            BooleanColumn::make("Is Full Day", "is_full_day")
                ->sortable()
                ->searchable(),
            Column::make("Actions", 'id')
                ->format(
                fn ($value, $row, Column $column) => view('bazintemplate::common.livewire-tables.actions', [
                        'showUrl' => route('cms.cmsEvents.show', $row->id),
                        'editUrl' => route('cms.cmsEvents.edit', $row->id),
                        'recordId' => $row->id,
                    ])
                )
        ];
    }
}
