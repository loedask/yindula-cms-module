<?php

namespace Modules\YindulaCms\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Models\Feature;
use Modules\YindulaCms\app\Models\FeatureSection;
use Modules\YindulaCms\app\Models\Page;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;

class FeaturesTable extends DataTableComponent
{
    protected $model = Feature::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];

    public $pageId;
    public $sectionId;


    public function mount(Page $page = null, FeatureSection $featureSection = null): void
    {
        // if $page is not null,
        // it will set $this->pageId to the value of $page->id
        // otherwise, it will set it to null.
        $this->pageId = $page ? $page->id : null;


        // if $featureSection is not null,
        // it will set $this->featureSectionId to the value of $featureSection->id
        // otherwise, it will set it to null.
        $this->sectionId = $featureSection ? $featureSection->id : null;
    }

    public function builder(): Builder
    {
        $builder = Feature::query();

        // If a specific page is set, filter features based on the page
        if (!is_null($this->pageId)) {
            $builder->whereHas('featureSection', function ($query) {
                $query->where('page_id', $this->pageId);
            });
        }
        // If a specific section is set, filter features based on the section
        elseif (!is_null($this->sectionId)) {
            $builder->where('feature_section_id', $this->sectionId);
        }

        return $builder;
    }



    public function deleteRecord($id)
    {
        Feature::find($id)->delete();
        Flash::success(__('messages.deleted', ['model' => __('models/features.singular')]));
        $this->dispatch('refreshDatatable');
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');

        // ->setCurrentlyReorderingEnabled()
        // ->setDefaultSort('sort_order', 'desc')
        // ->setDefaultReorderSort('sort_order', 'desc')
        // ->setHideReorderColumnUnlessReorderingEnabled()
        // ->setSortingEnabled()
    }

    // public function bulkActions(): array
    // {
    //     return [
    //         'activate' => 'Activate',
    //         'deactivate' => 'Deactivate',
    //     ];
    // }

    // public function activate()
    // {
    //     $this->model::whereIn('id', $this->getSelected())->update(['is_active' => true]);

    //     $this->clearSelected();
    // }

    // public function deactivate()
    // {
    //     $this->model::whereIn('id', $this->getSelected())->update(['is_active' => false]);

    //     $this->clearSelected();
    // }

    // private function updateStatus(bool $status): void
    // {
    //     $this->model::whereIn('id', $this->getSelected())->update(['is_active' => $status]);
    //     $this->clearSelected();
    // }

    // public function reorder(array $items): void
    // {
    //     foreach ($items as $item) {
    //         Feature::find($item[$this->getPrimaryKey()])
    //             ->update(['sort_order' => (int)$item[$this->getDefaultReorderColumn()]]);
    //     }
    // }

    public function columns(): array
    {
        return [
            // Column::make("Category Id", "category_id")
            //     ->sortable()
            //     ->searchable(),
            Column::make("Title", "title")
                ->sortable()
                ->searchable(),
            // Column::make("Sort Order", "sort_order")
            //     ->sortable()
            //     ->searchable(),
            BooleanColumn::make("Is Active", "is_active")
                ->sortable()
                ->searchable(),
            // Column::make("Image", "image")
            //     ->sortable()
            //     ->searchable(),
            // Column::make("Icon", "icon")
            //     ->sortable()
            //     ->searchable(),
            // Column::make("Url", "url")
            //     ->sortable()
            //     ->searchable(),
            Column::make("Page Id", "featureSection.page.title")
                ->sortable()
                ->searchable(),
            Column::make("Feature Section Id", "featureSection.name")
                ->sortable()
                ->searchable(),
            Column::make("Sort Order", "sort_order")
                ->sortable(),
            // ->collapseOnMobile()
            // ->excludeFromColumnSelect(),
            Column::make("Actions", 'id')
                ->format(
                    fn ($value, $row, Column $column) => view('bazintemplate::common.livewire-tables.actions', [
                        'showUrl' => route('cms.features.show', $row->id),
                        'editUrl' => route('cms.features.edit', $row->id),
                        'recordId' => $row->id,
                    ])
                )
        ];
    }
}
