<?php

namespace Modules\YindulaCms\Livewire\FeatureSections;

use Illuminate\Contracts\View\View;
use Livewire\Attributes\Locked;
use Livewire\Attributes\On;
use Livewire\Attributes\Rule;
use Livewire\Component;
use Modules\YindulaCms\app\Models\FeatureSection;

class CreateOrUpdateForm extends Component
{

    public $pageId;

    #[Locked]
    public int $featureSectionId;

    public ?FeatureSection $model;


    // #[Rule('required')]
    // public string $name = '';
    // #[Rule('required|numeric')]
    // public string $price = '';

    public function mount($pageId = null): void
    {
        $this->pageId = $pageId;
    }

    public function edit(int $featureSectionId): void
    {
        $this->model = FeatureSection::where('id', $featureSectionId)->first();
        $this->featureSectionId = $featureSectionId;

        // $this->name = $this->model->name;
        // $this->price = $this->model->price;
    }

    public function save(): void
    {
        $this->validate();

        if (empty($this->model)) {
            FeatureSection::create([
                'name' => $this->name,
                'price' => $this->price,
            ]);
        } else {
            $this->model->update([
                'name' => $this->name,
                'price' => $this->price,
            ]);
        }

        $this->reset('model', 'name', 'price', 'showModal');
    }

    public function render(): View
    {
        return view('yindulacms::livewire.feature_sections.feature-section-create');
    }
}
