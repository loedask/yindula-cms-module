<?php

namespace Modules\YindulaCms\Livewire\FeatureSections;

use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Models\Page;
use Illuminate\Database\Eloquent\Builder;
use Modules\YindulaCms\app\Models\FeatureSection;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;
use Modules\YindulaCms\app\Repositories\FeatureSectionRepository;

class FeatureSectionsTable extends DataTableComponent
{
    protected $model = FeatureSection::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];

    public $pageId;


    public function mount(Page $page = null)
    {
        // if $page is not null,
        // it will set $this->pageId to the value of $page->id
        // otherwise, it will set it to null.
        $this->pageId = $page ? $page->id : null;
    }

    public function builder(): Builder
    {
        $builder = FeatureSection::query();

        if (!is_null($this->pageId)) {
            $builder->where('page_id', $this->pageId);
        }

        return $builder;
    }

    public function deleteRecord($id)
    {
        app(FeatureSectionRepository::class)->delete($id);
        Flash::success(__('messages.deleted', ['model' => __('models/featureSections.singular')]));

        $this->dispatch('refreshDatatable');
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
            // ->setReorderEnabled();
    }

    public function bulkActions(): array
    {
        return [
            'activate' => 'Activate',
            'deactivate' => 'Deactivate',
        ];
    }

    // public function activate()
    // {
    //     $this->model::whereIn('id', $this->getSelected())->update(['is_active' => true]);

    //     $this->clearSelected();
    // }

    // public function deactivate()
    // {
    //     $this->model::whereIn('id', $this->getSelected())->update(['is_active' => false]);

    //     $this->clearSelected();
    // }

    public function columns(): array
    {
        return [
            // Column::make("Category Id", "category_id")
            //     ->sortable()
            //     ->searchable(),
            Column::make(__('models/featureSections.fields.name'), "name")
                ->sortable()
                ->searchable(),
            Column::make(__('models/featureSections.fields.key'), "key")
                ->sortable()
                ->searchable(),
            Column::make(__('models/featureSections.fields.title'), "title")
                ->sortable()
                ->searchable(),
            // Column::make("Image", "image")
            //     ->sortable()
            //     ->searchable(),
            // Column::make("Description", "description")
            //     ->sortable()
            //     ->searchable(),
            BooleanColumn::make(__('models/featureSections.fields.is_active'), "is_active")
                ->sortable()
                ->searchable(),
            Column::make(__('models/featureSections.fields.page_id'), "page.title")
                ->sortable()
                ->searchable(),
            Column::make("Actions", 'id')
                ->format(
                    fn ($value, $row, Column $column) => view('bazintemplate::common.livewire-tables.actions', [
                        'showUrl' => route('cms.featureSections.show', $row->id),
                        'editUrl' => route('cms.featureSections.edit', $row->id),
                        'recordId' => $row->id,
                    ])
                )
        ];
    }
}
