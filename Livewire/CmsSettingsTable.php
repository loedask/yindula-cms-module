<?php

namespace Modules\YindulaCms\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Laracasts\Flash\Flash;
use Modules\YindulaCms\app\Models\CmsSetting;
use Modules\YindulaCms\app\Repositories\CmsSettingRepository;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;

class CmsSettingsTable extends DataTableComponent
{
    protected $model = CmsSetting::class;

    protected $listeners = ['deleteRecord' => 'deleteRecord'];

    public $pageId;


    public function deleteRecord($id)
    {
        app(CmsSettingRepository::class)->delete($id);
        Flash::success(__('messages.deleted', ['model' => __('models/cmsSettings.singular')]));
        $this->dispatch('refreshDatatable');
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id')
            ->setReorderEnabled();
    }

    public function bulkActions(): array
    {
        return [
            'activate' => 'Activate',
            'deactivate' => 'Deactivate',
        ];
    }

    public function activate()
    {
        $this->model::whereIn('id', $this->getSelected())->update(['is_active' => true]);

        $this->clearSelected();
    }

    public function deactivate()
    {
        $this->model::whereIn('id', $this->getSelected())->update(['is_active' => false]);

        $this->clearSelected();
    }

    public function columns(): array
    {
        return [
            Column::make('Type', 'type')
                ->sortable()
                ->searchable(),
            Column::make('Key', 'key')
                ->sortable()
                ->searchable(),
            Column::make('Value', 'value')
                ->sortable()
                ->searchable(),
            Column::make('Sort Order', 'sort_order')
                ->sortable()
                ->searchable(),
            Column::make("Actions", 'id')
                ->format(
                    fn ($value, $row, Column $column) => view('bazintemplate::common.livewire-tables.actions', [
                        'showUrl' => route('cms.cmsSettings.show', $row->id),
                        'editUrl' => route('cms.cmsSettings.edit', $row->id),
                        'recordId' => $row->id,
                    ])
                ),
        ];
    }
}
