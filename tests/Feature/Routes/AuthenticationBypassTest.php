<?php
namespace Modules\YindulaCms\Tests\Feature\Routes;

use Modules\YindulaCore\Tests\BaseTestCase;

class AuthenticationBypassTest extends BaseTestCase
{

    /**
     * Test authentication bypass for the dashboard route.
     *
     * @return void
     */
    public function testDashboardRouteAuthenticationBypass()
    {
        $response = $this->get('/cms/dashboard');
        $response->assertRedirect('/login');
    }

    /**
     * Test authentication bypass for the banners resource routes.
     *
     * @return void
     */
    public function testBannersResourceAuthenticationBypass()
    {
        $routes = [
            '/cms/banners',
            '/cms/banners/{id}',
            '/cms/banners',
            '/cms/banners/{id}',
            '/cms/banners/{id}',
        ];

        foreach ($routes as $route) {
            $response = $this->get($route);
            $response->assertRedirect('/login');
        }
    }

    /**
     * Test authentication bypass for the categories resource routes.
     *
     * @return void
     */
    public function testCategoriesResourceAuthenticationBypass()
    {
        $routes = [
            '/cms/categories',
            '/cms/categories/{id}',
            '/cms/categories',
            '/cms/categories/{id}',
            '/cms/categories/{id}',
        ];

        foreach ($routes as $route) {
            $response = $this->get($route);
            $response->assertRedirect('/login');
        }
    }
}
