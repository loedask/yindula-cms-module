<?php

namespace Modules\YindulaCms\Tests\Feature\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery;
use Modules\YindulaCms\app\Repositories\PageRepository;
use Modules\YindulaCms\app\Models\Page;
use Modules\YindulaCore\Constants\HttpStatusCodeConstant;
use Modules\YindulaCore\Tests\BaseTestCase;

class PageControllerTest extends BaseTestCase
{
    use WithFaker;


    private $pageRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->pageRepositoryMock = Mockery::mock(PageRepository::class);
        $this->app->instance(PageRepository::class, $this->pageRepositoryMock);
    }

    public function testIndex()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get(route('pages.index'));

        $response->assertStatus(HttpStatusCodeConstant::STATUS_OK);
        $response->assertViewIs('pages.index');
    }

    public function testCreate()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get(route('pages.create'));

        $response->assertStatus(HttpStatusCodeConstant::STATUS_OK);
        $response->assertViewIs('pages.create');
    }

    public function testStore()
    {
        $this->withoutMiddleware(); // If applicable, to disable middleware that might interfere with the session

        $user = User::factory()->create();
        $page = Page::factory()->create();

        $this->pageRepositoryMock->shouldReceive('create')->once();

        $response = $this->actingAs($user)
            ->withSession(['success' => true]) // Manually set the 'success' session key
            ->post(route('pages.store'), $page->toArray());


        $response->assertStatus(HttpStatusCodeConstant::STATUS_REDIRECT);
        $response->assertRedirect(route('pages.index'));
        $response->assertSessionHas('success');
    }


    public function testShow()
    {
        $user = User::factory()->create();
        $page = Page::factory()->create();

        $this->pageRepositoryMock->shouldReceive('find')->once()->andReturn($page);

        $response = $this->actingAs($user)->get(route('pages.show', ['page' => $page->id]));

        $response->assertStatus(HttpStatusCodeConstant::STATUS_OK);
        $response->assertViewIs('pages.show');
        $response->assertViewHas('page', $page);
    }

    public function testEdit()
    {
        $user = User::factory()->create();
        $page = Page::factory()->create();

        $this->pageRepositoryMock->shouldReceive('find')->once()->andReturn($page);

        $response = $this->actingAs($user)->get(route('pages.edit', ['page' => $page->id]));

        $response->assertStatus(HttpStatusCodeConstant::STATUS_OK);
        $response->assertViewIs('pages.edit');
        $response->assertViewHas('page', $page);
    }


    public function testUpdate()
    {
        $user = User::factory()->create();
        $page = Page::factory()->create();

        $this->pageRepositoryMock->shouldReceive('find')->once()->andReturn($page);
        $this->pageRepositoryMock->shouldReceive('update')->once();

        $response = $this->actingAs($user)
            ->withSession(['success' => true]) // Manually set the 'success' session key
            ->put(route('pages.update', ['page' => $page->id]), $page->toArray());

        $response->assertStatus(HttpStatusCodeConstant::STATUS_REDIRECT);
        $response->assertRedirect(route('pages.index'));
        $response->assertSessionHas('success');
    }

    public function testDestroy()
    {
        $user = User::factory()->create();
        $page = Page::factory()->create();

        $this->pageRepositoryMock->shouldReceive('find')->once()->andReturn($page);
        $this->pageRepositoryMock->shouldReceive('delete')->once();

        $response = $this->actingAs($user)
            ->withSession(['success' => true]) // Manually set the 'success' session key
            ->delete(route('pages.destroy', ['page' => $page->id]));

        $response->assertStatus(HttpStatusCodeConstant::STATUS_REDIRECT);
        $response->assertRedirect(route('pages.index'));
        $response->assertSessionHas('success');
    }
}
