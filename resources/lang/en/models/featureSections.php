<?php

return array(
    'singular' => 'Feature Section',
    'plural' => 'Feature Sections',
    'fields' =>
    array(
        'category_id' => 'Category',
        'key' => 'Key',
        'name' => 'Name',
        'title' => 'Title',
        'page_id' => 'Page',
        'description' => 'Description',
        'is_active' => 'Is Active',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ),
);
