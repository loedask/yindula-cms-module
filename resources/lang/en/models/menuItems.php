<?php

return array (
  'singular' => 'Menu Item',
  'plural' => 'Menu Items',
  'fields' =>
  array (
    'menu_id' => 'Menu',
    'title' => 'Title',
    'url' => 'Url',
    'target' => 'Target',
    'icon_class' => 'Icon Class',
    'color' => 'Color',
    'parent_id' => 'Parent',
    'order' => 'Order',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'route' => 'Route',
    'parameters' => 'Parameters',
  ),
);
