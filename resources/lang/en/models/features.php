<?php

return [
    'singular' => 'Feature',
    'plural' => 'Features',
    'fields' => [
        'category_id' => 'Category',
        'feature_section_id' => 'Feature Section',
        'title' => 'Title',
        'content' => 'Content',
        'excerpt' => 'Excerpt',
        'redirect_to_page_id' => 'Redirect to Page',
        'description' => 'Description',
        'image' => 'Image',
        'sort_order' => 'Sort Order',
        'is_active' => 'Is Active',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
        'icon' => 'Icon',
        'url' => 'Url',
    ],
];
