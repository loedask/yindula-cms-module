<?php

return [
    'singular' => 'FeatureSection',
    'plural' => 'FeatureSections',
    'fields' => [
        'category_id' => 'Category',
        'key' => 'Key',
        'name' => 'Name',
        'title' => 'Title',
        'image' => 'Image',
        'page_id' => 'Page',
        'description' => 'Description',
        'is_active' => 'Is Active',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ],
];
