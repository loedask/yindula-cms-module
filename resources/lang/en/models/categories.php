<?php

return array (
  'singular' => 'Category',
  'plural' => 'Categories',
  'fields' =>
  array (
    'parent_id' => 'Parent',
    'order' => 'Order',
    'name' => 'Name',
    'slug' => 'Slug',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
