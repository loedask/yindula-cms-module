<?php

return [
    'singular' => 'Page',
    'plural' => 'Pages',
    'fields' => [
        'author_id' => 'Author',
        'title' => 'Title',
        'subtitle' => 'Subtitle',
        'excerpt' => 'Excerpt',
        'body' => 'Body',
        'image' => 'Image',
        'slug' => 'Slug',
        'meta_description' => 'Meta Description',
        'meta_keywords' => 'Meta Keywords',
        'status' => 'Status',
        'is_page' => 'Is Page',
        'is_home' => 'Is Home',
        'is_contact' => 'Is Contact',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ],
];
