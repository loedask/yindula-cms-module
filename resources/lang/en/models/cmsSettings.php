<?php

return array (
  'singular' => 'Cms Setting',
  'plural' => 'Cms Settings',
  'fields' =>
  array (
    'key' => 'Key',
    'name' => 'Name',
    'title' => 'Title',
    'page_id' => 'Page',
    'description' => 'Description',
    'is_active' => 'Is Active',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
