<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $banner->title }}</p>
</div>

<!-- Excerpt Field -->
<div class="form-group">
    {!! Form::label('excerpt', 'Excerpt:') !!}
    <p>{{ $banner->excerpt }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $banner->description }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{{ $banner->image }}</p>
</div>

<!-- Video Field -->
<div class="form-group">
    {!! Form::label('video', 'Video:') !!}
    <p>{{ $banner->video }}</p>
</div>

<!-- Is Default Field -->
<div class="form-group">
    {!! Form::label('is_default', 'Is Default:') !!}
    <p>{{ $banner->is_default }}</p>
</div>

<!-- Image Two Field -->
<div class="form-group">
    {!! Form::label('image_two', 'Image Two:') !!}
    <p>{{ $banner->image_two }}</p>
</div>

