@extends('bazintemplate::layouts.authed')

@section('title')
    Create Banner
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading m-0">New Banner</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                <a href="{{ route('cms.banners.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
        <div class="content">

            @include('bazintemplate::common.errors')

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body ">
                                {!! Form::open(['route' => 'cms.banners.store', 'files' => true]) !!}
                                <div class="row">
                                    @include('yindulacms::banners.fields')
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
@endsection
