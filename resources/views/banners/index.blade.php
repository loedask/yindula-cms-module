@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('models/banners.plural')
@endsection

@section('content')
    <section class="section">
        <div class="section-header">

            <h1>
                @lang('models/banners.plural')
            </h1>

            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.banners.create') }}" class="btn btn-primary form-btn">
                    @lang('models/banners.singular')
                    <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <livewire:yindulacms::banners-table />
                </div>
            </div>
        </div>

    </section>
@endsection
