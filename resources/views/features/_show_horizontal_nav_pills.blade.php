 <div class="row">
     <div class="col-12 col-sm-12 col-lg-12">
         <div class="card">
             <div class="card-header">

                 <div class="section-header-breadcrumb">
                     <a href="{{ route('cms.features.add', ['pageId' => $page->id]) }}" class="btn btn-primary form-btn">
                         @lang('crud.add_new') Feature
                         <i class="fas fa-plus"></i>
                     </a>
                 </div>

                 {{--   <div class="section-header-breadcrumb">
                    <a href="{{ route('cms.features.create') }}" class="btn btn-primary form-btn">
                        @lang('crud.add_new') Feature
                        <i class="fas fa-plus"></i>
                    </a>
                </div> --}}

             </div>
             <div class="card-body">
                 <ul class="nav nav-pills" id="myTab3" role="tabfeaturesList">
                     <li class="nav-item">
                         <a class="nav-link active show" id="featuresList-tab3" data-toggle="tab" href="#featuresList3"
                             role="tab" aria-controls="featuresList" aria-selected="true">
                             Feature List
                         </a>
                     </li>
                     {{-- <li class="nav-item">
                         <a class="nav-link" id="featuresCreate-tab3" data-toggle="tab" href="#featuresCreate3"
                             role="tab" aria-controls="featuresCreate" aria-selected="false">
                             Create Form
                         </a>
                     </li> --}}
                 </ul>
                 <div class="tab-content" id="myTabContent2">
                     <div class="tab-pane fade active show" id="featuresList3" role="tabpanel"
                         aria-labelledby="featuresList-tab3">
                         <div class="row">
                             <livewire:yindulacms::features-table :page="$page" :key="time() . $page->id" />
                         </div>
                     </div>
                     <div class="tab-pane fade" id="featuresCreate3" role="tabpanel"
                         aria-labelledby="featuresCreate-tab3">
                         {{-- <livewire:yindulacms::feature-create :page="$page" :key="time() . $page->id" /> --}}
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
