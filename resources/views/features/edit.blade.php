@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('crud.edit') @lang('models/features.singular')
@endsection

@section('content')
    <section class="section">

        <div class="section-header">
            <h3 class="page__heading m-0">@lang('crud.edit') @lang('models/features.singular')</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                <a href="{{ route('cms.features.index') }}" class="btn btn-primary">@lang('crud.back')</a>
            </div>
        </div>

        <div class="content">

            @include('bazintemplate::common.errors')

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body ">
                                {!! Form::model($feature, [
                                    'route' => ['cms.features.update', $feature->id],
                                    'method' => 'patch',
                                    'files' => true,
                                ]) !!}
                                <div class="row">
                                    @include('yindulacms::features._fields')
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
