<!-- Feature Section Id Field -->
<div class="form-group">
    {!! Form::label('feature_section_id', __('models/features.fields.feature_section_id').':') !!}
    <p>{{ $feature->feature_section_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', __('models/features.fields.title').':') !!}
    <p>{{ $feature->title }}</p>
</div>

<!-- Excerpt Field -->
<div class="form-group">
    {!! Form::label('excerpt', __('models/features.fields.excerpt').':') !!}
    <p>{{ $feature->excerpt }}</p>
</div>

<!-- Page Id Field -->
<div class="form-group">
    {!! Form::label('page_id', __('models/features.fields.page_id').':') !!}
    <p>{{ $feature->page_id }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/features.fields.description').':') !!}
    <p>{{ $feature->description }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/features.fields.image').':') !!}
    <p>{{ $feature->image }}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', __('models/features.fields.sort_order').':') !!}
    <p>{{ $feature->sort_order }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', __('models/features.fields.is_active').':') !!}
    <p>{{ $feature->is_active }}</p>
</div>

<!-- Icon Field -->
<div class="form-group">
    {!! Form::label('icon', __('models/features.fields.icon').':') !!}
    <p>{{ $feature->icon }}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', __('models/features.fields.url').':') !!}
    <p>{{ $feature->url }}</p>
</div>

