
<div class="col-sm-6">

    <!-- Feature Section Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('feature_section_id', __('yindulacms::models/features.fields.feature_section_id') . ':') !!}
        {!! Form::select('feature_section_id', $feature_sectionItems, null, ['class' => ['form-control', 'select2']]) !!}
    </div>

    <!-- Is Active Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('is_active', __('yindulacms::models/features.fields.is_active') . ':') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('is_active', 0) !!}
            {!! Form::checkbox('is_active', '1', null) !!} 1
        </label>
    </div>

    <!-- Icon Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('icon', __('yindulacms::models/features.fields.icon') . ':') !!}
        {!! Form::text('icon', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Excerpt Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('excerpt', __('yindulacms::models/features.fields.excerpt') . ':') !!}
        {!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Category Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('category_id', __('yindulacms::models/features.fields.category_id').':') !!}
        {!! Form::select('category_id', $categoryItems, null, ['class' => ['form-control', 'select2']]) !!}
    </div>

</div>

<div class="col-sm-6">

    <!-- Title Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('title', __('yindulacms::models/features.fields.title') . ':') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Excerpt Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('content', __('yindulacms::models/features.fields.content') . ':') !!}
        {!! Form::text('content', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Url Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('url', __('yindulacms::models/features.fields.url') . ':') !!}
        {!! Form::text('url', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Sort Order Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('sort_order', __('yindulacms::models/features.fields.sort_order') . ':') !!}
        {!! Form::text('sort_order', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Page Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('redirect_to_page_id', __('yindulacms::models/features.fields.redirect_to_page_id') . ':') !!}
        {!! Form::select('redirect_to_page_id', $pageItems, null, ['class' => ['form-control', 'select2']]) !!}
    </div>

    <!-- Image Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('image', __('yindulacms::models/features.fields.image') . ':') !!}
        {!! Form::file('image') !!}
    </div>

</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', __('yindulacms::models/features.fields.description') . ':') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cms.features.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>


<span id="_filepond" class="hiden">
    @include('yindulacms::features._filepond')
</span>



@push('scripts')
    <script>
        $("#type").on('feature_section_id', function() {
            if ($(this).val() == '17') {
                $("#_filepond").hide();
            } else {
                $("#_filepond").show();
            }
        }).change();
    </script>
    <script>
        $("#type").on('feature_section_id', function() {
            if ($(this).val() == '17') {
                $("#_filepond").hide();
            } else {
                $("#_filepond").show();
            }
        }).load();
    </script>
@endpush
