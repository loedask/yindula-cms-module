@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('models/features.singular') @lang('crud.details')
@endsection

@section('content')
    <section class="section">

        <div class="section-header">
            <h1>@lang('models/features.singular') @lang('crud.details')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.features.index') }}" class="btn btn-primary form-btn float-right">
                    @lang('crud.back')
                </a>
            </div>
        </div>

        @include('bazintemplate::common.errors')

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('yindulacms::features._show_fields')
                </div>
            </div>
        </div>
    </section>
@endsection
