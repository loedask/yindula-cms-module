<!-- Author Id Field -->
<div class="form-group">
    {!! Form::label('author_id', __('models/posts.fields.author_id').':') !!}
    <p>{{ $post->author_id }}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', __('models/posts.fields.category_id').':') !!}
    <p>{{ $post->category_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', __('models/posts.fields.title').':') !!}
    <p>{{ $post->title }}</p>
</div>

<!-- Seo Title Field -->
<div class="form-group">
    {!! Form::label('seo_title', __('models/posts.fields.seo_title').':') !!}
    <p>{{ $post->seo_title }}</p>
</div>

<!-- Excerpt Field -->
<div class="form-group">
    {!! Form::label('excerpt', __('models/posts.fields.excerpt').':') !!}
    <p>{{ $post->excerpt }}</p>
</div>

<!-- Body Field -->
<div class="form-group">
    {!! Form::label('body', __('models/posts.fields.body').':') !!}
    <p>{{ $post->body }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/posts.fields.image').':') !!}
    <p>{{ $post->image }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', __('models/posts.fields.slug').':') !!}
    <p>{{ $post->slug }}</p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    {!! Form::label('meta_description', __('models/posts.fields.meta_description').':') !!}
    <p>{{ $post->meta_description }}</p>
</div>

<!-- Meta Keywords Field -->
<div class="form-group">
    {!! Form::label('meta_keywords', __('models/posts.fields.meta_keywords').':') !!}
    <p>{{ $post->meta_keywords }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', __('models/posts.fields.status').':') !!}
    <p>{{ $post->status }}</p>
</div>

<!-- Featured Field -->
<div class="form-group">
    {!! Form::label('featured', __('models/posts.fields.featured').':') !!}
    <p>{{ $post->featured }}</p>
</div>

