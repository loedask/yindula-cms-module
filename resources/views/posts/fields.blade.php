<div class="form-group col-sm-6">

    <!-- Title Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('title', __('models/posts.fields.title') . ':') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Featured Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('featured', __('models/posts.fields.featured') . ':') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('featured', 0) !!}
            {!! Form::checkbox('featured', '1', null) !!}
        </label>
    </div>

    <!-- Excerpt Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('excerpt', __('models/posts.fields.excerpt') . ':') !!}
        {!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Title Field -->
    <div class="form-group">
        @if (!empty($feature->image))
            <p>
                <img src="{{ yindula_image($feature->image) }}" alt="">
            </p>
        @endif
    </div>

</div>

<div class="form-group col-sm-6">

    <!-- Seo Title Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('seo_title', __('models/posts.fields.seo_title') . ':') !!}
        {!! Form::text('seo_title', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Image Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('image', __('models/posts.fields.image') . ':') !!}
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Meta Description Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('meta_description', __('models/posts.fields.meta_description') . ':') !!}
        {!! Form::textarea('meta_description', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Meta Keywords Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('meta_keywords', __('models/posts.fields.meta_keywords') . ':') !!}
        {!! Form::textarea('meta_keywords', null, ['class' => 'form-control']) !!}
    </div>

</div>

<!-- Body Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('body', __('models/posts.fields.body') . ':') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cms.posts.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>


@include('yindulacms::common.filepond_image')
