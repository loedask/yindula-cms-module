<!-- Author Id Field -->
<div class="form-group col-sm-6" style="display: none">
    {!! Form::label('author_id', __('models/posts.fields.author_id') . ':') !!}
    {!! Form::number('author_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-12" style="display: none">
    {!! Form::label('category_id', __('models/posts.fields.category_id') . ':') !!}
    {!! Form::number('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6" style="display: none">
    {!! Form::label('slug', __('models/posts.fields.slug') . ':') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>


<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', __('models/posts.fields.status') . ':') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>
