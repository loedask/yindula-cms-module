@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('models/menus.singular') @lang('crud.details')
@endsection

@section('content')

    <section class="section">

        <div class="section-header">
            <h1>@lang('models/menus.singular') @lang('crud.details')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.menus.index') }}" class="btn btn-primary form-btn float-right">@lang('crud.back')</a>
            </div>
        </div>

        @include('bazintemplate::common.errors')

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('yindulacms::menus.show_fields')
                </div>
                @livewire('yindulacms::menu.parents',['menuId'=>$menu->id])
            </div>
        </div>
    </section>

@endsection
