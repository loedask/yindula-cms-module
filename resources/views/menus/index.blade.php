@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('models/menus.plural')
@endsection

@section('content')
    <section class="section">
        <div class="section-header">

            <h1>@lang('models/menus.plural')</h1>

            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.menus.create') }}" class="btn btn-primary form-btn">
                    @lang('crud.add_new')
                    <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    {{-- @include('yindulacms::menus.table') --}}
                    <livewire:yindulacms::menu.menu-builder />
                </div>
            </div>
        </div>

    </section>
@endsection
