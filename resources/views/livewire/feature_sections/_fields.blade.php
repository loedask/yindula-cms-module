<div class="form-group col-sm-6">
    <!-- Description Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('description', __('models/featureSections.fields.description') . ':') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control', 'wire:model' => 'description']) !!}
    </div>
</div>



<div class="form-group col-sm-6">
    <!-- Is Active Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('is_active', __('models/featureSections.fields.is_active') . ':') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('is_active', 0, ['wire:model' => 'is_active']) !!}
            {!! Form::checkbox('is_active', '1', null, ['wire:model' => 'is_active']) !!}
        </label>
    </div>

    <!-- Name Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('name', __('models/featureSections.fields.name') . ':') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'wire:model' => 'name']) !!}
    </div>

    <!-- Title Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('title', __('models/featureSections.fields.title') . ':') !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'wire:model' => 'title']) !!}
    </div>

    <!-- Image Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('image', __('models/pages.fields.image') . ':') !!}
        {!! Form::file('image', ['wire:model' => 'image']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary', 'wire:click' => 'save']) !!}
    <a href="{{ route('cms.featureSections.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>

{{-- <x:yindula-cms::ckeditor :content="$description" /> --}}

{{-- @include('yindula-cms::common.ckeditor') --}}
