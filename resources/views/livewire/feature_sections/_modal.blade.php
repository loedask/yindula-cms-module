<div class="modal" @if ($showModal) style="display:block" @endif>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form wire:submit.prevent="save">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $featureSectionId ? 'Edit Income' : 'Add New Income' }}</h5>
                    <button wire:click="close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-secondary">
                    {{-- @include('yindulacms::livewire.feature_sections._fields') --}}
                </div>
                <div class="modal-footer">
                    <button wire:click="close" type="button" class="btn btn-secondary" data-dismiss="modal">Close
                    </button>
                    <button type="submit"
                        class="btn btn-primary">{{ $featureSectionId ? 'Save Changes' : 'Save' }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
