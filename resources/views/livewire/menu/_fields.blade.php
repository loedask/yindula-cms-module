<!-- Title Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', __('models/menuItems.fields.title') . ':') !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'wire:model' => 'model.title']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url', __('models/menuItems.fields.url') . ':') !!}
    {!! Form::text('url', null, ['class' => 'form-control', 'wire:model' => 'model.url']) !!}
</div>

<!-- Target Field -->
<div class="form-group col-sm-12" style="display: none">
    {!! Form::label('target', __('models/menuItems.fields.target') . ':') !!}
    {!! Form::text('target', null, ['class' => 'form-control', 'wire:model' => 'model.target']) !!}
</div>
