<div class="modal" @if ($showModal) style="display:block" @endif>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form wire:submit.prevent="save">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $menuItemId ? 'Edit Saving' : 'Add New Saving' }}</h5>
                    <button wire:click="close" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-secondary">
                    @include('yindulacms::livewire.menu._fields')
                </div>
                <div class="modal-footer">
                    <button wire:click="close" type="button" class="btn btn-secondary" data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary">{{ $menuItemId ? 'Save Changes' : 'Save' }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
