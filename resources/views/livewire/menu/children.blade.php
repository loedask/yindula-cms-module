<div>

    <div class="card-header">

        @if (count($children_items) > 0)
            <h4>Children Items</h4>
        @else
            <h4>No Children Items</h4>
        @endif

        <div class="card-header-action">

            <button wire:click.prevent="create" class="btn btn-icon btn-success">
                <i class="fas fa-plus"></i>
            </button>

        </div>

    </div>


    <div class="table-responsive">

        @if (count($children_items) > 0)

            <table class="table" id="menus-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('models/menuItems.fields.title')</th>
                        <th colspan="3">@lang('crud.action')</th>
                    </tr>
                </thead>
                <tbody wire:sortable="updateOrder">
                    @foreach ($children_items as $key => $child_item)

                        <tr wire:sortable.item="{{ $child_item->id }}" wire:key="{{ time() . $child_item->id }}">

                            <td>
                                {{ $key + 1 }}
                            </td>

                            <td>
                                <span wire:sortable.handle style="cursor: move;">{{ $child_item->title }}</span>
                            </td>

                            <td class=" text-center">
                                <div class='btn-group'>

                                    <button wire:click.prevent="edit({{ $child_item->id }})" href="#"
                                        class="btn btn-warning action-btn edit-btn">
                                        <span class="fa fa-edit"></span>
                                    </button>

                                    <button wire:click.prevent="delete({{ $child_item->id }})"
                                        onclick="confirm(__('crud.are_you_sure')) || event.stopImmediatePropagation()"
                                        class="btn btn-danger action-btn delete-btn">
                                        <span class="fa fa-trash"></span>
                                    </button>

                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        @endif

    </div>

    @include('yindulacms::livewire.menu._modal')

</div>
