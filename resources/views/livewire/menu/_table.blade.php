<div class="table-responsive">
    <table class="table" id="menus-table">
        <thead>
            <tr>
                <th>@lang('models/menus.fields.menu_id')</th>
                <th>@lang('models/menus.fields.title')</th>
                <th>@lang('models/menus.fields.url')</th>
                <th>@lang('models/menus.fields.target')</th>
                <th>@lang('models/menus.fields.icon_class')</th>
                <th>@lang('models/menus.fields.color')</th>
                <th>@lang('models/menus.fields.parent_id')</th>
                <th>@lang('models/menus.fields.order')</th>
                <th>@lang('models/menus.fields.route')</th>
                <th>@lang('models/menus.fields.parameters')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($menus as $menu)
                <tr>
                    <td>{{ $menu->menu_id }}</td>
                    <td>{{ $menu->title }}</td>
                    <td>{{ $menu->url }}</td>
                    <td>{{ $menu->target }}</td>
                    <td>{{ $menu->icon_class }}</td>
                    <td>{{ $menu->color }}</td>
                    <td>{{ $menu->parent_id }}</td>
                    <td>{{ $menu->order }}</td>
                    <td>{{ $menu->route }}</td>
                    <td>{{ $menu->parameters }}</td>
                    <td class=" text-center">
                        {!! Form::open(['route' => ['menus.destroy', $menu->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{!! route('menus.show', [$menu->id]) !!}" class='btn btn-light action-btn '><i
                                    class="fa fa-eye"></i></a>
                            <a href="{!! route('menus.edit', [$menu->id]) !!}" class='btn btn-warning action-btn edit-btn'><i
                                    class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("' . __('crud.are_you_sure') . '")']) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
