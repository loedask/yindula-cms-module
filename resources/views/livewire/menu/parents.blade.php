<div>


    <div class="card-header">
        <h4>Parents Items</h4>

        <div class="card-header-action">

            <button wire:click.prevent="create" class="btn btn-icon btn-success">
                <i class="fas fa-plus"></i>
            </button>

        </div>

    </div>

    <div class="table-responsive">
        <table class="table" id="menus-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('models/menuItems.fields.title')</th>
                    <th colspan="3">@lang('crud.action')</th>
                </tr>
            </thead>
            <tbody wire:sortable="updateOrder">
                @foreach ($parent_items as $key => $parent_item)

                    <tr wire:sortable.item="{{ $parent_item->id }}" wire:key="{{ time() . $parent_item->id }}">

                        <td>
                            {{ $key + 1 }}
                        </td>

                        <td>
                            <span wire:sortable.handle style="cursor: move;">{{ $parent_item->title }}</span>

                            <livewire:yindulacms::menu.children :menuId="$parent_item->menu_id" :parentId="$parent_item->id"
                                :key="time().$parent_item->id" />
                        </td>

                        <td class=" text-center">
                            <div class='btn-group'>

                                <button wire:click.prevent="edit({{ $parent_item->id }})" href="#"
                                    class="btn btn-warning action-btn edit-btn">
                                    <span class="fa fa-edit"></span>
                                </button>

                                <button wire:click.prevent="delete({{ $parent_item->id }})"
                                    onclick="confirm(__('crud.are_you_sure')) || event.stopImmediatePropagation()"
                                    class="btn btn-danger action-btn delete-btn">
                                    <span class="fa fa-trash"></span>
                                </button>

                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>


    @include('yindulacms::livewire.menu._modal')

</div>
