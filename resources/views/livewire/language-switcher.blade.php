<ul class="nav navbar-nav">
    <li class="dropdown">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Languages ({{$country_model->languageName(app()->getLocale())}})
            <b class="caret"></b>
        </a>

        <ul class="dropdown-menu">
            <!-- <li class="divider"></li> -->
            <!-- <li class="dropdown-header">Nav header</li> -->
            @foreach($countries as $item)
            <li>
                <a href="locale/{{$item->language_code}}" title="{{$item->name}}">
                    {!! $country_model->findFlagByCountryCode($item->iso_3166_2) !!}
                    {{$country_model->languageName($item->language_code)}}
                </a>
            </li>

            <li class="divider"></li>
            @endforeach
        </ul>

    </li>
</ul>
