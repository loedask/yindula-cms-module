@php
    use Spatie\Menu\Laravel\Menu as SpatieMenu;

    SpatieMenu::macro('main', function () {
        return SpatieMenu::new()
            ->route('cms.dashboard', 'Home')
            ->route('cms.cmsSettings.index', 'Home')
            ->route('cms.menus.index', 'Home');
    });

@endphp

{!! SpatieMenu::main() !!}
