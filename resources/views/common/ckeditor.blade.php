

    @push('page_js')
        <script src="https://cdn.ckeditor.com/ckeditor5/31.1.0/classic/ckeditor.js"></script>
        <script>
            document.addEventListener('livewire:init', () => {
                ClassicEditor
                    .create(document.querySelector('#description'))
                    .then(editor => {
                        editor.model.document.on('change:data', () => {
                            @this.set('description', editor.getData());
                        });
                        Livewire.on('reinit', () => {
                            editor.setData('', '');
                        });
                    })
                    .catch(error => {
                        console.error(error);
                    });
            });
        </script>
    @endpush

    @push('page_css')
        <style>
            .ck-editor__editable_inline {
                min-height: 200px;
            }
        </style>
    @endpush

