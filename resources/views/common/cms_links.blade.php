@if (Module::find('YindulaCms'))
    @if (Module::find('YindulaCms')->isEnabled())
        <a class="dropdown-item has-icon" target="_blank" href="/">
            <i class="fas fa-home"></i>
            Home Page
        </a>

        <a class="dropdown-item has-icon" href="{{ route('cms.dashboard') }}">
            <i class="fas fa-tablet-alt"></i>
            CMS
        </a>
    @endif
@endif
