@component('mail::message')
# {{$name}}

{{$message}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
