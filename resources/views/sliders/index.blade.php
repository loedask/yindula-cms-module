@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('models/sliders.plural')
@endsection

@section('content')
    <section class="section">
        <div class="section-header">

            <h1>
                @lang('models/sliders.plural')
            </h1>

            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.sliders.create') }}" class="btn btn-primary form-btn">
                    @lang('models/sliders.singular')
                    <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <livewire:yindulacms::sliders-table />
                </div>
            </div>
        </div>

    </section>
@endsection
