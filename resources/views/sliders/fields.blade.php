<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Excerpt Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('excerpt', 'Excerpt:') !!}
    {!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, ['class' => 'form-control','maxlength' => 125,'maxlength' => 125]) !!}
</div>

<!-- Video Field -->
<div class="form-group col-sm-6">
    {!! Form::label('video', 'Video:') !!}
    {!! Form::file('video', null, ['class' => 'form-control','maxlength' => 125,'maxlength' => 125]) !!}
</div>

<!-- Is Default Field -->
<div class="form-group col-sm-6" style="padding-top: 37px">
    <div class="form-check">
        {!! Form::checkbox('is_default', '1', null, ['id' => 'is_default']) !!}
        <label class="form-check-label" for="is_default">
           is_default
        </label>
    </div>
</div>

<!-- Image Two Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('image_two', 'Image Two:') !!}
    {!! Form::file('image_two', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cms.sliders.index') }}" class="btn btn-light">Cancel</a>
</div>


@include('yindulacms::sliders._filepond_image')
