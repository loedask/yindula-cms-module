@extends('bazintemplate::layouts.authed')
@section('title')
    Banner Details
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
        <h1>Banner Details</h1>
        <div class="section-header-breadcrumb">
            <a href="{{ route('cms.sliders.index') }}"
                 class="btn btn-primary form-btn float-right">Back</a>
        </div>
      </div>
   @include('bazintemplate::common.errors')
    <div class="section-body">
           <div class="card">
            <div class="card-body">
                    @include('yindulacms::sliders.show_fields')
            </div>
            </div>
    </div>
    </section>
@endsection
