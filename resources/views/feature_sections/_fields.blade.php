<div class="form-group col-sm-6">
    
    @if (isset($page))
        <!-- Hidden Page Id Field -->
        <div class="form-group col-sm-12">
            {!! Form::hidden('page_id', $page->id) !!}
        </div>
    @else
        <!-- Page Id Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('page_id', __('yindulacms::models/featureSections.fields.page_id') . ':') !!}
            {!! Form::select('page_id', $pageItems, null, ['class' => ['form-control', 'select2']]) !!}
        </div>
    @endif

    <!-- Category Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('category_id', __('yindulacms::models/featureSections.fields.category_id') . ':') !!}
        {!! Form::select('category_id', $categoryItems, null, ['class' => ['form-control', 'select2']]) !!}
    </div>

    <!-- Description Field style="display: none" -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('description', __('yindulacms::models/featureSections.fields.description') . ':') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

</div>

<div class="form-group col-sm-6">

    <!-- Is Active Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('is_active', __('yindulacms::models/featureSections.fields.is_active') . ':') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('is_active', 0) !!}
            {!! Form::checkbox('is_active', '1', null) !!}
        </label>
    </div>

    <!-- Name Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('name', __('yindulacms::models/featureSections.fields.name') . ':') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Title Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('title', __('yindulacms::models/featureSections.fields.title') . ':') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Image Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('image', __('yindulacms::models/pages.fields.image') . ':') !!}
        {!! Form::file('image') !!}
    </div>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cms.featureSections.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>


@include('yindulacms::common.filepond_image')
