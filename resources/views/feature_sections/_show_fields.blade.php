
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/featureSections.fields.name').':') !!}
    <p>{{ $featureSection->name }}</p>
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', __('models/featureSections.fields.title').':') !!}
    <p>{{ $featureSection->title }}</p>
</div>

<!-- Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('key', __('models/featureSections.fields.key').':') !!}
    <p>{{ $featureSection->key }}</p>
</div>


<!-- Page Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('page_id', __('models/featureSections.fields.page_id').':') !!}
    <p>{{ $featureSection->page_id }}</p>
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', __('models/featureSections.fields.description').':') !!}
    <p>{{ $featureSection->description }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_active', __('models/featureSections.fields.is_active').':') !!}
    <p>{{ $featureSection->is_active }}</p>
</div>

