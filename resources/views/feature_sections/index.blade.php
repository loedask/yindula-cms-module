@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('models/featureSections.plural')
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/featureSections.plural')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.featureSections.create') }}" class="btn btn-primary form-btn">@lang('crud.add_new')
                    <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <livewire:yindulacms::feature-sections.feature-sections-table />
                </div>
            </div>
        </div>

    </section>
@endsection
