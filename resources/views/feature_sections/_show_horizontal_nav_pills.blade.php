 <div class="row">
     <div class="col-12 col-sm-12 col-lg-12">
         <div class="card">
             <div class="card-header">

                 <div class="section-header-breadcrumb">
                    <a href="{{ route('cms.featureSections.add', ['pageId' => $page->id]) }}" class="btn btn-primary form-btn">
                        @lang('crud.add_new') Section
                        <i class="fas fa-plus"></i>
                    </a>
                </div>
{{--
                <div class="section-header-breadcrumb">
                    <a href="{{ route('cms.features.create') }}" class="btn btn-primary form-btn">
                        @lang('crud.add_new') Feature
                        <i class="fas fa-plus"></i>
                    </a>
                </div> --}}

             </div>
             <div class="card-body">
                 <ul class="nav nav-pills" id="myTab3" role="tablist">
                     <li class="nav-item">
                         <a class="nav-link active show" id="sections-tab3" data-toggle="tab" href="#sections3"
                             role="tab" aria-controls="sections" aria-selected="true">
                             Section List
                         </a>
                     </li>
                     {{-- <li class="nav-item">
                         <a class="nav-link" id="features-tab3" data-toggle="tab" href="#features3" role="tab"
                             aria-controls="features" aria-selected="false">
                             Create Form
                         </a>
                     </li> --}}
                 </ul>
                 <div class="tab-content" id="myTabContent2">
                     <div class="tab-pane fade active show" id="sections3" role="tabpanel"
                         aria-labelledby="sections-tab3">
                         <div class="row">
                             <livewire:yindulacms::feature-sections.feature-sections-table :page="$page"
                                 :key="time() . $page->id" />
                         </div>
                     </div>
                     <div class="tab-pane fade" id="features3" role="tabpanel" aria-labelledby="features-tab3">
                         <livewire:yindulacms::feature-sections.create-or-update-form :page="$page"
                             :key="time() . $page->id" />
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
