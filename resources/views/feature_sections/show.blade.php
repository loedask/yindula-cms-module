@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('models/featureSections.singular') @lang('crud.details')
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/featureSections.singular') @lang('crud.details')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.featureSections.index') }}"
                    class="btn btn-primary form-btn float-right">@lang('crud.back')</a>
            </div>
        </div>

        @include('bazintemplate::common.errors')

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        @include('yindulacms::feature_sections._show_fields')
                    </div>
                    @livewire('yindulacms::features-table', ['featureSection' => $featureSection])
                </div>
            </div>
        </div>
    </section>
@endsection
