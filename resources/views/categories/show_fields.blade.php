<!-- Parent Id Field -->
<div class="form-group">
    {!! Form::label('parent_id', __('models/categories.fields.parent_id').':') !!}
    <p>{{ $category->parent_id }}</p>
</div>

<!-- Order Field -->
<div class="form-group">
    {!! Form::label('order', __('models/categories.fields.order').':') !!}
    <p>{{ $category->order }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/categories.fields.name').':') !!}
    <p>{{ $category->name }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', __('models/categories.fields.slug').':') !!}
    <p>{{ $category->slug }}</p>
</div>

