@extends('bazintemplate::layouts.authed')
@section('title')
    Cms Settings
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Cms Settings</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.cmsSettings.create')}}" class="btn btn-primary form-btn">Cms Setting <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                <livewire:yindulacms::cms-settings-table />
            </div>
       </div>
   </div>

    </section>
@endsection

