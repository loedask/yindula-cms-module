@section('css')
    @include('yindulacms::layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}

@push('scripts')
    @include('yindulacms::layouts.datatables_js')
    {!! $dataTable->scripts() !!}
@endpush
