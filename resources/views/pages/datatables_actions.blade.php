<div class='btn-group'>
    <a href="{{ $showUrl }}" class='btn btn-primary btn-xs'>
        <i class="fa fa-list"></i>
    </a>
    <a href="{{ $editUrl }}" class='btn btn-secondary btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    <a class='btn btn-danger btn-xs' wire:click="deleteRecord({{ $recordId }})"
        onclick="confirm('Are you sure you want to remove this Record?') || event.stopImmediatePropagation()">
        <i class="fa fa-trash"></i>
    </a>

    <a class='btn btn-info btn-xs' wire:click="setHomePage({{ $homePageId }})" title="{{ __('Set Home Page') }}"
        onclick="confirm('Are you sure you want to update this Record?') || event.stopImmediatePropagation()">
        <i class="fa fa-home"></i>
    </a>
    <a class='btn btn-secondary btn-xs' wire:click="setEventPage({{ $eventPageId }})"
        title="{{ __('Set Event Page') }}"
        onclick="confirm('Are you sure you want to update this Record?') || event.stopImmediatePropagation()">
        <i class="fa fa-calendar"></i>
    </a>
    <a class='btn btn-info btn-xs' wire:click="setContactPage({{ $contactPageId }})"
        title="{{ __('Set Contact Page') }}"
        onclick="confirm('Are you sure you want to update this Record?') || event.stopImmediatePropagation()">
        <i class="fa fa-at"></i>
    </a>
</div>
