<div class="form-group col-sm-6">

    <!-- Title Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('title', __('yindulacms::models/pages.fields.title') . ':') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    @if ($page_setting && $page_setting->has_subtitle)
        <!-- Subtitle Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('subtitle', __('yindulacms::models/pages.fields.subtitle') . ':') !!}
            {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
        </div>
    @endif

    <!-- Is Home Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('is_home', __('yindulacms::models/pages.fields.is_home') . ':') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('is_home', 0) !!}
            {!! Form::checkbox('is_home', '1', null, ['id' => 'is_home_checkbox']) !!}
        </label>
    </div>

    <!-- Is Page Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('is_page', __('yindulacms::models/pages.fields.is_page') . ':') !!}
        <label class="checkbox-inline">
            {!! Form::hidden('is_page', 0) !!}
            {!! Form::checkbox('is_page', '1', null, ['id' => 'is_page_checkbox']) !!}
        </label>
    </div>

    @if ($page_setting && $page_setting->has_excerpt)
        <!-- Excerpt Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('excerpt', __('yindulacms::models/pages.fields.excerpt') . ':') !!}
            {!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
        </div>
    @endif

</div>

<div class="form-group col-sm-6">

    @if ($page_setting && $page_setting->has_image)
        <!-- Image Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('image', __('yindulacms::models/pages.fields.image') . ':') !!}
            {!! Form::file('image') !!}
        </div>
    @endif

    <!-- Status Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('status', __('yindulacms::models/pages.fields.status') . ':') !!}
        {!! Form::select('status', $statuses, ['class' => 'form-control select2']) !!}
    </div>


    @if ($page_setting && $page_setting->has_meta_description)
        <!-- Meta Description Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('meta_description', __('yindulacms::models/pages.fields.meta_description') . ':') !!}
            {!! Form::textarea('meta_description', null, ['class' => 'form-control']) !!}
        </div>
    @endif

    @if ($page_setting && $page_setting->has_meta_keywords)
        <!-- Meta Keywords Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('meta_keywords', __('yindulacms::models/pages.fields.meta_keywords') . ':') !!}
            {!! Form::textarea('meta_keywords', null, ['class' => 'form-control']) !!}
        </div>
    @endif

</div>

@if ($page_setting && $page_setting->has_body)
    <!-- Body Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('body', __('yindulacms::models/pages.fields.body') . ':') !!}
        {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cms.pages.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>

@include('yindulacms::common.filepond_image')
