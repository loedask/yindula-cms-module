<div class="col-md-6">
    <div class="row">

        <!-- Title Field -->
        <div class="form-group col-md-6">
            {!! Form::label('title', __('models/pages.fields.title') . ':') !!}
            <p>{{ $page->title }}</p>
        </div>


        @if ($page_setting && $page_setting->has_subtitle)
            <!-- Subtitle Field -->
            <div class="form-group col-md-6">
                {!! Form::label('subtitle', __('models/pages.fields.subtitle') . ':') !!}
                <p>{{ $page->subtitle }}</p>
            </div>
        @endif

        <!-- Author Id Field -->
        <div class="form-group col-md-6">
            {!! Form::label('author_id', __('models/pages.fields.author_id') . ':') !!}
            <p>{{ $page->author->name ?? '' }}</p>
        </div>

        <!-- Status Field -->
        <div class="form-group col-md-4">
            {!! Form::label('status', __('models/pages.fields.status') . ':') !!}
            <p>{{ $page->status }}</p>
        </div>

        <!-- Is Home Field -->
        <div class="form-group col-md-4">
            {!! Form::label('is_home', __('models/pages.fields.is_home') . ':') !!}
            <p>{{ $page->is_home }}</p>
        </div>

        <!-- Is Page Field -->
        <div class="form-group col-md-4">
            {!! Form::label('is_page', __('models/pages.fields.is_page') . ':') !!}
            <p>{{ $page->is_page }}</p>
        </div>

        <!-- Image Field -->
        <div class="form-group col-sm-12">
            <x-bazintemplate::image-component :src="$page->image_url" :alt="__('models/pages.fields.image')" />
        </div>
    </div>
</div>
<div class="col-md-6">

    @if ($page_setting && $page_setting->has_meta_description)
        <!-- Meta Description Field -->
        <div class="form-group col-md-12">
            {!! Form::label('meta_description', __('models/pages.fields.meta_description') . ':') !!}
            <p>{{ $page->meta_description }}</p>
        </div>
    @endif

    @if ($page_setting && $page_setting->has_meta_keywords)
        <!-- Meta Keywords Field -->
        <div class="form-group col-md-12">
            {!! Form::label('meta_keywords', __('models/pages.fields.meta_keywords') . ':') !!}
            <p>{{ $page->meta_keywords }}</p>
        </div>
    @endif

    @if ($page_setting && $page_setting->has_excerpt)
        <!-- Excerpt Field -->
        <div class="form-group col-md-12">
            {!! Form::label('excerpt', __('models/pages.fields.excerpt') . ':') !!}
            <p>{!! $page->excerpt !!}</p>
        </div>
    @endif

    @if ($page_setting && $page_setting->has_body)
        <!-- Body Field -->
        <div class="form-group col-md-12">
            {!! Form::label('body', __('models/pages.fields.body') . ':') !!}
            <p>{!! $page->body !!}</p>
        </div>
    @endif
</div>
