@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('yindulacms::models/pages.singular') @lang('yindulacms::crud.details')
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('yindulacms::models/pages.singular')
                @lang('yindulacms::crud.details')
            </h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.pages.index') }}" class="btn btn-primary form-btn float-right">
                    @lang('yindulacms::crud.back')
                </a>
                <a href="{{ route('cms.pages.edit', $page->id) }}" class="btn btn-secondary form-btn">
                    {{-- @lang('yindulacms::crud.edit') --}}
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{ route('cms.pages.create') }}" class="btn btn-primary form-btn">
                    <i class="fas fa-plus"></i>
                </a>
                <a href="{{ route('cms.pageSettings.createOrEdit', ['pageId' => $page->id]) }}"
                    class="btn btn-secondary form-btn">
                    <i class="fas fa-cogs"></i>
                </a>
            </div>
        </div>

        @include('bazintemplate::common.errors')

        <div class="section-body">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        @include('yindulacms::pages._show_fields')
                    </div>
                    @include('yindulacms::pages._show_vertical_nav_pills')

                </div>
            </div>
        </div>

    </section>
@endsection

@push('page_js')
    <script type="importmap">
    {
      "imports": {
        "@popperjs/core": "https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/esm/popper.min.js",
        "bootstrap": "https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.esm.min.js"
      }
    }
    </script>


    <script type="module">
        import {
            Modal
        } from 'bootstrap';

        document.addEventListener('livewire:init', () => {
            Livewire.on('closeModal', ({
                modalId
            }) => {
                const modalsElement = document.querySelector(modalId)
                let modal = Modal.getInstance(modalsElement);

                modal.hide()
            })

            let modalsElement = document.querySelector('#productForm');

            modalsElement.addEventListener('hidden.bs.modal', () => {
                Livewire.dispatch('resetModal');
            });
        })
    </script>
@endpush
