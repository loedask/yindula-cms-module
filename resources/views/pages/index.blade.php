@extends('bazintemplate::layouts.authed')

@section('title')
    @lang('yindulacms::models/pages.plural')
@endsection

@section('content')
    <section class="section">

        <div class="section-header">

            <h1>@lang('yindulacms::models/pages.plural')</h1>

            <div class="section-header-breadcrumb">
                <a href="{{ route('cms.pages.create') }}" class="btn btn-primary form-btn">
                    @lang('yindulacms::crud.add_new')
                    <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <livewire:yindulacms::pages-table />
                </div>
            </div>
        </div>

    </section>
@endsection
