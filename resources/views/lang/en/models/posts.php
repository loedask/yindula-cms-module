<?php

return array (
  'singular' => 'Post',
  'plural' => 'Posts',
  'fields' =>
  array (
    'author_id' => 'Author',
    'category_id' => 'Category',
    'title' => 'Title',
    'alt_text' => 'ALT Text',
    'seo_title' => 'Seo Title',
    'excerpt' => 'Excerpt',
    'body' => 'Body',
    'image' => 'Image',
    'slug' => 'Slug',
    'meta_description' => 'Meta Description',
    'meta_keywords' => 'Meta Keywords',
    'status' => 'Status',
    'featured' => 'Featured',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
