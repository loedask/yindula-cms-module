<?php

return [
    'singular' => 'Banner',
    'plural' => 'Banners',
    'fields' => [
        'title' => 'Title',
        'excerpt' => 'Excerpt',
        'description' => 'Description',
        'image' => 'Image',
        'video' => 'Video',
        'is_default' => 'Is Default',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
        'image_two' => 'Image Two',
    ],
];
