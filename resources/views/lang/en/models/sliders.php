<?php

return array (
  'singular' => 'Slider',
  'plural' => 'Sliders',
  'fields' => 
  array (
    'title' => 'Title',
    'subtitle' => 'Subtitle',
    'excerpt' => 'Excerpt',
    'description' => 'Description',
    'image' => 'Image',
    'image_two' => 'Image Two',
    'video' => 'Video',
    'url' => 'Url',
    'is_default' => 'Is Default',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
