<?php

return array (
  'singular' => 'Menu',
  'plural' => 'Menus',
  'fields' => 
  array (
    'name' => 'Name',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
