<?php

return [
    'singular' => 'FeatureSection',
    'plural' => 'FeatureSections',
    'fields' => [
        'category_id' => 'Category Id',
        'key' => 'Key',
        'name' => 'Name',
        'title' => 'Title',
        'image' => 'Image',
        'page_id' => 'Page Id',
        'description' => 'Description',
        'is_active' => 'Is Active',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ],
];
