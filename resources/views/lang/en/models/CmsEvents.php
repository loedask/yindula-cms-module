<?php

return [
    'singular' => 'Event',
    'plural' => 'Events',
    'fields' => [
        'title' => 'Title',
        'description' => 'Description',
        'start_date' => 'Start Date',
        'end_date' => 'End Date',
        'image' => 'Image',
        'start_time' => 'Start Time',
        'end_time' => 'End Time',
        'is_full_day' => 'Is Full Day',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ],
];
