<?php

return [
    'singular' => 'Feature',
    'plural' => 'Features',
    'fields' => [
        'category_id' => 'Category Id',
        'feature_section_id' => 'Feature Section Id',
        'title' => 'Title',
        'content' => 'Content',
        'excerpt' => 'Excerpt',
        'page_id' => 'Page Id',
        'description' => 'Description',
        'image' => 'Image',
        'sort_order' => 'Sort Order',
        'is_active' => 'Is Active',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
        'icon' => 'Icon',
        'url' => 'Url',
    ],
];
