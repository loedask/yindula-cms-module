<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', __('models/cmsEvents.fields.title') . ':') !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'required', 'maxlength' => 255, 'maxlength' => 255]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', __('models/cmsEvents.fields.description') . ':') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'maxlength' => 65535, 'maxlength' => 65535]) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', __('models/cmsEvents.fields.start_date') . ':') !!}
    {!! Form::date('start_date', null, ['class' => 'form-control', 'id' => 'start_date']) !!}
</div>

{{-- @push('scripts')
    <script type="text/javascript">
        $('#start_date').datepicker()
    </script>
@endpush --}}

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', __('models/cmsEvents.fields.end_date') . ':') !!}
    {!! Form::date('end_date', null, ['class' => 'form-control', 'id' => 'end_date']) !!}
</div>
{{--
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
        // $('#end_date').datepicker()
        $('.input-group.end_date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });
    </script>
@endpush --}}

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/cmsEvents.fields.image') . ':') !!}
    {!! Form::file('image', null, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255]) !!}
</div>

@include('yindulacms::cms_events._filepond')


<!-- Start Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_time', __('models/cmsEvents.fields.start_time') . ':') !!}
    {!! Form::time('start_time', null, ['class' => 'form-control']) !!}
</div>

<!-- End Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_time', __('models/cmsEvents.fields.end_time') . ':') !!}
    {!! Form::time('end_time', null, ['class' => 'form-control', 'id' => 'end_time']) !!}
</div>

{{-- --}}
@push('page_scripts')
    <script type="text/javascript">
        $('#end_time').datepicker()
    </script>
@endpush

<!-- Is Full Day Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('is_full_day', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('is_full_day', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('is_full_day', 'Is Full Day', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cms.cmsEvents.index') }}" class="btn btn-light">
        @lang('crud.cancel')
    </a>
</div>
