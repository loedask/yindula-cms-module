<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', __('models/cmsEvents.fields.title').':') !!}
    <p>{{ $cmsEvent->title }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', __('models/cmsEvents.fields.description').':') !!}
    <p>{{ $cmsEvent->description }}</p>
</div>

<!-- Start Date Field -->
<div class="col-sm-12">
    {!! Form::label('start_date', __('models/cmsEvents.fields.start_date').':') !!}
    <p>{{ $cmsEvent->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="col-sm-12">
    {!! Form::label('end_date', __('models/cmsEvents.fields.end_date').':') !!}
    <p>{{ $cmsEvent->end_date }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    {!! Form::label('image', __('models/cmsEvents.fields.image').':') !!}
    <p>{{ $cmsEvent->image }}</p>
</div>

<!-- Start Time Field -->
<div class="col-sm-12">
    {!! Form::label('start_time', __('models/cmsEvents.fields.start_time').':') !!}
    <p>{{ $cmsEvent->start_time }}</p>
</div>

<!-- End Time Field -->
<div class="col-sm-12">
    {!! Form::label('end_time', __('models/cmsEvents.fields.end_time').':') !!}
    <p>{{ $cmsEvent->end_time }}</p>
</div>

<!-- Is Full Day Field -->
<div class="col-sm-12">
    {!! Form::label('is_full_day', __('models/cmsEvents.fields.is_full_day').':') !!}
    <p>{{ $cmsEvent->is_full_day }}</p>
</div>

