<a href="/" target="_blank" class="dropdown-item has-icon">
    <i class="fas fa-home"></i>
    <span class="dropdown-menu__caption">@lang('Home Page')</span>
</a>

<a href="{{ route('cms.dashboard') }}" class="dropdown-item has-icon" target="_blank">
    <i class="dropdown-menu__icon fas fa-book"></i>
    <span class="dropdown-menu__caption">
        @lang('CMS')
    </span>
</a>
