<li class="side-menus {{ Request::is('cms/dashboard*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('cms.dashboard') }}">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>

<li class="side-menus {{ Request::is('cms/cmsSettings*') ? 'active' : '' }}">
    <a href="{{ route('cms.cmsSettings.index') }}">
        <i class="fas fa-cogs"></i>
        <span>
            @lang('yindulacms::models/cmsSettings.plural')
        </span>
    </a>
</li>

<li class="side-menus {{ Request::is('cms/menus*') ? 'active' : '' }}">
    <a href="{{ route('cms.menus.index') }}">
        <i class="fa fa-bars"></i><span>@lang('yindulacms::models/menus.plural')</span></a>
</li>

@if (yindula_setting('cms', 'has_categories') === 'yes')
    <li class="side-menus {{ Request::is('cms/categories*') ? 'active' : '' }}">
        <a href="{{ route('cms.categories.index') }}">
            <i class="fa fa-sitemap"></i>
            <span>
                @lang('yindulacms::models/categories.plural')
            </span>
        </a>
    </li>
@endif


@if (yindula_setting('cms', 'has_banners') === 'yes')
    <li class="side-menus {{ Request::is('cms/banners*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('cms.banners.index') }}">
            <i class="fas fa-flag"></i>
            <span>Banners</span>
        </a>
    </li>
@endif


@if (yindula_setting('cms', 'has_sliders') === 'yes')
    <li class="side-menus {{ Request::is('cms/sliders*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('cms.sliders.index') }}">
            <i class="fas fa-sliders-h"></i>
            <span>Sliders</span>
        </a>
    </li>
@endif

@if (yindula_setting('cms', 'has_posts') === 'yes')
    <li class="side-menus {{ Request::is('cms/posts*') ? 'active' : '' }}">
        <a href="{{ route('cms.posts.index') }}">
            <i class="fa fa-edit"></i>
            <span>@lang('yindulacms::models/posts.plural')
            </span>
        </a>
    </li>
@endif


@if (yindula_setting('cms', 'has_events') === 'yes')
    <li class="side-menus {{ Request::is('cms/cmsEvents*') ? 'active' : '' }}">
        <a href="{{ route('cms.cmsEvents.index') }}">
            <i class="fa fa-calendar-alt"></i>
            <span>@lang('yindulacms::models/cmsEvents.plural')
            </span>
        </a>
    </li>
@endif

<li class="side-menus {{ Request::is('cms/pages*') ? 'active' : '' }}">
    <a href="{{ route('cms.pages.index') }}">
        <i class="fa fa-file"></i>
        <span>@lang('yindulacms::models/pages.plural')
        </span>
    </a>
</li>


@if (yindula_setting('cms', 'has_feature_sections') === 'yes')
    <li class="side-menus {{ Request::is('cms/featureSections*') ? 'active' : '' }}">
        <a href="{{ route('cms.featureSections.index') }}">
            <i class="fa fa-th"></i>
            <span>
                @lang('yindulacms::models/featureSections.plural')
            </span>
        </a>
    </li>
@endif


@if (yindula_setting('cms', 'has_features') === 'yes')
    <li class="side-menus {{ Request::is('cms/features*') ? 'active' : '' }}">
        <a href="{{ route('cms.features.index') }}">
            <i class="fa fa-star"></i>
            <span>@lang('yindulacms::models/features.plural')</span>
        </a>
    </li>
@endif
