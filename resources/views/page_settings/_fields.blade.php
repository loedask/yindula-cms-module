@if (isset($page))
    <!-- Hidden Page Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::hidden('page_id', $page->id) !!}
    </div>
@endif

<!-- Page Id Field
<div class="form-group col-sm-3">
    {!! Form::label('page_id', __('models/pageSettings.fields.page_id') . ':') !!}
    {!! Form::number('page_id', null, ['class' => 'form-control', 'required']) !!}
</div>
 -->

<!-- Has Subtitle Field -->
<div class="form-group col-sm-3">
    <div class="form-check">
        {!! Form::hidden('has_subtitle', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('has_subtitle', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('has_subtitle', 'Has Subtitle', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Has Meta Description Field -->
<div class="form-group col-sm-3">
    <div class="form-check">
        {!! Form::hidden('has_meta_description', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('has_meta_description', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('has_meta_description', 'Has Meta Description', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Has Meta Keywords Field -->
<div class="form-group col-sm-3">
    <div class="form-check">
        {!! Form::hidden('has_meta_keywords', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('has_meta_keywords', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('has_meta_keywords', 'Has Meta Keywords', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Has Excerpt Field -->
<div class="form-group col-sm-3">
    <div class="form-check">
        {!! Form::hidden('has_excerpt', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('has_excerpt', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('has_excerpt', 'Has Excerpt', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Has Body Field -->
<div class="form-group col-sm-3">
    <div class="form-check">
        {!! Form::hidden('has_body', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('has_body', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('has_body', 'Has Body', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Has Image Field -->
<div class="form-group col-sm-3">
    <div class="form-check">
        {!! Form::hidden('has_image', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('has_image', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('has_image', 'Has Image', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Has Image Two Field -->
<div class="form-group col-sm-3">
    <div class="form-check">
        {!! Form::hidden('has_image_two', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('has_image_two', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('has_image_two', 'Has Image Two', ['class' => 'form-check-label']) !!}
    </div>
</div>
