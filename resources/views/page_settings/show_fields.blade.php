<!-- Page Id Field -->
<div class="col-sm-12">
    {!! Form::label('page_id', __('models/pageSettings.fields.page_id').':') !!}
    <p>{{ $pageSetting->page_id }}</p>
</div>

<!-- Has Subtitle Field -->
<div class="col-sm-12">
    {!! Form::label('has_subtitle', __('models/pageSettings.fields.has_subtitle').':') !!}
    <p>{{ $pageSetting->has_subtitle }}</p>
</div>

<!-- Has Meta Description Field -->
<div class="col-sm-12">
    {!! Form::label('has_meta_description', __('models/pageSettings.fields.has_meta_description').':') !!}
    <p>{{ $pageSetting->has_meta_description }}</p>
</div>

<!-- Has Meta Keywords Field -->
<div class="col-sm-12">
    {!! Form::label('has_meta_keywords', __('models/pageSettings.fields.has_meta_keywords').':') !!}
    <p>{{ $pageSetting->has_meta_keywords }}</p>
</div>

<!-- Has Excerpt Field -->
<div class="col-sm-12">
    {!! Form::label('has_excerpt', __('models/pageSettings.fields.has_excerpt').':') !!}
    <p>{{ $pageSetting->has_excerpt }}</p>
</div>

<!-- Has Body Field -->
<div class="col-sm-12">
    {!! Form::label('has_body', __('models/pageSettings.fields.has_body').':') !!}
    <p>{{ $pageSetting->has_body }}</p>
</div>

<!-- Has Image Field -->
<div class="col-sm-12">
    {!! Form::label('has_image', __('models/pageSettings.fields.has_image').':') !!}
    <p>{{ $pageSetting->has_image }}</p>
</div>

<!-- Has Image Two Field -->
<div class="col-sm-12">
    {!! Form::label('has_image_two', __('models/pageSettings.fields.has_image_two').':') !!}
    <p>{{ $pageSetting->has_image_two }}</p>
</div>

