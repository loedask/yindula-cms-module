@extends('bazintemplate::layouts.authed')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>
                        @lang('crud.edit') @lang('models/pageSettings.singular')
                    </h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">


        <div class="card">

            {!! Form::model($page_setting, ['route' => ['cms.pageSettings.update', $page_setting->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('yindulacms::page_settings._fields')
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('cms.pageSettings.index') }}" class="btn btn-default"> @lang('crud.cancel') </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
