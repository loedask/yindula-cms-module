<?php

namespace Modules\YindulaCms\Database\Seeders;

use Illuminate\Database\Seeder;

class FeatureSectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('feature_sections')->delete();

        \DB::table('feature_sections')->insert(array (
            0 =>
            array (
                'id' => 1,
                'key' => 'services',
                'name' => 'Services',
                'description' => 'We put customers\' satisfaction on the top of our list. After studying through users\' needs, wants, and limitations, we apply cutting-edge technologies to create functional, large-scale, engaging mobile apps and responsive web designs.',
                'is_active' => 1,
                'created_at' => '2020-03-09 01:48:54',
                'updated_at' => '2020-03-11 11:50:46',
            ),
            1 =>
            array (
                'id' => 2,
                'key' => 'technologies',
                'name' => 'We love new technologies',
                'description' => 'Latest and emerging technologies are always preferred. Here is the brief of services that we are providing as well as a short list of technologies accompanied by the services.',
                'is_active' => 1,
                'created_at' => '2020-03-21 23:35:52',
                'updated_at' => '2020-03-21 23:35:52',
            ),
            2 =>
            array (
                'id' => 3,
                'key' => 'development-cycle',
                'name' => 'Development Process',
                'description' => 'Development Life Cycle',
                'is_active' => 1,
                'created_at' => '2020-03-26 22:01:02',
                'updated_at' => '2020-03-28 16:40:00',
            ),
        ));


    }
}
