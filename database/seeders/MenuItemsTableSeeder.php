<?php

namespace Modules\YindulaCms\Database\Seeders;

use Illuminate\Database\Seeder;

class MenuItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('menu_items')->delete();

        \DB::table('menu_items')->insert(array (
            0 =>
            array (
                'id' => 1,
                'menu_id' => 1,
                'title' => 'Dashboard',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-dashboard',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 1,
                'created_at' => '2020-02-08 06:39:54',
                'updated_at' => '2020-06-22 19:15:06',
                'route' => 'voyager.dashboard',

            ),
            1 =>
            array (
                'id' => 2,
                'menu_id' => 1,
                'title' => 'Media',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-images',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 6,
                'created_at' => '2020-02-08 06:39:54',
                'updated_at' => '2020-02-08 13:38:19',
                'route' => 'voyager.media.index',

            ),
            2 =>
            array (
                'id' => 3,
                'menu_id' => 1,
                'title' => 'Users',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-group',
                'color' => '#000000',
                'parent_id' => 15,
                'order' => 2,
                'created_at' => '2020-02-08 06:39:54',
                'updated_at' => '2020-06-22 20:44:19',
                'route' => 'voyager.users.index',

            ),
            3 =>
            array (
                'id' => 4,
                'menu_id' => 1,
                'title' => 'Roles',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-lock',
                'color' => NULL,
                'parent_id' => 15,
                'order' => 1,
                'created_at' => '2020-02-08 06:39:54',
                'updated_at' => '2020-02-08 13:37:17',
                'route' => 'voyager.roles.index',

            ),
            4 =>
            array (
                'id' => 5,
                'menu_id' => 1,
                'title' => 'Tools',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tools',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 6,
                'created_at' => '2020-02-08 06:39:54',
                'updated_at' => '2020-06-22 22:13:11',
                'route' => NULL,

            ),
            5 =>
            array (
                'id' => 6,
                'menu_id' => 1,
                'title' => 'Menu Builder',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-list',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 1,
                'created_at' => '2020-02-08 06:39:54',
                'updated_at' => '2020-02-08 13:37:08',
                'route' => 'voyager.menus.index',

            ),
            6 =>
            array (
                'id' => 7,
                'menu_id' => 1,
                'title' => 'Database',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-data',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 2,
                'created_at' => '2020-02-08 06:39:54',
                'updated_at' => '2020-02-08 13:37:08',
                'route' => 'voyager.database.index',

            ),
            7 =>
            array (
                'id' => 8,
                'menu_id' => 1,
                'title' => 'Compass',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-compass',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 3,
                'created_at' => '2020-02-08 06:39:55',
                'updated_at' => '2020-02-08 13:37:08',
                'route' => 'voyager.compass.index',

            ),
            8 =>
            array (
                'id' => 9,
                'menu_id' => 1,
                'title' => 'BREAD',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-bread',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 4,
                'created_at' => '2020-02-08 06:39:55',
                'updated_at' => '2020-02-08 13:37:08',
                'route' => 'voyager.bread.index',

            ),
            9 =>
            array (
                'id' => 10,
                'menu_id' => 1,
                'title' => 'Settings',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-handle',
                'color' => '#000000',
                'parent_id' => 26,
                'order' => 1,
                'created_at' => '2020-02-08 06:39:55',
                'updated_at' => '2020-06-22 20:41:35',
                'route' => 'voyager.settings.index',

            ),
            10 =>
            array (
                'id' => 11,
                'menu_id' => 1,
                'title' => 'Categories',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-categories',
                'color' => NULL,
                'parent_id' => 16,
                'order' => 6,
                'created_at' => '2020-02-08 06:40:14',
                'updated_at' => '2020-06-25 20:09:16',
                'route' => 'voyager.categories.index',

            ),
            11 =>
            array (
                'id' => 12,
                'menu_id' => 1,
                'title' => 'Posts',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-news',
                'color' => NULL,
                'parent_id' => 16,
                'order' => 5,
                'created_at' => '2020-02-08 06:40:16',
                'updated_at' => '2020-06-24 16:28:43',
                'route' => 'voyager.posts.index',

            ),
            12 =>
            array (
                'id' => 13,
                'menu_id' => 1,
                'title' => 'Pages',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-file-text',
                'color' => NULL,
                'parent_id' => 16,
                'order' => 4,
                'created_at' => '2020-02-08 06:40:20',
                'updated_at' => '2020-06-24 16:28:43',
                'route' => 'voyager.pages.index',

            ),
            13 =>
            array (
                'id' => 14,
                'menu_id' => 1,
                'title' => 'Hooks',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-hook',
                'color' => NULL,
                'parent_id' => 5,
                'order' => 5,
                'created_at' => '2020-02-08 06:40:29',
                'updated_at' => '2020-02-08 13:37:08',
                'route' => 'voyager.hooks',

            ),
            14 =>
            array (
                'id' => 15,
                'menu_id' => 1,
                'title' => 'Administration',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-person',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 2,
                'created_at' => '2020-02-08 13:36:57',
                'updated_at' => '2020-02-08 13:37:15',
                'route' => NULL,

            ),
            15 =>
            array (
                'id' => 16,
                'menu_id' => 1,
                'title' => 'CMS',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-book',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 5,
                'created_at' => '2020-02-08 13:38:52',
                'updated_at' => '2020-06-22 22:13:11',
                'route' => NULL,

            ),
            16 =>
            array (
                'id' => 17,
                'menu_id' => 1,
                'title' => 'Banners',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-tv',
                'color' => '#000000',
                'parent_id' => 16,
                'order' => 3,
                'created_at' => '2020-02-09 10:06:58',
                'updated_at' => '2020-06-24 16:28:43',
                'route' => 'voyager.banner.index',

            ),
            17 =>
            array (
                'id' => 19,
                'menu_id' => 1,
                'title' => 'Feature Sections',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-paperclip',
                'color' => '#000000',
                'parent_id' => 16,
                'order' => 1,
                'created_at' => '2020-03-09 01:43:53',
                'updated_at' => '2020-03-09 01:51:51',
                'route' => 'voyager.feature-section.index',

            ),
            18 =>
            array (
                'id' => 20,
                'menu_id' => 1,
                'title' => 'Features',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-window-list',
                'color' => NULL,
                'parent_id' => 16,
                'order' => 2,
                'created_at' => '2020-03-09 01:55:12',
                'updated_at' => '2020-03-09 01:58:43',
                'route' => 'voyager.feature.index',

            ),
            19 =>
            array (
                'id' => 22,
                'menu_id' => 2,
                'title' => 'Home',
                'url' => '/',
                'target' => '_self',
                'icon_class' => NULL,
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 1,
                'created_at' => '2020-03-21 23:05:57',
                'updated_at' => '2020-03-26 00:10:18',
                'route' => NULL,

            ),
            20 =>
            array (
                'id' => 23,
                'menu_id' => 2,
                'title' => 'About us',
                'url' => 'page/about-us',
                'target' => '_self',
                'icon_class' => NULL,
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 2,
                'created_at' => '2020-03-21 23:06:20',
                'updated_at' => '2020-03-26 00:58:46',
                'route' => NULL,

            ),
            21 =>
            array (
                'id' => 26,
                'menu_id' => 1,
                'title' => 'App Settings',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-settings',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 3,
                'created_at' => '2020-06-22 20:40:56',
                'updated_at' => '2020-06-22 21:48:34',
                'route' => NULL,

            ),
            22 =>
            array (
                'id' => 27,
                'menu_id' => 1,
                'title' => 'Quote Statuses',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-medal-rank-star',
                'color' => NULL,
                'parent_id' => 26,
                'order' => 2,
                'created_at' => '2020-06-22 21:47:51',
                'updated_at' => '2020-06-22 22:34:54',
                'route' => 'voyager.quote-statuses.index',

            ),
            23 =>
            array (
                'id' => 28,
                'menu_id' => 1,
                'title' => 'Documents',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-paperclip',
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 4,
                'created_at' => '2020-06-22 22:10:11',
                'updated_at' => '2020-06-27 12:45:27',
                'route' => NULL,

            ),
            24 =>
            array (
                'id' => 29,
                'menu_id' => 1,
                'title' => 'Quotes',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-book-download',
                'color' => '#000000',
                'parent_id' => 28,
                'order' => 1,
                'created_at' => '2020-06-22 22:12:26',
                'updated_at' => '2020-06-22 22:35:47',
                'route' => 'voyager.quotes.index',

            ),
            25 =>
            array (
                'id' => 30,
                'menu_id' => 1,
                'title' => 'Companies',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-company',
                'color' => '#000000',
                'parent_id' => 15,
                'order' => 3,
                'created_at' => '2020-06-22 22:34:28',
                'updated_at' => '2020-06-22 22:35:18',
                'route' => 'voyager.companies.index',

            ),
            26 =>
            array (
                'id' => 31,
                'menu_id' => 1,
                'title' => 'Quote Items',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-list',
                'color' => NULL,
                'parent_id' => 28,
                'order' => 2,
                'created_at' => '2020-06-24 16:27:50',
                'updated_at' => '2020-06-24 16:28:43',
                'route' => 'voyager.quote-items.index',

            ),
            27 =>
            array (
                'id' => 32,
                'menu_id' => 1,
                'title' => 'Invoices',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-receipt',
                'color' => '#000000',
                'parent_id' => 28,
                'order' => 3,
                'created_at' => '2020-06-25 20:08:41',
                'updated_at' => '2020-06-25 20:27:09',
                'route' => 'voyager.invoices.index',

            ),
            28 =>
            array (
                'id' => 33,
                'menu_id' => 2,
                'title' => 'Contact Us',
                'url' => 'contact',
                'target' => '_self',
                'icon_class' => NULL,
                'color' => '#000000',
                'parent_id' => NULL,
                'order' => 7,
                'created_at' => '2021-07-06 02:20:31',
                'updated_at' => '2021-07-06 02:20:31',
                'route' => NULL,
                
            ),
        ));


    }
}
