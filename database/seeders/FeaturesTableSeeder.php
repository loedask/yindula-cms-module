<?php

namespace Modules\YindulaCms\Database\Seeders;

use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('features')->delete();

        \DB::table('features')->insert(array (
            0 =>
            array (
                'id' => 1,
                'feature_section_id' => 1,
                'title' => 'Web development',
                'description' => '<p>Fully customized<br />Single page<br />Realtime<br />Responsive</p>',
                'image' => NULL,
                'is_active' => 1,
                'created_at' => '2020-03-09 02:10:25',
                'updated_at' => '2020-03-22 00:03:24',
                'icon' => 'icon-cogs',
            ),
            1 =>
            array (
                'id' => 2,
                'feature_section_id' => 1,
                'title' => 'Mobile application',
                'description' => '<p>Native<br />Cross-platform<br />Multi-device<br />support Security</p>',
                'image' => NULL,
                'is_active' => 1,
                'created_at' => '2020-03-11 11:09:26',
                'updated_at' => '2020-03-22 00:02:51',
                'icon' => 'icon-cogs',
            ),
            2 =>
            array (
                'id' => 3,
                'feature_section_id' => 1,
                'title' => 'Design',
                'description' => '<p>Web application<br />Mobile application<br />Graphic design<br />Brand identity</p>',
                'image' => NULL,
                'is_active' => 1,
                'created_at' => '2020-03-11 11:16:20',
                'updated_at' => '2020-03-22 00:03:08',
                'icon' => 'icon-cogs',
            ),
            3 =>
            array (
                'id' => 4,
                'feature_section_id' => 3,
                'title' => 'Development Life Cycle',
                'description' => NULL,
                'image' => 'feature/January2021/nZGo1nXxlQbFNKH9bIEY.png',
                'is_active' => 1,
                'created_at' => '2020-03-28 16:26:34',
                'updated_at' => '2021-01-18 00:42:29',
                'icon' => NULL,
            ),
        ));


    }
}
