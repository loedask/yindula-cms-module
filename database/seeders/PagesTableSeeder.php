<?php

namespace Modules\YindulaCms\Database\Seeders;

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('pages')->delete();

        \DB::table('pages')->insert(array (
            0 =>
            array (
                'id' => 1,
                'author_id' => 1,
                'title' => 'We are the Loecos Corporate Group',
                'excerpt' => 'Give us the privilege to materialize your dream by building your project.',
                'body' => '<p>The Loecos Corporate is a dynamic tech company founded by people who love what they do.</p>
<p>We\'re innovative, energetic, and constantly growing. Our days consist of building great applications, providing real solutions, crafting custom-made tools that meet organizations unique needs and growing our community.</p>
<p>We\'re looking for people who can thrive with us.</p>',
                'image' => 'pages/January2021/0CArMAVgbADWcd6PQNkw.jpeg',
                'slug' => 'about-us',
                'meta_description' => 'Yar Meta Description',
                'status' => 'ACTIVE',
                'created_at' => '2020-02-08 06:40:21',
                'updated_at' => '2021-01-18 01:58:57',
            ),
        ));


    }
}
