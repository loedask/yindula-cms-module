<?php

namespace Modules\YindulaCms\Database\Seeders;

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'created_at' => '2020-02-08 06:39:54',
                'updated_at' => '2020-02-08 06:39:54',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'main',
                'created_at' => '2020-03-08 19:19:12',
                'updated_at' => '2020-03-08 19:19:12',
            ),
        ));
        
        
    }
}
