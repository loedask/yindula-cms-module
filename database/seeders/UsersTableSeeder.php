<?php

namespace Modules\YindulaCms\Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'role_id' => 1,
                'company_id' => 1,
                'name' => 'Clotildah',
                'surname' => 'Loecos',
                'email' => 'clotildah@loecos.net',
                'contact_number' => '0768846524',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$bzeR34ddA4heKaIaZlbCSeRxK2eVG95rFzkGmcDc./zrBx8oS29vW',
                'remember_token' => 'hORL2kEpzFdk1Yna544dFG2U4XX8oyhscmagvHA1svHz13N0MsHmITJJD2Y0',
                'settings' => '{"locale":"en"}',
                'created_by' => NULL,
                'created_at' => '2020-02-08 06:40:15',
                'updated_at' => '2020-06-23 00:43:43',
            ),
            1 =>
            array (
                'id' => 2,
                'role_id' => 3,
                'company_id' => 1,
                'name' => 'Daskana',
                'surname' => 'Loecos',
                'email' => 'engineer@loecos.net',
                'contact_number' => '0768846524',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$iKzPFwIBUF20VYJwlORibuK3WvuB/OWeGdLNI0GTLyNx1VhompVgK',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_by' => NULL,
                'created_at' => '2020-06-22 18:56:14',
                'updated_at' => '2020-06-23 00:43:17',
            ),
            2 =>
            array (
                'id' => 3,
                'role_id' => 4,
                'company_id' => 1,
                'name' => 'Client',
                'surname' => 'Test',
                'email' => 'client@loecos.net',
                'contact_number' => '0768846524',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$mJA0vbW90XmSV41Z8DMq/eL7fEBPaeLX4Y.wK8CjdRAeT1fQYiqWC',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_by' => NULL,
                'created_at' => '2020-06-23 00:42:05',
                'updated_at' => '2020-06-23 00:42:05',
            ),
            3 =>
            array (
                'id' => 4,
                'role_id' => 4,
                'company_id' => 2,
                'name' => 'David',
                'surname' => 'Lethabane',
                'email' => 'olethabane@gmail.com',
                'contact_number' => '0845994602',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$iyjdjSHl3mMF43D/hLUuUOjU0Lusd6uCq7UQ3VEIGLQtpozDF6nCS',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2020-06-30 15:55:46',
                'updated_at' => '2020-06-30 15:55:46',
            ),
            4 =>
            array (
                'id' => 5,
                'role_id' => 4,
                'company_id' => 3,
                'name' => 'Cedric',
                'surname' => 'Ngandu',
                'email' => 'cedngk1985@gmail.com',
                'contact_number' => '0840681296',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$eTOo6KplnxPul2yv127pxe/.ggK46c7PsED1skwce85RZ51nmEa06',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2020-11-06 18:13:45',
                'updated_at' => '2020-11-06 18:13:45',
            ),
            5 =>
            array (
                'id' => 6,
                'role_id' => 4,
                'company_id' => 3,
                'name' => 'Juanita',
                'surname' => 'Boonzaaier',
                'email' => 'Juanitaboonzaaier4@gmail.com',
                'contact_number' => '0722414314',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$fb2pP050/UzHUnLEjNZdBuWwz8Sen5ggv0uKOOIY7D07zbonIPaii',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2020-11-16 18:35:39',
                'updated_at' => '2020-11-16 18:35:39',
            ),
            6 =>
            array (
                'id' => 7,
                'role_id' => 4,
                'company_id' => 3,
                'name' => 'Jean Jacques',
                'surname' => 'Dhybondo',
                'email' => 'jackd@globalsolution-cd.com',
                'contact_number' => '0659103991',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$/ZJ4cltSBcsbaePI5i9s4uMM3rvedxYgTK4gRyR5ePoi1rf6y6ILO',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2020-12-04 08:06:54',
                'updated_at' => '2020-12-04 08:06:54',
            ),
            7 =>
            array (
                'id' => 8,
                'role_id' => 4,
                'company_id' => 3,
                'name' => 'Nyiko',
                'surname' => 'Bvuma',
                'email' => 'nyiko@example.net',
                'contact_number' => '072 790 0302',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$ry7evvQJZhWxem/P6HXgF.LAStPed4o7gMsdQfyFyqGdBRKRBiZP6',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2021-07-06 22:39:56',
                'updated_at' => '2021-07-06 22:39:56',
            ),
            8 =>
            array (
                'id' => 9,
                'role_id' => 4,
                'company_id' => 3,
                'name' => 'Tshepo',
                'surname' => 'Thebyane',
                'email' => 'thebyanetshepo@gmail.com',
                'contact_number' => '0765371710',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$D7/GJHpv8zJEp3asfDkzZuhltEfhuoQAFZjRdaP7XvGD5it/7oBQK',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2021-07-27 17:11:30',
                'updated_at' => '2021-07-27 17:11:30',
            ),
            9 =>
            array (
                'id' => 10,
                'role_id' => 4,
                'company_id' => 3,
                'name' => 'Aymar',
                'surname' => 'Boungou',
                'email' => 'armelboungou@gmail.com',
                'contact_number' => '0714144546',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$JhCDULI8dceHjTEtd/LnceFLlmPWcruGihwij0/oIdttZ1pibhOVW',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2021-08-01 11:51:16',
                'updated_at' => '2021-08-01 12:19:36',
            ),
            10 =>
            array (
                'id' => 11,
                'role_id' => 4,
                'company_id' => 3,
                'name' => 'Jack',
                'surname' => 'Theron',
                'email' => 'jack@touchpoint.co.za',
                'contact_number' => '0768846580',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Ej44fppVUVnBgyYMJQLAfeXFL3r1G..miFYMAf5mm1ATOWKPk4DGW',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2021-08-28 08:07:37',
                'updated_at' => '2021-08-28 08:07:37',
            ),
            11 =>
            array (
                'id' => 12,
                'role_id' => 4,
                'company_id' => 3,
                'name' => 'Rodrick',
                'surname' => NULL,
                'email' => 'rodrick@gmail.com',
                'contact_number' => '+1 (646) 684-6077',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$5djxWfKquo2d1kSwrRRGFuHA.bXanGDphvBCNu2brGqV1Ph.fQ9sm',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_by' => NULL,
                'created_at' => '2022-07-27 20:13:45',
                'updated_at' => '2022-07-27 20:13:45',
            ),
        ));


    }
}
