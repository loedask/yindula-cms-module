<?php

namespace Modules\YindulaCms\Database\Seeders;

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('banners')->delete();

        \DB::table('banners')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'We build creative solutions for great people.',
                'description' => NULL,
                'image' => 'banner/January2021/7dmgpnc2HW0Cm7ooY1AZ.png',
                'video' => NULL,
                'is_default' => 1,
                'created_at' => '2020-02-09 11:03:00',
                'updated_at' => '2021-01-18 01:56:57',
                'excerpt' => 'What part of your business do you need help with?',
                'image_two' => 'banner/January2021/fw3n5FBYgs2phn04uMLw.png',
            ),
        ));


    }
}
