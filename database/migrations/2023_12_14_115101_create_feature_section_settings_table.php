<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('feature_section_settings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('feature_section_id')->constrained();

            $table->boolean('has_category')->nullable()->default(false);

            $table->boolean('has_name')->nullable()->default(true);
            $table->boolean('has_title')->nullable()->default(true);

            $table->boolean('has_description')->nullable()->default(true);

            $table->boolean('has_image')->nullable()->default(false);
            $table->boolean('has_image_two')->nullable()->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('feature_section_settings');
    }
};
