<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->id();

            // $table->unsignedBigInteger('feature_section_id')->index();
            // $table->foreign('feature_section_id')->references('id')->on('feature_sections');

            $table->foreignId('feature_section_id')->nullable()->constrained();

            $table->string('title', 100);
            $table->text('description')->nullable();
            $table->string('image')->nullable();

            $table->boolean('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
}
