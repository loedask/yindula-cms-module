<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('page_settings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('page_id')->constrained();

            $table->boolean('has_subtitle')->nullable()->default(true);

            $table->boolean('has_meta_description')->nullable()->default(false);
            $table->boolean('has_meta_keywords')->nullable()->default(false);

            $table->boolean('has_excerpt')->nullable()->default(false);
            $table->boolean('has_body')->nullable()->default(true);
            
            $table->boolean('has_image')->nullable()->default(false);
            $table->boolean('has_image_two')->nullable()->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('page_settings');
    }
};
