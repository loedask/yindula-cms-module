<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('feature_settings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('feature_section_id')->constrained();

            $table->boolean('has_category')->nullable()->default(false);
            $table->boolean('has_content')->nullable()->default(false);
            $table->boolean('has_url')->nullable()->default(false);
            $table->boolean('has_icon')->nullable()->default(false);
            $table->boolean('has_sort_order')->nullable()->default(false);
            $table->boolean('has_redirect_to_page')->nullable()->default(false);

            $table->boolean('has_image')->nullable()->default(false);
            $table->boolean('has_image_two')->nullable()->default(false);

            $table->boolean('has_video')->nullable()->default(false);
            $table->boolean('has_video_two')->nullable()->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('feature_settings');
    }
};
