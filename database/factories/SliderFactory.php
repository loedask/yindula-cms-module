<?php

namespace Modules\YindulaCms\database\factories;

use Modules\YindulaCms\app\Models\Slider;
use Illuminate\Database\Eloquent\Factories\Factory;

class SliderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Slider::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
            'subtitle' => $this->faker->word,
            'excerpt' => $this->faker->text,
            'description' => $this->faker->text,
            'image' => $this->faker->word,
            'image_two' => $this->faker->text,
            'video' => $this->faker->word,
            'url' => $this->faker->word,
            'is_default' => $this->faker->word,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
