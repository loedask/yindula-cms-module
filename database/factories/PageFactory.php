<?php

namespace Modules\YindulaCms\database\factories;

use Modules\YindulaCms\app\Models\Page;
use Illuminate\Database\Eloquent\Factories\Factory;


class PageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Page::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'author_id' => $this->faker->word,
            'title' => $this->faker->text($this->faker->numberBetween(5, 255)),
            'subtitle' => $this->faker->text($this->faker->numberBetween(5, 255)),
            'excerpt' => $this->faker->text($this->faker->numberBetween(5, 65535)),
            'body' => $this->faker->text($this->faker->numberBetween(5, 65535)),
            'image' => $this->faker->text($this->faker->numberBetween(5, 255)),
            'slug' => $this->faker->text($this->faker->numberBetween(5, 255)),
            'meta_description' => $this->faker->text($this->faker->numberBetween(5, 65535)),
            'meta_keywords' => $this->faker->text($this->faker->numberBetween(5, 65535)),
            'status' => $this->faker->text($this->faker->numberBetween(5, 4096)),
            'is_page' => $this->faker->boolean,
            'is_home' => $this->faker->boolean,
            'is_contact' => $this->faker->boolean,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
