<?php

namespace Modules\YindulaCms\database\factories;

use Modules\YindulaCms\app\Models\Banner;
use Illuminate\Database\Eloquent\Factories\Factory;

class BannerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Banner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text($this->faker->numberBetween(5, 100)),
            'excerpt' => $this->faker->text($this->faker->numberBetween(5, 65535)),
            'description' => $this->faker->text($this->faker->numberBetween(5, 65535)),
            'image' => $this->faker->text($this->faker->numberBetween(5, 255)),
            'video' => $this->faker->text($this->faker->numberBetween(5, 255)),
            'is_default' => $this->faker->boolean,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s'),
            'image_two' => $this->faker->text($this->faker->numberBetween(5, 65535))
        ];
    }
}
