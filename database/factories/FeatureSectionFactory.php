<?php

namespace Modules\YindulaCms\database\factories;

use App\Models\Category;
use Modules\YindulaCms\app\Models\Page;

use Modules\YindulaCms\app\Models\FeatureSection;
use Illuminate\Database\Eloquent\Factories\Factory;

class FeatureSectionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FeatureSection::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $page = Page::first();
        if (!$page) {
            $page = Page::factory()->create();
        }

        return [
            'category_id' => $this->faker->word,
            'key' => $this->faker->text($this->faker->numberBetween(5, 100)),
            'name' => $this->faker->text($this->faker->numberBetween(5, 100)),
            'title' => $this->faker->text($this->faker->numberBetween(5, 100)),
            'image' => $this->faker->text($this->faker->numberBetween(5, 255)),
            'page_id' => $this->faker->word,
            'description' => $this->faker->text($this->faker->numberBetween(5, 65535)),
            'is_active' => $this->faker->boolean,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
